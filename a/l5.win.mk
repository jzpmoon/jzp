bin_name = l5$(suf_pg)
obj_list = l5.obj

libl5_path = ..\l\libl5\

libl_path = ..\l\

libv_path = ..\v\

libu_path = ..\u\

liblm_path = ..\l\liblm\

libed_path = ..\l\libed\

libdbg_path = ..\l\libdbg\

somk   = makefile

CFLAGS = $(STDC98) $(WALL) $(WEXTRA) $(WNO_UNUSED_PARAMETER) $(DEBUG_MODE)

&(bin_name):libl5.lib liblm.lib libed.lib libdbg.lib $(obj_list)
	$(LINK) $(obj_list) $(L)$(libl5_path) libl5.lib $(L)$(libl_path) libl.lib $(L)$(libv_path) libv.lib $(L)$(libu_path) libu.lib $(OUT)$(bin_name)
.c.obj:
	$(CC) $(C) $(COUT)$@ $< $(I) $(libl5_path) $(I) $(libl_path) $(I) $(libv_path) $(I) $(libu_path) $(CFLAGS)
libl5.lib:
	cd $(libl5_path)
	nmake /f $(somk)
	cd $(currdir)
liblm.lib:
	cd $(liblm_path)
	nmake /f $(somk)
	cd $(currdir)
libed.lib:
	cd $(libed_path)
	nmake /f $(somk)
	cd $(currdir)
libdbg.lib:
	cd $(libdbg_path)
	nmake /f $(somk)
	cd $(currdir)
install:
	copy $(bin_name) $(prefix)
	cd $(libl5_path)
	nmake /f $(somk) install
	cd $(currdir)
	cd $(libl_path)
	nmake /f $(somk) install
	cd $(currdir)
	cd $(libv_path)
	nmake /f $(somk) install
	cd $(currdir)
	cd $(libu_path)
	nmake /f $(somk) install
	cd $(currdir)
	cd $(liblm_path)
	nmake /f $(somk) install
	cd $(currdir)
	cd $(libed_path)
	nmake /f $(somk) install
	cd $(currdir)
	cd $(libdbg_path)
	nmake /f $(somk) install
	cd $(currdir)
uninstall:
	del $(prefix)\$(bin_name)
	cd $(libl5_path)
	nmake /f $(somk) uninstall
	cd $(currdir)
	cd $(libl_path)
	nmake /f $(somk) uninstall
	cd $(currdir)
	cd $(libv_path)
	nmake /f $(somk) uninstall
	cd $(currdir)
	cd $(libu_path)
	nmake /f $(somk) uninstall
	cd $(currdir)
	cd $(liblm_path)
	nmake /f $(somk) uninstall
	cd $(currdir)
	cd $(libed_path)
	nmake /f $(somk) uninstall
	cd $(currdir)
	cd $(libdbg_path)
	nmake /f $(somk) uninstall
	cd $(currdir)
clean:
	cd $(libl5_path)
	nmake /f $(somk) clean
	cd $(currdir)
	cd $(libl_path)
	nmake /f $(somk) clean
	cd $(currdir)
	cd $(libv_path)
	nmake /f $(somk) clean
	cd $(currdir)
	cd $(libu_path)
	nmake /f $(somk) clean
	cd $(currdir)
	cd $(liblm_path)
	nmake /f $(somk) clean
	cd $(currdir)
	cd $(libed_path)
	nmake /f $(somk) clean
	cd $(currdir)
	cd $(libdbg_path)
	nmake /f $(somk) clean
	cd $(currdir)
	del $(bin_name) $(obj_list)

#include "uerror.h"
#include "ustring.h"
#include "l5eval.h"

int main(int argc, char** args)
{
  l5eval* eval = UNULL;
  char* path = UNULL;
  char* vm_asz_str;
  int vm_asz_int = 10 * 1024;
  char* vm_ssz_str;
  int vm_ssz_int = 100 * 1024;
  char* vm_rsz_str;
  int vm_rsz_int = 1024;
  char* conf = UNULL;
  char* client_conf = UNULL;
  char* script = UNULL;
  int log_enable = UTRUE;
  char* log_name = "l5.log";
  char* log_max_line_str = UNULL;
  int log_max_line_int = -1;
  char* lib_path = UNULL;
  static ulog_conf log_conf;
  int i;

  for (i = 1; i < argc; i++) {
    if (!script) {
      if (!ustrcmp(args[i], "--log-enable")) {
        if (++i >= argc) break;
        if (!ustrcmp(args[i], "true")) {
          log_enable = UTRUE;
        } else {
          log_enable = UFALSE;
        }
      } else if (!ustrcmp(args[i], "--log-name")) {
        if (++i >= argc) break;
        log_name = args[i];
      } else if (!ustrcmp(args[i], "--log-max-line")) {
        if (++i >= argc) break;
        log_max_line_str = args[i];
        if (ucharp_isint(log_max_line_str)) {
          log_max_line_int = atoi(log_max_line_str);
        }
        else {
          uabort("--log-max-line not a integer");
        }
      } else if (!ustrcmp(args[i], "--self-path")) {
        if (++i >= argc) break;
        path = args[i];
      } else if (!ustrcmp(args[i], "--conf")) {
        if (++i >= argc) break;
        conf = args[i];
      } else if (!ustrcmp(args[i], "--client-conf")) {
        if (++i >= argc) break;
        client_conf = args[i];
      } else if (!ustrcmp(args[i], "--vm-asz")) {
        if (++i >= argc) break;
        vm_asz_str = args[i];
        if (ucharp_isint(vm_asz_str)) {
          vm_asz_int = atoi(vm_asz_str);
        }
        else {
          uabort("--vm-asz not a integer");
        }
      } else if (!ustrcmp(args[i], "--vm-ssz")) {
        if (++i >= argc) break;
        vm_ssz_str = args[i];
        if (ucharp_isint(vm_ssz_str)) {
          vm_ssz_int = atoi(vm_ssz_str);
        } else {
          uabort("--vm-ssz not a integer");
        }
      } else if (!ustrcmp(args[i], "--vm-rsz")) {
        if (++i >= argc) break;
        vm_rsz_str = args[i];
        if (ucharp_isint(vm_rsz_str)) {
          vm_rsz_int = atoi(vm_rsz_str);
        } else {
          uabort("--vm-rsz not a integer");
        }
      } else if (!ustrcmp(args[i], "--lib-path")) {
        if (++i >= argc) break;
        lib_path = args[i];
      } else {
        script = args[i];
        log_conf.log_fn = log_name;
        log_conf.power = log_enable;
        log_conf.line = log_max_line_int;
        ulog_init(&log_conf);

        if (!path) {
          uabort("--self-path not define");
        }
        if (!script) {
          uabort("script file not define");
        }
        
        eval = l5startup(path, vm_asz_int, vm_ssz_int, vm_rsz_int);
        l5eval_init(eval);
      }
    } else {
      char* arg_name;
      arg_name = args[i];
      l5eval_cmd_arg_put(eval, arg_name);
    }
  }

  if (!script) {
    log_conf.log_fn = log_name;
    log_conf.power = log_enable;
    log_conf.line = log_max_line_int;
    ulog_init(&log_conf);
  }
  
  if (!eval) {
    if (!path) {
      uabort("--self-path not define");
    }
    eval = l5startup(path, vm_asz_int, vm_ssz_int, vm_rsz_int);
    l5eval_init(eval);
  }

  if (lib_path) {
    if (leval_lib_path_add_s(eval->base_eval,lib_path)) {
      uabort("lib path add error!");
    }
  }
  
  if (eval && script) {
    l5eval_src_path_set(eval, script);
  }
  
  if (!conf) {
    uabort("--conf not define");
  }
  leval_path_self(eval->base_eval);
  l5eval_sys_conf_load(eval, conf, s);
  
  if (client_conf) {
    leval_path_client(eval->base_eval);
    l5eval_usr_conf_load(eval, client_conf, s);
  }
  
  if (script) {
    l5eval_src_load(eval);
  } else {
    l5eval_interactive_enable(eval);
    while (1) {
      l5eval_cmd_load(eval);
      l5eval_init(eval);
    }
  }
  
  return 0;
}

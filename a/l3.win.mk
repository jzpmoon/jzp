bin_name = l3$(suf_pg)
obj_list = l3.obj

libl3_path = ..\l\libl3\

libl_path = ..\l\

libv_path = ..\v\

libu_path = ..\u\

liblm_path = ..\l\

somk   = makefile

CFLAGS = $(STDC98) $(WALL) $(WEXTRA) $(WNO_UNUSED_PARAMETER) $(DEBUG_MODE)

&(bin_name):libl3.lib liblm.lib $(obj_list)
	$(LINK) $(obj_list) $(L)$(libl_path) libl3.lib $(L)$(libv_path) libv.lib $(L)$(libu_path) libu.lib $(OUT)$(bin_name)
.c.obj:
	$(CC) $(I) $(libl_path) $(I) $(libv_path) $(I) $(libu_path) $(C) $(COUT)$@ $< $(CFLAGS)
libl3.lib:
	cd $(libl3_path)
	nmake /f $(somk)
	cd $(currdir)
libl.lib:
	cd $(libl_path)
	nmake /f $(somk)
	cd $(currdir)
liblm.lib:
	cd $(liblm_path)
	nmake /f $(somk)
	cd $(currdir)
install:
	copy $(bin_name) $(prefix)
	copy $(entry_exec) $(prefix)
  cd $(libl3_path)
	nmake /f $(somk) install
	cd $(currdir)
	cd $(libl_path)
	nmake /f $(somk) install
	cd $(currdir)
	cd $(libv_path)
	nmake /f $(somk) install
	cd $(currdir)
	cd $(libu_path)
	nmake /f $(somk) install
	cd $(currdir)
	cd $(liblm_path)
	nmake /f $(somk) install
	cd $(currdir)
uninstall:
	del $(prefix)\$(bin_name)
	del $(prefix)\$(entry_exec)
	cd $(libl3_path)
	nmake /f $(somk) uninstall
	cd $(currdir)
  cd $(libl_path)
	nmake /f $(somk) uninstall
	cd $(currdir)
	cd $(libv_path)
	nmake /f $(somk) uninstall
	cd $(currdir)
	cd $(libu_path)
	nmake /f $(somk) uninstall
	cd $(currdir)
	cd $(liblm_path)
	nmake /f $(somk) uninstall
	cd $(currdir)
clean:
  cd $(libl3_path)
	nmake /f $(somk) clean
	cd $(currdir)
	cd $(libl_path)
	nmake /f $(somk) clean
	cd $(currdir)
  cd $(libv_path)
	nmake /f $(somk) clean
	cd $(currdir)
  cd $(libu_path)
	nmake /f $(somk) clean
	cd $(currdir)
	cd $(liblm_path)
	nmake /f $(somk) clean
	cd $(currdir)
	del $(bin_name) $(obj_list)

@echo off
setlocal enabledelayedexpansion
set prefix=%cd%
set envc=win
set thw=x86_64
set currdir=%cd%

:loop
	shift
	if "%0%" == "" (
		goto end
	) else if "%0%" == "--prefix" (
		set prefix=%1
	) else if "%0%" == "--envc" (
		set envc=%1
	) else if "%0%" == "--thw" (
		set thw=%1
	)
	goto loop
:end

set entry_exec=l3.bat
echo @echo off > %entry_exec%
echo %%~dp0l3 --self-path %%~dp0 --conf l3.win.conf --log-enable true --log-name l3.log %%* >> %entry_exec%

echo prefix=%prefix% > makefile
echo envc=%envc% >> makefile
echo thw=%thw% >> makefile
echo currdir=%currdir% >> makefile
echo entry_exec=%entry_exec% >> makefile
echo !include ..\u\env\env_%envc%.mk >> makefile
type l3.win.mk >> makefile

cd ..\l\libl3\
call .\configure.bat --prefix=%prefix% --envc=%envc% --thw=%thw%
cd %currdir%
cd ..\l\liblm\
call .\configure.bat --prefix=%prefix% --envc=%envc% --thw=%thw%
cd %currdir%

endlocal

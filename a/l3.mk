bin_name = l3$(suf_pg)
obj_list = l3.o

l3_lib = l3
l_lib = l
v_lib = v
u_lib = u
lm_lib = lm

libl3_path = ../l/libl3/
libl_path = ../l/
libv_path = ../v/
libu_path = ../u/
liblm_path = ../l/

l3_somk  = $(libl3_path)makefile
l_somk   = $(libl_path)makefile
v_somk   = $(libv_path)makefile
u_somk   = $(libu_path)makefile
lmod_somk   = $(liblm_path)makefile

CFLAGS   = -std=c89 -Wall $(DEBUG_MODE)

&(bin_name):$(obj_list) $(l_lib) $(lm_lib)
	$(CC) $(obj_list) -L$(libl3_path) -l$(l3_lib) -L$(libl_path) -l$(l_lib) -L$(libv_path) -l$(v_lib) -L$(libu_path) -l$(u_lib) -o $(bin_name)
.c.o:
	$(CC) -c -o $@ $< -I $(libl_path) -I $(libv_path) -I $(libu_path) $(CFLAGS)
$(l3_lib):
	make -C $(libl3_path) -f $(l3_somk)
$(l_lib):
	make -C $(libl_path) -f $(l_somk)
$(lm_lib):
	make -C $(liblm_path) -f $(lmod_somk)
install:
	cp $(bin_name) $(prefix)/; 				\
	make -C $(libl3_path) -f $(l3_somk) install; 		\
	make -C $(libl_path) -f $(l_somk) install; 		\
	make -C $(libv_path) -f $(v_somk) install; 		\
	make -C $(libu_path) -f $(u_somk) install; 		\
	make -C $(liblm_path) -f $(lmod_somk) install; 	\
	cp $(entry_exec) $(prefix)/
uninstall:
	rm $(prefix)/$(bin_name); 				\
	rm $(prefix)/$(entry_exec); 				\
	make -C $(libl3_path) -f $(l3_somk) uninstall; 		\
	make -C $(libl_path) -f $(l_somk) uninstall; 		\
	make -C $(libv_path) -f $(v_somk) uninstall; 		\
	make -C $(libu_path) -f $(u_somk) uninstall;		\
	make -C $(liblm_path) -f $(lmod_somk) uninstall
clean:
	make -C $(libl3_path) -f $(l3_somk) clean;       \
	make -C $(libl_path) -f $(l_somk) clean;       \
	make -C $(libv_path) -f $(v_somk) clean;       \
	make -C $(libu_path) -f $(u_somk) clean;       \
	make -C $(liblm_path) -f $(lmod_somk) clean; \
	rm -f $(bin_name) $(obj_list)

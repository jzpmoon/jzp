bin_name = l5$(suf_pg)
obj_list = l5.o

l5_lib = l5
l_lib = l
v_lib = v
u_lib = u
lm_lib = lm
ed_lib = ed
dbg_lib = dbg

libl5_path = ../l/libl5/
libl_path = ../l/
libv_path = ../v/
libu_path = ../u/
liblm_path = ../l/liblm/
libed_path = ../l/libed/
libdbg_path = ../l/libdbg/

somk = makefile

CFLAGS   = -std=c89 -Wall $(DEBUG_MODE)

&(bin_name):$(obj_list) $(l_lib) $(l5_lib) $(lm_lib) $(ed_lib) $(dbg_lib)
	$(CC) $(obj_list) -L$(libl5_path) -l$(l5_lib) -L$(libl_path) -l$(l_lib) -L$(libv_path) -l$(v_lib) -L$(libu_path) -l$(u_lib) -o $(bin_name)
.c.o:
	$(CC) -c -o $@ $< -I $(libl5_path) -I $(libl_path) -I $(libv_path) -I $(libu_path) $(CFLAGS)
$(l5_lib):
	make -C $(libl5_path) -f $(somk)
$(l_lib):
	make -C $(libl_path) -f $(somk)
$(lm_lib):
	make -C $(liblm_path) -f $(somk)
$(ed_lib):
	make -C $(libed_path) -f $(somk)
$(dbg_lib):
	make -C $(libdbg_path) -f $(somk)
install:
	cp $(bin_name) $(prefix)/; \
	make -C $(libl5_path) -f $(somk) install; \
	make -C $(libl_path) -f $(somk) install; \
	make -C $(libv_path) -f $(somk) install; \
	make -C $(libu_path) -f $(somk) install; \
	make -C $(liblm_path) -f $(somk) install; \
	make -C $(libed_path) -f $(somk) install; \
	make -C $(libdbg_path) -f $(somk) install
uninstall:
	rm $(prefix)/$(bin_name); \
	make -C $(libl5_path) -f $(somk) uninstall; \
	make -C $(libl_path) -f $(somk) uninstall; \
	make -C $(libv_path) -f $(somk) uninstall; \
	make -C $(libu_path) -f $(somk) uninstall; \
	make -C $(liblm_path) -f $(somk) uninstall; \
	make -C $(libed_path) -f $(somk) uninstall; \
	make -C $(libdbg_path) -f $(somk) uninstall
clean:
	make -C $(libl5_path) -f $(somk) clean; \
	make -C $(libl_path) -f $(somk) clean; \
	make -C $(libv_path) -f $(somk) clean; \
	make -C $(libu_path) -f $(somk) clean; \
	make -C $(liblm_path) -f $(somk) clean; \
	make -C $(libed_path) -f $(somk) clean; \
	make -C $(libdbg_path) -f $(somk) clean; \
	rm -f $(bin_name) $(obj_list)

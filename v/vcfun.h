#ifndef _VCFUN_H_
#define _VCFUN_H_

#include "vmacro.h"
#include "vcontext.h"

struct _vcfun_infor{
  char* lname;
  ustring* name;
  int params_count;
  unsigned char has_retval;
  unsigned char scope;
  vcfun_ft entry;
};

typedef vgc_obj*(vcall *var_gen_ft)();

struct _var_infor{
  char* lname;
  ustring* name;
  unsigned char scope;
  unsigned char mdf;
  var_gen_ft var_gen;
};

typedef void (vcall *vcfun_init_ft)(vcontext* ctx,vmod* mod);

#define VSCOPE_GLOBAL VPS_SCOPE_GLOBAL
#define VSCOPE_LOCAL VPS_SCOPE_LOCAL
#define VRETVAL_YES UTRUE
#define VRETVAL_NO UFALSE
#define VMDF_YES UTRUE
#define VMDF_NO UFALSE

#define VDEFUN(FNAME,LNAME,SCOPE,PCOUNT,RETVAL,BODY)            \
  UDEFUN(UFNAME _vcfun_entry_##FNAME,                           \
         UARGS (vcontext* ctx),                                 \
         URET int)                                              \
    UDECLARE                                                    \
    UBEGIN                                                      \
    BODY;                                                       \
  UEND                                                          \
  static struct _vcfun_infor _vcfun_infor_##FNAME =             \
    {LNAME,UNULL,PCOUNT,RETVAL,SCOPE,_vcfun_entry_##FNAME};

#define VDEFVAR(FNAME,LNAME,SCOPE,MDF,BODY)     \
  UDEFUN(UFNAME _var_gen_##FNAME,               \
         UARGS (vcontext* ctx),                 \
         URET vgc_obj* vcall)                   \
    UDECLARE                                    \
    UBEGIN                                      \
    BODY;                                       \
  UEND                                          \
  static struct _var_infor _var_infor_##FNAME = \
    {LNAME,UNULL,SCOPE,MDF,_var_gen_##FNAME};

#define VFUNONLOAD(fname,body)                  \
  UDEFUN(UFNAME _vcfun_file_init_##fname,       \
         UARGS (vcontext* ctx,vmod* mod),       \
         URET void)                             \
    UDECLARE                                    \
    UBEGIN                                      \
    body                                        \
    UEND

#define VDECLFUN(NAME) VFUN_INIT(ctx,mod,NAME)

#define VFUN_INIT(CTX,MOD,FNAME)                                \
  do{                                                           \
    vgc_cfun* cfun;                                             \
    ustring* str;                                               \
    vslot slot;                                                 \
    str = ustring_table_put(                                    \
      (CTX)->symtb,                                             \
      _vcfun_infor_##FNAME.lname,                               \
      -1);                                                      \
    if (!str) { uabort("init cfun error!"); }                   \
    _vcfun_infor_##FNAME.name = str;                            \
    cfun = vgc_cfun_new(                                        \
      (CTX)->heap,                                              \
      str,                                                      \
      _vcfun_infor_##FNAME.entry,                               \
      _vcfun_infor_##FNAME.params_count,                        \
      _vcfun_infor_##FNAME.has_retval,                          \
      vgc_heap_area_static);                                    \
    if (!cfun) {                                                \
      uabort("LFUN_INIT:cfun new error!");                      \
    }                                                           \
    slot = vgc_obj2ref2slot((CTX)->heap, (vgc_obj*)cfun, UTRUE);\
    vmod_lslot_put(                                             \
      (CTX)->heap,                                              \
      MOD,                                                      \
      _vcfun_infor_##FNAME.name,                                \
      slot);                                                    \
    if (_vcfun_infor_##FNAME.scope == VPS_SCOPE_GLOBAL) {       \
      vmod_gslot_put(                                           \
        (CTX)->heap,                                            \
        MOD,                                                    \
        _vcfun_infor_##FNAME.name,                              \
        slot);                                                  \
    }                                                           \
  }while(0)

#define VDECLVAR(NAME) VAR_INIT(ctx,mod,NAME)

#define VAR_INIT(CTX,MOD,VNAME)                                 \
  do{                                                           \
    ustring* str;                                               \
    vgc_obj* var;                                               \
    vslot slot;                                                 \
    str = ustring_table_put(                                    \
      (CTX)->symtb,                                             \
      _var_infor_##VNAME.lname,                                 \
      -1);                                                      \
    if (!str) { uabort("init var error!"); }                    \
    var = _var_infor_##VNAME.var_gen(CTX);                      \
    slot = vgc_obj2ref2slot((CTX)->heap, var,                   \
      !_var_infor_##VNAME.mdf);                                 \
    if (var) {                                                  \
      vmod_lslot_put((CTX)->heap, MOD, str, slot);              \
      if (_var_infor_##VNAME.scope == VPS_SCOPE_GLOBAL) {       \
        vmod_gslot_put((CTX)->heap, MOD, str, slot);            \
      }                                                         \
    }                                                           \
  } while(0)

#endif

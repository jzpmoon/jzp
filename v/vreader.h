#ifndef _VREADER_H_
#define _VREADER_H_

#include "vreader.in.h"

#ifndef _UMAP_TPL_VAST_ATTR_
#define _UMAP_TPL_VAST_ATTR_
#endif

#ifndef _UHSTB_TPL_VAST_ATTR_
#define _UHSTB_TPL_VAST_ATTR_
#endif

#ifndef _UMAP_TPL_VAST_KW_
#define _UMAP_TPL_VAST_KW_
#endif

#ifndef _ULIST_TPL_VAST_KW_
#define _ULIST_TPL_VAST_KW_
#endif

#ifndef _UMAP_TPL_USTRINGP_
#define _UMAP_TPL_USTRINGP_
#endif

#ifndef _ULIST_TPL_USTRINGP_
#define _ULIST_TPL_USTRINGP_
#endif

#endif

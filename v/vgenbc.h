#ifndef _VGENBC_H_
#define _VGENBC_H_

#include "vbytecode.h"
#include "vgc_obj.h"
#include "vpass.h"

enum vbytecode{
  #define DF(code,name,value,len,oct) \
    code=value,
    VBYTECODE
  #undef DF
};

#define vopdnon (-1)

UDECLFUN(UFNAME vps_inst_to_str,
         UARGS (vgc_heap* heap, ulist_vps_instp* insts),
         URET vgc_string*);

UDECLFUN(UFNAME vps_inst_to_str_pos,
         UARGS (ulist_vps_instp* insts,vps_inst* inst),
         URET usize_t);

#endif

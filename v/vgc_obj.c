#include "uref.h"
#include "ualloc.h"
#include "uerror.h"
#include "vgc_obj.h"

UDEFUN(UFNAME vslot_to_str,
       UARGS (vslot slot),
       URET vapi char* vcall)
UDECLARE

UBEGIN
  if (vslot_is_ref(slot)) {
    vgc_obj* obj = vslot_ref_get(slot,vgc_obj);
    if (!obj) {
      return "nil";
    }
    #undef DF
    #define DF(t) if(vgc_obj_typeof(obj,t)) return "["#t"]";
    VGC_OBJ_TYPE_DEFINE
    #undef DF
  } else {
    #undef DF
    #define DF(t) if(vslot_typeof(slot,t)) return "["#t"]";
    VSLOT_TYPE_DEFINE
    #undef DF
  }
  return UNULL;
UEND
  
UDEFUN(UFNAME vgc_array_new,
       UARGS (vgc_heap* heap,
              usize_t array_length,
              int area_type),
       URET vapi vgc_array* vcall)
UDECLARE
  usize_t array_size = TYPE_SIZE_OF(vgc_array,vslot,array_length);
  usize_t offset = uoffsetof(vgc_array,objs);
  vgc_array* array;
  vslot* slot_list;
  usize_t i = 0;
UBEGIN
  array = (vgc_array*)vgc_heap_data_new(
    heap,
    array_size,
    offset,
    array_length,
    vgc_obj_type_array,
    area_type);

  if (array) {
    array->top = 0;
    slot_list = vgc_obj_slot_list(array);
    while (i < array_length) {
      vslot_null_set(slot_list[i]);
      i++;
    }
  }

  return array;
UEND

UDEFUN(UFNAME vgc_array_push,
       UARGS (vgc_array* array,vslot slot),
       URET vapi int vcall)
UDECLARE

UBEGIN
  if (array->top <= array->_len) {
    array->objs[array->top] = slot;
    return array->top++;
  } else {
    return -1;
  }
UEND

UDEFUN(UFNAME vgc_array_pop,
       UARGS (vgc_array* array),
       URET vapi vslot vcall)
UDECLARE

UBEGIN
  if (array->top > 0) {
    array->top--;
  }
  return array->objs[array->top];
UEND

UDEFUN(UFNAME vgc_array_set,
       UARGS (vgc_array* array,usize_t idx,vslot slot),
       URET vapi void vcall)
UDECLARE

UBEGIN
  if (idx < array->_len) {
    array->objs[idx] = slot;
  }
UEND

UDEFUN(UFNAME vgc_string_new,
       UARGS (vgc_heap* heap,
              usize_t string_length,
              int area_type),
       URET vapi vgc_string* vcall)
UDECLARE
  usize_t string_size = TYPE_SIZE_OF(vgc_string,char,string_length);
  vgc_string* string;
UBEGIN
  string = (vgc_string*)vgc_heap_data_new(
    heap,
    string_size,
    0,
    0,
    vgc_obj_type_string,
    area_type);

  if (string) {
    string->len = string_length;
  }

  return string;
UEND

UDEFUN(UFNAME vgc_ustr2vstr,
       UARGS (vgc_string* vstr,ustring* ustr),
       URET vapi void vcall)
UDECLARE

UBEGIN
  ustdc_memcpy(vstr->u.b,ustr->value,ustr->len);
UEND

UDEFUN(UFNAME vgc_ref_new,
       UARGS (vgc_heap* heap,vslot slot),
       URET vapi vgc_ref* vcall)
UDECLARE
  vgc_ref* ref;
UBEGIN
  ref = vgc_heap_obj_new(
    heap,vgc_ref,vgc_obj_type_ref,vgc_heap_area_static);
  if (ref) {
    vgc_slot_set(ref,ref,slot);
  }
  return ref;
UEND

UDEFUN(UFNAME vgc_obj2ref2slot,
       UARGS (vgc_heap* heap,vgc_obj* obj,int const_flag),
       URET vapi vslot vcall)
UDECLARE
  vslot slot;
  vgc_ref* ref;
  vslot sym_slot;
UBEGIN
  vslot_ref_set(slot,obj);
  ref = vgc_ref_new(heap,slot);
  if (!ref) {
    uabort("ref new error!");
  }
  vslot_ref_set(sym_slot,ref);
  if (const_flag) {
    vslot_const_yes(vgc_slot_get(ref,ref));
  } else {
    vslot_const_no(vgc_slot_get(ref,ref));
  }
  return sym_slot;
UEND

UDEFUN(UFNAME vslot2ref2slot,
       UARGS (vgc_heap* heap,vslot slot),
       URET vapi vslot vcall)
UDECLARE
  vgc_ref* ref;
  vslot sym_slot;
UBEGIN
  ref = vgc_ref_new(heap,slot);
  if (!ref) {
    uabort("ref new error!");
  }
  vslot_ref_set(sym_slot,ref);
  if (vslot_is_const(slot)) {
    vslot_const_yes(vgc_slot_get(ref,ref));
  } else {
    vslot_const_no(vgc_slot_get(ref,ref));
  }
  return sym_slot;
UEND

UDEFUN(UFNAME vslot_num_eq,
       UARGS (vslot slot1,vslot slot2),
       URET vslot)
UDECLARE
  double num1 = vslot_num_get(slot1);
  double num2 = vslot_num_get(slot2);
  vslot bool;
UBEGIN
  if (num1 == num2) {
    vslot_bool_set(bool,vbool_true);
  } else {
    vslot_bool_set(bool,vbool_false);
  }
  return bool;
UEND

UDEFUN(UFNAME vslot_int_eq,
       UARGS (vslot slot1,vslot slot2),
       URET vslot)
UDECLARE
  int int1 = vslot_int_get(slot1);
  int int2 = vslot_int_get(slot2);
  vslot bool;
UBEGIN
  if (int1 == int2) {
    vslot_bool_set(bool,vbool_true);
  } else {
    vslot_bool_set(bool,vbool_false);
  }
  return bool;
UEND

UDEFUN(UFNAME vslot_ref_eq,
       UARGS (vslot slot1,vslot slot2),
       URET vslot)
UDECLARE
  vgc_obj* ref1 = vslot_ref_get(slot1,vgc_obj);
  vgc_obj* ref2 = vslot_ref_get(slot2,vgc_obj);
  vslot bool;
UBEGIN
  if (ref1 == ref2) {
    vslot_bool_set(bool,vbool_true);
  } else {
    vslot_bool_set(bool,vbool_false);
  }
  return bool;
UEND

UDEFUN(UFNAME vgc_exception_new,
       UARGS (vgc_heap* heap,vslot code,vslot desc),
       URET vapi vgc_exception* vcall)
UDECLARE
  vgc_exception* ex;
UBEGIN
  ex = vgc_heap_obj_new(
    heap,vgc_exception,vgc_obj_type_exception,vgc_heap_area_static);
  if (ex) {
    vgc_slot_set(ex,code,code);
    vgc_slot_set(ex,desc,desc);
  }
  return ex;
UEND

UDEFUN(UFNAME vgc_extend_new,
       UARGS (vgc_heap* heap,
              usize_t struct_size,
              usize_t ref_offset,
              usize_t ref_count,
              vgc_objex_t* oet),
       URET vapi vgc_extend* vcall)
UDECLARE
  vgc_extend* extend;
UBEGIN
  extend = (vgc_extend*) vgc_heap_data_new(
    heap,
    struct_size,
    ref_offset,
    ref_count,
    vgc_obj_type_extend,
    vgc_heap_area_active);

  if (extend) {
    extend->oet = oet;
  }

  return extend;
UEND

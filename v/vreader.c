#include "ulist_tpl.c"
#include "vreader.in.h"

#ifndef _ULIST_TPL_VAST_KW_
ulist_def_tpl(v,vast_kw);
#endif

#ifndef _ULIST_TPL_USTRINGP_
ulist_def_tpl(v,ustringp);
#endif

vapi vreader* vcall
vreader_new(ustring_table* symtb,
            ustring_table* strtb,
            vattr_init_ft ainit,
            vast_attr* dattr)
{
  uhstb_vast_attr* attrtb;
  ulist_vast_kw* kws;
  ubuffer* buff;
  vreader* reader;

  reader = ualloc(sizeof(vreader));
  if (!reader) {
    uabort("vreader new error!");
  }

  buff = ubuffer_new(VTOKEN_BUFF_SIZE);
  if (!buff) {
    uabort("ubuffer new error!");
  }

  attrtb = uhstb_vast_attr_new(-1);
  if (!attrtb) {
    uabort("attr table new error!");
  }

  kws = ulist_vast_kw_new();
  if (!kws) {
    uabort("keyword list new error!");
  }

  umem_pool_init(&reader->mp, -1);

  reader->symtb = symtb;
  reader->strtb = strtb;
  reader->buff = buff;
  reader->attrtb = attrtb;
  reader->ainit = ainit;
  reader->dattr = dattr;
  reader->kws = kws;
  reader->fi.file_path = UNULL;
  reader->fi.file_name = UNULL;
  reader->fi.dir_name = UNULL;

  if (ainit) ainit(reader);

  return reader;
}

vapi vreader* vcall
vreader_easy_new(vattr_init_ft ainit)
{
  ustring_table* symtb;
  ustring_table* strtb;
 
  symtb = ustring_table_new(-1);
  if(!symtb){
    uabort("symtb new error!");
  }

  strtb = ustring_table_new(-1);
  if(!strtb){
    uabort("strtb new error!");
  }

  return vreader_new(symtb,strtb,ainit,UNULL);
}

vapi int vcall
vreader_keyword_put(vreader* reader,vast_kw keyword)
{
  return ulist_vast_kw_append(reader->kws,keyword);
}

vapi vtoken_state* vcall
vreader_from(vreader* reader)
{
  uallocator* allocator;
  ustream* stream;
  vtoken_state* ts;

  allocator = vreader_alloc_get(reader);
  stream = ustream_alloc(allocator,USTREAM_INPUT,USTREAM_FILE,!USTREAM_READ_LINE);
  if (!stream) {
    return UNULL;
  }
  ts = vtoken_state_alloc(allocator,
                          stream,
                          reader);
  return ts;
}

vapi vtoken_state* vcall
vreader_from_stream(vreader* reader, ustream* stream)
{
  uallocator* allocator;
  vtoken_state* ts;

  if (!ustream_iot_of(stream, USTREAM_INPUT)) {
    return UNULL;
  }
  allocator = vreader_alloc_get(reader);
  ts = vtoken_state_alloc(allocator,
                          stream,
                          reader);
  return ts;
}

vapi void vcall
vreader_clean(vreader* reader)
{
  uallocator* allocator;
  
  allocator = vreader_alloc_get(reader);
  allocator->clean(allocator);
}

vapi ustring* vcall
vreader_path_get(vreader* reader,ustring* name)
{
  ustring* path;
  
  if (reader->fi.dir_name) {
    path = ustring_concat_by_strtb(reader->symtb,
                                   reader->fi.dir_name,name);
    if (!path) {
      uabort("mod name concat error!");
    }
  } else {
    path = name;
  }
  return path;
}

UDEFUN(UFNAME vreader_fi_init,
       UARGS (vreader* reader,
              ustring* base_path,
              ustring* file_path),
       URET vapi int vcall)
UDECLARE
  ustring* file_full_path;
UBEGIN
  if (base_path) {
    file_full_path = ustring_concat_by_strtb(reader->symtb,
                                             base_path,file_path);
    if (!file_full_path) {
      return -1;
    }
  } else {
    file_full_path = file_path;
  }
  if (!ufile_init_by_strtb(reader->symtb,&reader->fi,file_full_path)) {
    return -1;
  }
  return 0;
UEND

vapi int vcall
vast_attr_call(vast_attr* attr,vast_attr_req* req,vast_attr_res* res)
{
  int retval;
  vast_attr_ft req_from;

  if (!attr || !attr->action) {
    return -1;
  }
  req_from = req->req_from;
  req->req_from = req->req_this;
  retval = attr->action(req,res);
  req->req_this = req->req_from;
  req->req_from = req_from;
  return retval;
}

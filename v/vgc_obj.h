#ifndef _VGC_OBJ_H_
#define _VGC_OBJ_H_

#include "udef.h"
#include "ustring_table.h"
#include "ubuffer.h"
#include "ustack_tpl.h"
#include "uhstb_tpl.h"
#include "ulist_tpl.h"
#include "vmacro.h"

#define VGC_OBJ_TYPE_DEFINE \
  DF(vgc_obj_type_array) \
  DF(vgc_obj_type_string) \
  DF(vgc_obj_type_cfun) \
  DF(vgc_obj_type_subr) \
  DF(vgc_obj_type_call) \
  DF(vgc_obj_type_ctx) \
  DF(vgc_obj_type_extend) \
  DF(vgc_obj_type_ref) \
  DF(vgc_obj_type_exception) \
  DF(vgc_obj_type_closure)

enum {
  #undef DF
  #define DF(t) t,
  VGC_OBJ_TYPE_DEFINE
  #undef DF
};

#define vgc_obj_typeof(O,T) \
  (((O)->_mark.t==(T)))

struct mark_t{
  unsigned char m : 1;
  unsigned char a : 1;
  unsigned char l : 1;
  unsigned char t : 5;
};

#define VGCHEADER \
  struct _vgc_obj* _addr; \
  usize_t _size; \
  struct mark_t _mark

#define VGCLHEADER \
  VGCHEADER; \
  unsigned char _offset; \
  usize_t _len

#define vgc_obj_is_list(obj) \
  ((obj)->_mark.l)

typedef struct _vgc_obj{
  VGCHEADER;
} vgc_obj,* vgc_objp;

typedef struct _vgc_list{
  VGCLHEADER;
} vgc_list,* vgc_listp;

#define vgc_obj_offset(obj) \
  ((obj)->_mark.l ? ((vgc_list*)(obj))->_offset : 0)

#define vgc_obj_len(obj) \
  ((obj)->_mark.l ? ((vgc_list*)(obj))->_len : 0)

#define vgc_obj_ref_check(obj,index) \
  (index < 0 || index >= vgc_obj_len(obj))

#define vgc_obj_log(obj) \
  do{ \
    udebug0("*******vgc_obj_log"); \
    udebug1("addr  : %p",obj); \
    udebug1("type  : %d",obj->_mark.t); \
    udebug1("size  : %d",obj->_size); \
    udebug1("_addr : %d",obj->_addr); \
    if (obj->_mark.l) { \
      udebug1("offset: %d",vgc_obj_offset(obj)); \
      udebug1("len   : %d",vgc_obj_len(obj)); \
    } \
  } while(0)

#define VSLOT_TYPE_DEFINE \
  DF(vslot_type_ref) \
  DF(vslot_type_num) \
  DF(vslot_type_int) \
  DF(vslot_type_char) \
  DF(vslot_type_bool)

enum{
  #undef DF
  #define DF(t) t,
  VSLOT_TYPE_DEFINE
  #undef DF
};

typedef struct _vslot{
  unsigned char t; /* slot type */
  unsigned char c; /* const flag */
  union {
    double num;
    vgc_obj* ref;
    int inte;
    int bool;
    int chara;
  } u;
} vslot;

#define vslot_typeof(S,T) \
  ((S).t == (T))

#define vslot_const_yes(S) \
  ((S).c = 1)

#define vslot_const_no(S) \
  ((S).c = 0)

#define vslot_is_const(S) \
  ((S).c)

#define vbool_true (1)
#define vbool_false (0)

#define vslot_define_begin \
  struct{
#define vslot_define(type,name) \
  vslot name
#define vslot_define_end \
  } _u;
#define vslot_is_num(slot) \
  ((slot).t == vslot_type_num)
#define vslot_is_int(slot) \
  ((slot).t == vslot_type_int)
#define vslot_is_char(slot) \
  ((slot).t == vslot_type_char)
#define vslot_is_bool(slot) \
  ((slot).t == vslot_type_bool)
#define vslot_is_ref(slot) \
  ((slot).t == vslot_type_ref)
#define vslot_is_null(slot) \
  ((slot).u.ref == UNULL)
#define vslot_bool_get(slot) \
  ((slot).u.bool)
#define vslot_bool_set(slot,val) \
  (slot.t = vslot_type_bool,slot.u.bool = val)
#define vslot_num_get(slot) \
  ((slot).u.num)
#define vslot_num_set(slot,val) \
  (slot.t = vslot_type_num,slot.u.num = val)
#define vslot_int_get(slot) \
  ((slot).u.inte)
#define vslot_int_set(slot,val) \
  (slot.t = vslot_type_int,slot.u.inte = val)
#define vslot_char_get(slot) \
  ((slot).u.chara)
#define vslot_char_set(slot,val) \
  (slot.t = vslot_type_char,slot.u.chara = val)
#define vslot_ref_get(slot,obj_type) \
  ((obj_type*)(slot).u.ref)
#define vslot_ref_set(slot,obj) \
  (slot.t = vslot_type_ref, \
   slot.u.ref = (vgc_obj*)obj)
#define vslot_null_set(slot) \
  ((slot).t = vslot_type_ref, \
   (slot).u.ref = UNULL)
#define vslot_is_true(slot) \
  (vslot_is_bool(slot) && (slot).u.bool)

#define vslot_log(slot) \
  udebug1("slot type:%d",(slot).t); \
  udebug1("slot const:%d",(slot).c); \
  udebug1("slot num:%f",(slot).u.num); \
  udebug1("slot int:%d",(slot).u.inte); \
  udebug1("slot char:%c",(slot).u.chara); \
  udebug1("slot bool:%d",(slot).u.bool); \
  udebug1("slot ref:%p",(slot).u.ref)

UDECLFUN(UFNAME vslot_to_str,
         UARGS (vslot slot),
         URET vapi char* vcall);

UDECLFUN(UFNAME vslot_num_eq,
         UARGS (vslot slot1,vslot slot2),
         URET vslot);

UDECLFUN(UFNAME vslot_int_eq,
         UARGS (vslot slot1,vslot slot2),
         URET vslot);

UDECLFUN(UFNAME vslot_ref_eq,
         UARGS (vslot slot1,vslot slot2),
         URET vslot);

enum{
  vgc_heap_area_static,
  vgc_heap_area_active,
};

#ifndef _USTACK_TPL_VSLOT_
ustack_decl_tpl(v,vslot);
#endif
#ifndef _USTACK_TPL_VGC_OBJP_
ustack_decl_tpl(v,vgc_objp);
#endif
  
typedef struct _vgc_heap_area{
  vgc_obj* area_begin;
  vgc_obj* area_index;
  usize_t area_size;
} vgc_heap_area;
  
typedef struct _vgc_heap{
  usize_t heap_size;
  ustack_vslot root_set;
  ustack_vgc_objp stack;
  vgc_heap_area area_static;
  vgc_heap_area area_active;
  char heap_data[1];
} vgc_heap;

UDECLFUN(UFNAME vgc_heap_new,
         UARGS (usize_t area_static_size,
                usize_t area_active_size,
                usize_t root_set_size),
         vapi vgc_heap* vcall);

UDECLFUN(UFNAME vgc_heap_data_new,
         UARGS (vgc_heap* heap,
                usize_t obj_size,
                usize_t ref_offset,
                usize_t ref_count,
                int obj_type,
                int area_type),
         URET vapi vgc_obj* vcall);
         
#define vgc_obj_solt_offset(type) \
  ((usize_t)&((type*)0)->_u)

#define vgc_obj_slot_count(type) \
  (sizeof(((type*)0)->_u)/sizeof(vslot))

#define vgc_heap_obj_new(heap,type,obj_type,area_type) \
  (type*)vgc_heap_data_new( \
    heap, \
    sizeof(type), \
    vgc_obj_solt_offset(type), \
    vgc_obj_slot_count(type), \
    obj_type,area_type)

#define vgc_slot_get(obj,slot) \
  ((obj)->_u.slot)

#define vgc_slot_set(obj,slot,slot_val) \
  ((obj)->_u.slot = slot_val)

#define vgc_obj_slot_list(obj) \
  (vslot*)((char*)(obj) + (obj)->_offset)

#define vgc_obj_null_set(obj,slot) \
  vslot_null_set((obj)->_u.slot)

#define vgc_obj_ref_get(obj,slot,obj_type) \
  vslot_ref_get((obj)->_u.slot,obj_type)

#define vgc_obj_ref_set(obj,slot,vobj) \
  vslot_ref_set((obj)->_u.slot,vobj)

typedef struct _vgc_array{
  VGCLHEADER;
  usize_t top;
  vslot objs[1];
} vgc_array;

UDECLFUN(UFNAME vgc_array_new,
         UARGS (vgc_heap* heap,
                usize_t array_length,
                int area_type),
         URET vapi vgc_array* vcall);

UDECLFUN(UFNAME vgc_array_push,
         UARGS (vgc_array* array,vslot slot),
         URET vapi int vcall);

UDECLFUN(UFNAME vgc_array_pop,
         UARGS (vgc_array* array),
         URET vapi vslot vcall);

UDECLFUN(UFNAME vgc_array_set,
         UARGS (vgc_array* array,usize_t idx,vslot slot),
         URET vapi void vcall);

typedef struct _vgc_string{
  VGCHEADER;
  usize_t len;
  union{
    char c[1];
    unsigned char b[1];
  } u;
} vgc_string;

UDECLFUN(UFNAME vgc_string_new,
         UARGS (vgc_heap* heap,
                usize_t string_length,
                int area_type),
         URET vapi vgc_string* vcall);

UDECLFUN(UFNAME vgc_ustr2vstr,
         UARGS (vgc_string* vstr,ustring* ustr),
         URET vapi void vcall);

#define vgc_str_bound_check(obj,index) \
  (index >= 0 && index < obj->len)

typedef struct _vgc_ref{
  VGCLHEADER;
  vslot_define_begin
  vslot_define(vgc_obj,ref);
  vslot_define_end
} vgc_ref;

UDECLFUN(UFNAME vgc_ref_new,
         UARGS (vgc_heap* heap,vslot slot),
         URET  vapi vgc_ref* vcall);

UDECLFUN(UFNAME vgc_obj2ref2slot,
         UARGS (vgc_heap* heap,vgc_obj* obj,int const_flag),
         URET vapi vslot vcall);

UDECLFUN(UFNAME vslot2ref2slot,
         UARGS (vgc_heap* heap,vslot slot),
         URET vapi vslot vcall);

typedef struct _vgc_exception{
  VGCLHEADER;
  vslot_define_begin
  vslot_define(vgc_obj,code);
  vslot_define(vgc_obj,desc);
  vslot_define_end
} vgc_exception;

UDECLFUN(UFNAME vgc_exception_new,
         UARGS (vgc_heap* heap,vslot code,vslot desc),
         URET vapi vgc_exception* vcall);

typedef struct _vgc_obj_ex_t{
  char* type_name;
} vgc_objex_t;

#define VGCHEADEREX \
  VGCHEADER; \
  vgc_objex_t* oet

#define VGCLHEADEREX \
  VGCLHEADER; \
  vgc_objex_t* oet

typedef struct _vgc_extend{
  VGCHEADEREX;
} vgc_extend;

typedef struct _vgc_list_extend{
  VGCLHEADEREX;
} vgc_list_extend;

UDECLFUN(UFNAME vgc_extend_new,
         UARGS (vgc_heap* heap,
                usize_t struct_size,
                usize_t ref_offset,
                usize_t ref_count,
                vgc_objex_t* oet),
         URET vapi vgc_extend* vcall);

#define vgc_objvex_new(heap,stype,oet) \
  (stype*) vgc_extend_new(heap,sizeof(stype),0,0,oet)

#define vgc_objfex_new(heap,stype,oet) \
  (stype*) vgc_extend_new( \
    heap, \
    sizeof(stype), \
    vgc_obj_slot_offset(stype), \
    vgc_obj_slot_count(stype), \
    oet)

#endif

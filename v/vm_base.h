#ifndef _VM_BASE_H_
#define _VM_BASE_H_

#include "vpass.h"

typedef struct _vm_base vm_base;

#define VMHEADER \
  vps_cntr vps; \
  uallocator* allocator

struct _vm_base{
  VMHEADER;
};

#endif

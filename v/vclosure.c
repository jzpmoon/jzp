#include "ulist_tpl.c"
#include "vparser.h"
#include "vclosure.h"

#ifndef _ULIST_TPL_VCLOSUREP_
ulist_def_tpl(v,vclosurep);
#endif
#ifndef _ULIST_TPL_VCLOSURE_DATAP_
ulist_def_tpl(v,vclosure_datap);
#endif

UDEFUN(UFNAME vclosure_base_new,
       UARGS (vps_cntr* vps,
              int struct_size),
       URET vapi vclosure_base* vcall)
UDECLARE
  uallocator* allocator;
  vclosure_base* closure;
UBEGIN
  allocator = vps_cntr_alloc_get(vps);
  closure = allocator->alloc(allocator,struct_size);
  if (closure) {
    closure->change_flag = 1;
    closure->parent = UNULL;
  }
  return closure;
UEND

UDEFUN(UFNAME vclosure_new,
       UARGS (vps_cntr* vps,
              int struct_size),
       URET vapi vclosure* vcall)
UDECLARE
  uallocator* allocator;
  vclosure* closure;
  ulist_vclosure_datap* fields;
  ulist_vclosurep* childs;
UBEGIN
  allocator = vps_cntr_alloc_get(vps);
  closure = allocator->alloc(allocator,struct_size);
  if (closure) {
    fields = ulist_vclosure_datap_alloc(allocator);
    if (!fields) {
      goto err;
    }
    childs = ulist_vclosurep_alloc(allocator);
    if (!childs) {
      goto err;
    }
    closure->change_flag = 1;
    closure->parent = UNULL;
    closure->fields = fields;
    closure->childs = childs;
  }

  return closure;
 err:
  return UNULL;
UEND

UDEFUN(UFNAME vclosure_file_new,
       UARGS (vps_cntr* vps,
              int closure_type,
              ustring* name,
              ustring* path),
       URET vapi vclosure_file* vcall)
UDECLARE
  vclosure_file* file;
  vcfg_graph* init;
UBEGIN
  if (closure_type != VCLOSURE_TYPE_FILE &&
      closure_type != VCLOSURE_TYPE_MAIN) {
    return UNULL;
  }
  file = vclosure_obj_new(vps,vclosure_file);
  if (file) {
    init = vcfg_graph_new(vps,UNULL);
    if (!init) {
      return UNULL;
    }
    file->closure_type = closure_type;
    file->named_flag = 1;
    file->prog_flag = 1;
    file->closure_name = name;
    file->closure_path = path;
    file->init = init;
  }
  return file;
UEND

UDEFUN(UFNAME vclosure_file_name_set,
       UARGS (vclosure_file* file,
              ustring* closure_name),
       URET vapi void vcall)
UDECLARE

UBEGIN
  file->closure_name = closure_name;
UEND

UDEFUN(UFNAME vclosure_func_new,
       UARGS (vps_cntr* vps,
              ustring* name,
              int store_type,
              int scope),
       URET vapi vclosure_func* vcall)
UDECLARE
  vclosure_func* func;
  vcfg_graph* init;
UBEGIN
  func = vclosure_obj_new(vps,vclosure_func);
  if (func) {
    init = vcfg_graph_new(vps,UNULL);
    if (!init) {
      return UNULL;
    }
    func->closure_type = VCLOSURE_TYPE_FUNC;
    func->named_flag = 1;
    func->prog_flag = 1;
    func->store_type = store_type;
    func->closure_flag = VCLOSURE_FUNC_NO;
    func->scope = scope;
    func->closure_name = name;
    func->init = init;
  }
  return func;
UEND

UDEFUN(UFNAME vclosure_none_new,
       UARGS (vps_cntr* vps),
       URET vapi vclosure_none* vcall)
UDECLARE
  vclosure_none* none;
UBEGIN
  none = vclosure_obj_new(vps,vclosure_none);
  if (none) {
    none->closure_type = VCLOSURE_TYPE_NONE;
    none->named_flag = 0;
    none->prog_flag = 0;
  }
  return none;
UEND

UDEFUN(UFNAME vclosure_stru_new,
       UARGS (vps_cntr* vps,
              ustring* name,
              int scope),
       URET vapi vclosure_stru* vcall)
UDECLARE
  vclosure_stru* stru;
UBEGIN
  stru = vclosure_obj_new(vps,vclosure_stru);
  if (stru) {
    stru->closure_type = VCLOSURE_TYPE_STRU;
    stru->named_flag = 1;
    stru->prog_flag = 0;
    stru->scope = scope;
    stru->closure_name = name;
  }
  return stru;
UEND

UDEFUN(UFNAME vclosure_code_new,
       UARGS (vps_cntr* vps,
              vclosure_prog* prog),
       URET vapi vclosure_code* vcall)
UDECLARE
  vclosure_code* code;
UBEGIN
  code = vclosure_obj_new(vps,vclosure_code);
  if (code) {
    code->closure_type = VCLOSURE_TYPE_CODE;
    code->named_flag = 0;
    code->prog_flag = 0;
    code->prog = prog;
  }
  return code;
UEND

UDEFUN(UFNAME vclosure_data_new_by_vps_data,
       UARGS (vps_cntr* vps,
              int store_type,
              vps_data* dt),
       URET vapi vclosure_data* vcall)
UDECLARE
  vclosure_data* data;
UBEGIN
  data = vclosure_base_obj_new(vps,vclosure_data);
  if (data) {
    data->closure_type = VCLOSURE_TYPE_DATA;
    data->named_flag = 0;
    data->prog_flag = 0;
    data->store_type = store_type;
    data->scope = vps_data_scope_get(dt);
    data->closure_name = dt->id.name;
    data->data = dt;
  }
  return data;
UEND

UDEFUN(UFNAME vclosure_data_new,
       UARGS (vps_cntr* vps,
              ustring* name,
              int store_type,
              int scope),
       URET vapi vclosure_data* vcall)
UDECLARE
  vclosure_data* data;
  vps_data* dt;
UBEGIN
  data = vclosure_base_obj_new(vps,vclosure_data);
  if (data) {
    data->closure_type = VCLOSURE_TYPE_DATA;
    data->named_flag = 0;
    data->prog_flag = 0;
    data->store_type = store_type;
    data->scope = scope;
    data->closure_name = name;
    dt = vps_any_new(vps,name);
    vps_data_scope_set(dt, scope);
    data->data = dt;
  }
  return data;
UEND

UDEFUN(UFNAME vclosure2mod,
       UARGS (vclosure* closure,vps_cntr* vps,vps_mod* mod),
       URET vapi void vcall)
UDECLARE
  uset_vclosure_datap* set_fields;
  uset_vclosurep* set_childs;
  ucursor c;
  vps_mod* new_mod;
  vclosure_stru* stru;
  vclosure_file* file;
  vclosure_func* func;
  vclosure_data* data;
UBEGIN
  if (!vclosure_is_change(closure)) {
    return;
  }
  switch (closure->closure_type) {
    case VCLOSURE_TYPE_NONE:
      new_mod = UNULL;
      break;
    case VCLOSURE_TYPE_STRU:
      stru = (vclosure_stru*)closure;
      new_mod = mod;
      /*fields*/
      set_fields = (uset_vclosure_datap*)stru->fields;
      set_fields->iterate(&c);
      while (1) {
        vclosure_datap* fieldp = set_fields->next(set_fields,&c);
        vclosure_data* field;
        if (!fieldp) {
          break;
        }
        field = *fieldp;
        vclosure2mod((vclosure*)field,vps,new_mod);
      }
      break;
    case VCLOSURE_TYPE_FILE:
      file = (vclosure_file*)closure;
      new_mod = vps_mod_new(vps,file->closure_name,file->closure_path);
      if (!new_mod) {
        uabort("mod new error!");
      }
      goto label;
    case VCLOSURE_TYPE_MAIN:
      file = (vclosure_file*)closure;
      new_mod = vps_mod_new(vps,file->closure_name,file->closure_path);
      if (!new_mod) {
        uabort("mod new error!");
      }
      vps_mod_entry_set(new_mod);
    label:
      vps_cntr_load(vps,new_mod);
      vps_mod_loaded(new_mod);
      /*fields*/
      set_fields = (uset_vclosure_datap*)file->fields;
      set_fields->iterate(&c);
      while (1) {
        vclosure_datap* fieldp = set_fields->next(set_fields,&c);
        vclosure_data* field;
        if (!fieldp) {
          break;
        }
        field = *fieldp;
        vclosure2mod((vclosure*)field,vps,new_mod);
      }
      /*entry*/
      new_mod->entry = file->init;
      break;
    case VCLOSURE_TYPE_FUNC:
      func = (vclosure_func*)closure;
      if (vclosure_isdecl(func)) {
        return;
      }
      new_mod = mod;
      vps_mod_code_put(new_mod,func->init);
      break;
    case VCLOSURE_TYPE_CODE:
      break;
    case VCLOSURE_TYPE_DATA:
      new_mod = mod;
      data = (vclosure_data*)closure;
      if (data->store_type != VCLOSURE_STORE_DECL) {
        vps_mod_data_put(new_mod,data->data);
      }
      closure->change_flag = 0;
      return;
    default:
      uabort("unknow closure type!");
  }
  set_childs = (uset_vclosurep*)closure->childs;
  set_childs->iterate(&c);
  while (1) {
    vclosurep* childp = set_childs->next(set_childs,&c);
    vclosure* child;
    if (!childp) {
      break;
    }
    child = *childp;
    /*childs*/
    if (child) {
      vclosure2mod(child,vps,new_mod);
    }
  }
  closure->change_flag = 0;
UEND

UDEFUN(UFNAME vfile2closure,
       UARGS (vps_closure_req* req,
              vclosure* parent,
              vreader* reader,
              ustring* name,
              ustring* path,
              vps_cntr* vps,
              vps_data* cmd_args,
              int closure_type),
       URET vapi vclosure* vcall)
UDECLARE
  vclosure* closure;
  vclosure_data* data;
  vast_attr_res res;
  vcfg_graph* init;
  vps_inst* inst;
UBEGIN
  closure = (vclosure*)vclosure_file_new(vps,
                                         closure_type,
                                         name,
                                         path);
  if (!closure) {
    uabort("closure new error!");
  }
  if (parent) {
    if (vclosure_child_add(parent,closure)) {
      uabort("parent closure add child error!");
    }
  }
  if (cmd_args) {
    data = vclosure_data_new_by_vps_data(vps,VCLOSURE_STORE_IMPL,cmd_args);
    if (!data) {
      uabort("vclosure_data_new_by_vps_data error!");
    }
    if (vclosure_field_add(closure,data)) {
      uabort("closure add cmd args error!");
    }
  }
  init = ((vclosure_file*)closure)->init;
  init->scope = VPS_SCOPE_ENTRY;
  vast_attr_req_init(*req, reader);
  req->vps = vps;
  req->closure = closure;
  if (vfile2obj(reader,path,(vast_attr_req*)req,&res)) {
    uabort("file2obj error!");
  }

  inst = vps_iretvoid(vps);
  vcfg_grp_inst_apd(init,inst);
  vcfg_grp_build(vps,init);
  vcfg_grp_connect(vps,init);

  return closure;
UEND

UDEFUN(UFNAME vstream2closure,
       UARGS (vps_closure_req* req,
              vclosure* parent,
              vreader* reader,
              ustring* name,
              ustring* path,
              vps_cntr* vps,
              ustream* stream),
       URET vapi vclosure* vcall)
UDECLARE
  vclosure* closure;
  vast_attr_res res;
  vcfg_graph* init;
  vps_inst* inst;
UBEGIN
  closure = (vclosure*)vclosure_file_new(vps,
                                         VCLOSURE_TYPE_MAIN,
                                         name,
                                         path);
  if (!closure) {
    uabort("closure new error!");
  }
  if (parent) {
    if (vclosure_child_add(parent,closure)) {
      uabort("parent closure add child error!");
    }
  }
  init = ((vclosure_file*)closure)->init;
  init->scope = VPS_SCOPE_ENTRY;
  vast_attr_req_init(*req, reader);
  req->vps = vps;
  req->closure = closure;
  if (vstream2obj(reader,stream,(vast_attr_req*)req,&res)) {
    uabort("vstream2obj error!");
  }

  inst = vps_iretvoid(vps);
  vcfg_grp_inst_apd(init,inst);
  vcfg_grp_build(vps,init);
  vcfg_grp_connect(vps,init);

  return closure;
UEND

UDEFUN(UFNAME vclosure2vps,
       UARGS (vps_closure_req* req,
              vreader* reader,
              ustring* name,
              ustring* path,
              vps_cntr* vps),
       URET vapi vps_mod* vcall)
UDECLARE
  vclosure* top_closure;
UBEGIN
  top_closure = (vclosure*)vclosure_none_new(vps);
  if (!top_closure) {
    uabort("top closure new error!");
  }
  vfile2closure(req,
                top_closure,
                reader,
                name,
                path,
                vps,
                UNULL,
                VCLOSURE_TYPE_MAIN);
  vclosure2mod(top_closure,vps,UNULL);

  return vps->entry;
UEND

UDEFUN(UFNAME vclosure_field_get,
       UARGS (vclosure* closure,ustring* name),
       URET vapi vclosure_data* vcall)
UDECLARE
  uset_vclosure_datap* fields;
  uset_vclosurep* childs;
  ucursor c;
  ucursor i;
  vclosure_data* data;
UBEGIN

  /*find closure fields*/
  fields = (uset_vclosure_datap*)closure->fields;
  fields->iterate(&c);
  while (1) {
    vclosure_datap* dp = fields->next(fields,&c);
    if (!dp) {
      break;
    }
    data = *dp;
    if (!ustring_comp(name,data->closure_name)) {
      return data;
    }
  }

  /*find closure childs*/
  childs = (uset_vclosurep*)closure->childs;
  childs->iterate(&c);
  while (1) {
    vclosurep* childp;
    vclosure* child;

    childp = childs->next(childs,&c);
    if (!childp) {
      break;
    }
    child = *childp;
    if (vclosure_isfile(child)) {
      /*find child fields*/
      fields = (uset_vclosure_datap*)child->fields;
      fields->iterate(&i);
      while (1) {
        vclosure_datap* dp = fields->next(fields,&i);
        if (!dp) {
          break;
        }
        data = *dp;
        if ((data->scope == VPS_SCOPE_GLOBAL ||
             data->scope == VPS_SCOPE_DECL) &&
            !ustring_comp(name,data->closure_name)) {
          return data;
        }
      }
    }
  }

  /*find parent fields*/
  if (closure->parent) {
    return vclosure_field_get(closure->parent,name);
  } else {
    return UNULL;
  }
UEND

UDEFUN(UFNAME vclosure_curr_field_get,
         UARGS (vclosure* closure,ustring* name),
         URET vapi vclosure_data* vcall)
UDECLARE
  uset_vclosure_datap* fields;
  ucursor c;
  vclosure_data* data;
UBEGIN
  
  /*find closure fields*/
  fields = (uset_vclosure_datap*)closure->fields;
  fields->iterate(&c);
  while (1) {
    vclosure_datap* dp = fields->next(fields,&c);
    if (!dp) {
      break;
    }
    data = *dp;
    if (!ustring_comp(name,data->closure_name)) {
      return data;
    }
  }
  return UNULL;
UEND

UDEFUN(UFNAME vclosure_field_add,
       UARGS (vclosure* closure,vclosure_data* field),
       URET vapi int vcall)
UDECLARE
  uset_vclosure_datap* fields;
  ucursor c;
  vclosure_data* data;
UBEGIN
  if (field->closure_name) {
    fields = (uset_vclosure_datap*)closure->fields;
    fields->iterate(&c);
    while (1) {
      unext_vclosure_datap next = fields->next(fields,&c);
      if (!next) {
        break;
      }
      data = unext_get(next);
      if (data->closure_name &&
        !ustring_comp(field->closure_name,data->closure_name)) {
        return -1;
      }
    }
  }
  field->parent = closure;
  return ulist_vclosure_datap_append(closure->fields,field);
UEND

UDEFUN(UFNAME vclosure_child_add,
       UARGS (vclosure* closure,vclosure* child),
       URET vapi int vcall)
UDECLARE
  vclosure_named* named;
  uset_vclosurep* childs;
  ucursor c;
  vclosure* tmp;
  vclosure_named* tmp_named;
UBEGIN
  if (vclosure_isnamed(child)) {
    named = (vclosure_named*)child;
    if (named->closure_name) {
      childs = (uset_vclosurep*)closure->childs;
      childs->iterate(&c);
      while (1) {
        unext_vclosurep next = childs->next(childs, &c);
        if (!next) {
          break;
        }
        tmp = unext_get(next);
        if (!vclosure_isnamed(tmp)) {
          continue;
        }
        tmp_named = (vclosure_named*)tmp;
        if (tmp_named->closure_name &&
          !ustring_comp(tmp_named->closure_name,
            named->closure_name)) {
          return -1;
        }
      }
    }
  }
  child->parent = closure;
  return ulist_vclosurep_append(closure->childs,child);
UEND

UDEFUN(UFNAME vclosure_child_func_get,
       UARGS (vclosure* closure,ustring* name),
       URET static vclosure*)
UDECLARE
  uset_vclosurep* childs;
  uset_vclosurep* cchilds;
  ucursor c;
  ucursor cc;
  vclosure* child;
  vclosure_func* func;
  ustring* func_name;
UBEGIN
  childs = (uset_vclosurep*)closure->childs;
  childs->iterate(&c);
  while (1) {
    unext_vclosurep next = childs->next(childs,&c);
    if (!next) {
      break;
    }
    child = unext_get(next);
    if (child->closure_type == VCLOSURE_TYPE_FUNC) {
      func = (vclosure_func*)child;
      func_name = func->closure_name;
      if (func_name != UNULL &&
          !ustring_comp(name,func_name)) {
        return child;
      }
    } else if (child->closure_type == VCLOSURE_TYPE_FILE) {
      cchilds = (uset_vclosurep*)child->childs;
      cchilds->iterate(&cc);
      while (1) {
        unext_vclosurep nnext = cchilds->next(cchilds,&cc);
        if (!nnext) {
          break;
        }
        child = unext_get(nnext);
        if (child->closure_type == VCLOSURE_TYPE_FUNC) {
          func = (vclosure_func*)child;
          func_name = func->closure_name;
          if (func->scope == VPS_SCOPE_GLOBAL &&
              func_name != UNULL &&
              !ustring_comp(name,func_name)) {
            return child;
          }
        } else {
          continue;
        }
      }
    }
  }
  return UNULL;
UEND

UDEFUN(UFNAME vclosure_func_get,
       UARGS (vclosure* closure,ustring* name),
       URET vapi vclosure* vcall)
UDECLARE
  vclosure* child;
UBEGIN
  child = vclosure_child_func_get(closure,name);
  if (child) {
    return child;
  }
  if (closure->parent) {
    return vclosure_func_get(closure->parent,name);
  }
  return UNULL;
UEND

UDEFUN(UFNAME vclosure_func_args_count,
       UARGS (vclosure* closure),
       URET vapi int vcall)
UDECLARE
  vclosure_func* func;
UBEGIN
  if (vclosure_isfunc(closure)) {
    func = (vclosure_func*)closure;
    return func->init->params_count;
  } else {
    return 0;
  }
UEND

UDEFUN(UFNAME vclosure_file_get,
       UARGS (vclosure* closure,ustring* path),
       URET vapi vclosure* vcall)
UDECLARE
  uset_vclosurep* childs;
  ucursor c;
  vclosure* child;
  vclosure_file* file;
UBEGIN
  childs = (uset_vclosurep*)closure->childs;
  childs->iterate(&c);
  while (1) {
    unext_vclosurep next = childs->next(childs,&c);
    if (!next) {
      break;
    }
    child = unext_get(next);
    if (!vclosure_ismain(child) && !vclosure_isfile(child)) {
      continue;
    }
    file = (vclosure_file*)child;
    if (!ustring_comp(path,file->closure_path)) {
      return child;
    }
  }
  if (closure->parent) {
    return vclosure_file_get(closure->parent,path);
  }
  return UNULL;
UEND

UDEFUN(UFNAME vclosure_stru_field_get,
       UARGS (vclosure* closure,ustring* name),
       URET vapi vclosure_data* vcall)
UDECLARE
  uset_vclosure_datap* fields;
  ucursor c;
  vclosure_data* field;
UBEGIN
  fields = (uset_vclosure_datap*)closure->fields;
  fields->iterate(&c);
  while (1) {
    unext_vclosure_datap next = fields->next(fields,&c);
    if (!next) {
      break;
    }
    field = unext_get(next);
    if (!ustring_comp(name,field->closure_name)) {
      return field;
    }
  }
  return UNULL;
UEND
  
UDEFUN(UFNAME vclosure_stru_child_get,
       UARGS (vclosure* closure,ustring* name),
       URET vapi vclosure* vcall)
UDECLARE
  uset_vclosurep* childs;
  uset_vclosurep* cchilds;
  ucursor c;
  ucursor cc;
  vclosure* child;
  vclosure_stru* stru;
UBEGIN
  childs = (uset_vclosurep*)closure->childs;
  childs->iterate(&c);
  while (1) {
    unext_vclosurep next = childs->next(childs,&c);
    if (!next) {
      break;
    }
    child = unext_get(next);
    if (vclosure_isstru(child)) {
      stru = (vclosure_stru*)child;
      if (!ustring_comp(name, stru->closure_name)) {
        return child;
      }
    } else if (vclosure_isfile(child)) {
      cchilds = (uset_vclosurep*)child->childs;
      cchilds->iterate(&cc);
      while (1) {
        unext_vclosurep nnext = cchilds->next(cchilds, &cc);
        if (!nnext) {
          break;
        }
        child = unext_get(nnext);
        if (vclosure_isstru(child)) {
          stru = (vclosure_stru*)child;
          if (stru->scope == VPS_SCOPE_GLOBAL &&
              !ustring_comp(name, stru->closure_name)) {
            return child;
          }
        } else {
          continue;
        }
      }
    }
  }
  return UNULL;
UEND

UDEFUN(UFNAME vclosure_stru_get,
       UARGS (vclosure* closure,ustring* name),
       URET vapi vclosure* vcall)
UDECLARE
  vclosure* child;
UBEGIN
  child = vclosure_stru_child_get(closure,name);
  if (!child && closure->parent) {
    return vclosure_stru_get(closure->parent,name);
  }
  return child;
UEND

UDEFUN(UFNAME vclosure_child_index_of,
       UARGS (vclosure* index_child),
       URET vapi int vcall)
UDECLARE
  vclosure* closure;
  uset_vclosurep* childs;
  ucursor c;
  vclosure* child;
  int count;
  vclosure_named* index_named;
  vclosure_named* named;
UBEGIN
  if (!vclosure_isnamed(index_child)) {
    return -1;
  }
  closure = index_child->parent;
  if (!closure) {
    return -1;
  }
  index_named = (vclosure_named*)index_child;
  count = closure->fields->len;
  childs = (uset_vclosurep*)closure->childs;
  childs->iterate(&c);
  for ( ; ;count++) {
    unext_vclosurep next = childs->next(childs,&c);
    if (!next) {
      break;
    }
    child = unext_get(next);
    if (!vclosure_isnamed(child)) {
      continue;
    }
    named = (vclosure_named*)child;
    if (!ustring_comp(index_named->closure_name,named->closure_name)) {
      return count;
    }
  }
  return -1;
UEND

UDEFUN(UFNAME vclosure_field_index_of,
       UARGS (vclosure_data* field),
       URET vapi int vcall)
UDECLARE
  vclosure* closure;
  uset_vclosure_datap* fields;
  ucursor c;
  vclosure_data* data;
  int count;
UBEGIN
  /*find closure fields*/
  closure = field->parent;
  fields = (uset_vclosure_datap*)closure->fields;
  fields->iterate(&c);
  for (count = 0; ;count++) {
    unext_vclosure_datap next = fields->next(fields,&c);
    if (!next) {
      break;
    }
    data = unext_get(next);
    if (!ustring_comp(field->closure_name,data->closure_name)) {
      return count;
    }
  }
  return -1;
UEND

UDEFUN(UFNAME vclosure_size_of,
       UARGS (vclosure* closure),
       URET vapi int vcall)
UDECLARE

UBEGIN
  return closure->fields->len + closure->childs->len;
UEND

UDEFUN(UFNAME vclosure_path_get,
       UARGS (vreader* reader,ustring* name),
       URET vapi ustring* vcall)
UDECLARE
UBEGIN
  return vreader_path_get(reader,name);
UEND

UDEFUN(UFNAME vclosure_code_apd,
       UARGS(vclosure_code* code,vps_inst* inst),
       URET vapi void vcall)
UDECLARE
UBEGIN
  vcfg_grp_inst_apd(code->prog->init, inst);
UEND

UDEFUN(UFNAME vclosure_func_params_add,
       UARGS(vclosure* closure, vclosure_data* data),
       URET vapi int vcall)
UDECLARE
  vclosure_prog* prog;
  vcfg_graph* grp;
UBEGIN
  if (vclosure_field_add(closure, data)) {
    return -1;
  }
  if (vclosure_isprog(closure)) {
    prog = (vclosure_prog*)closure;
    grp = prog->init;
    data->data->idx = grp->locals->count;
    vcfg_grp_params_apd(grp, data->data);
  }
  return 0;
UEND

UDEFUN(UFNAME vclosure_func_locals_add,
       UARGS(vclosure * closure, vclosure_data * data),
       URET vapi int vcall)
UDECLARE
  vclosure_prog* prog;
  vcfg_graph* grp;
UBEGIN
  if (vclosure_field_add(closure, data)) {
    return -1;
  }
  if (vclosure_isprog(closure)) {
    prog = (vclosure_prog*)closure;
    grp = prog->init;
    data->data->idx = grp->locals->count;
    vcfg_grp_locals_apd(grp, data->data);
  }
  return 0;
UEND

UDEFUN(UFNAME vclosure_func_locals_push,
       UARGS (vclosure* closure, vclosure_data* data),
       URET vapi int vcall)
UDECLARE
  vclosure_prog* prog;
  vclosure_code* code;
  vcfg_graph* grp;
UBEGIN
  if (vclosure_field_add(closure, data)) {
    return -1;
  }
  if (vclosure_is_body(closure)) {
    if (vclosure_isprog(closure)) {
      prog = (vclosure_prog*)closure;
      grp = prog->init;
    } else if (vclosure_iscode(closure)) {
      code = (vclosure_code*)closure;
      grp = code->prog->init;
    } else {
      uabort("closure type error!");
      return -1;
    }
    data->data->idx = grp->params_count + grp->locals_index;
    grp->locals_index++;
    if (grp->locals_index > grp->locals_count) {
      grp->locals_count = grp->locals_index;
    }
  }
  return 0;
UEND

UDEFUN(UFNAME vclosure_func_locals_pop,
       UARGS (vclosure* closure),
       URET vapi void vcall)
UDECLARE
  vclosure_code* code;
  vcfg_graph* grp;
UBEGIN
  if (vclosure_iscode(closure)) {
    code = (vclosure_code*)closure;
    grp = code->prog->init;
    grp->locals_index -= closure->fields->len;
  }
UEND

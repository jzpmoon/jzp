#include "ualloc.h"
#include "uerror.h"
#include "vclosure.h"
#include "vparser.h"
#include "vm.h"

UDEFUN(UFNAME vm_sym_decoration,
       UARGS (ustring_table* symtb, ubuffer* buff, void* ptr,
	      enum vfe_base_type base_type, int sub_type,
	      enum vfe_base_type res_base_type, int res_sub_type),
       URET static ustring*)
UDECLARE
  ustring* new_str;
  vast_symbol* sym;
  vclosure* closure;
  vclosure_func* func;
  vclosure_stru* stru;
  vclosure_data* data;
  ustring* sym_name;
  ustring* closure_name;
UBEGIN
  switch (base_type) {
    case vfe_ast:
      switch (sub_type) {
        case vastk_symbol:{
          int i;
          sym = (vast_symbol*)ptr;
          sym_name = sym->name;
          ubuffer_ready_write(buff);
          for (i = 0; i < sym_name->len; i++) {
            if (ubuffer_write_next(buff, sym_name->value[i]) == -1) {
              uabort("buffer overflow!");
            }
          }
          if (ubuffer_write_int2ascii(buff, i) == -1) {
            uabort("buffer overflow!");
          }
          if (ubuffer_write_next(buff, '\0') == -1) {
            uabort("buffer overflow!");
          }
          ubuffer_ready_read(buff);
          new_str = ustring_table_put(symtb, buff->data, -1);
        }
        break;
        default:
          new_str = UNULL;
      }
      break;
    case vfe_closure:
      switch (sub_type) {
        case VCLOSURE_TYPE_FUNC:
          func = (vclosure_func*)ptr;
          ubuffer_ready_write(buff);
          while (1) {
            int i;
            closure_name = func->closure_name;
            for (i = 0; i < closure_name->len; i++) {
              if (ubuffer_write_next(buff, closure_name->value[i]) == -1) {
                uabort("buffer overflow!");
              }
            }
            if (ubuffer_write_int2ascii(buff, i) == -1) {
              uabort("buffer overflow!");
            }
            closure = func->parent;
            if (closure && vclosure_isfunc(closure)) {
              func = (vclosure_func*)closure;
            } else {
              break;
            }
          }
          if (ubuffer_write_next(buff, '\0') == -1) {
            uabort("buffer overflow!");
          }
          ubuffer_ready_read(buff);
          new_str = ustring_table_put(symtb, buff->data, -1);
          break;
        case VCLOSURE_TYPE_DATA: {
          int i;
          data = (vclosure_data*)ptr;
          closure_name = data->closure_name;
          ubuffer_ready_write(buff);
          for (i = 0; i < closure_name->len; i++) {
            if (ubuffer_write_next(buff, closure_name->value[i]) == -1) {
              uabort("buffer overflow!");
            }
          }
          if (ubuffer_write_int2ascii(buff, i) == -1) {
            uabort("buffer overflow!");
          }
          closure = data->parent;
          if (closure && vclosure_isfunc(closure)) {
            while (1) {
              func = (vclosure_func*)closure;
              closure_name = func->closure_name;
              for (i = 0; i < closure_name->len; i++) {
                if (ubuffer_write_next(buff, closure_name->value[i]) == -1)
                {
                  uabort("buffer overflow!");
                }
              }
              if (ubuffer_write_int2ascii(buff, i) == -1) {
                uabort("buffer overflow!");
              }
              closure = func->parent;
              if (!closure || !vclosure_isfunc(closure)) {
                break;
              }
            }
          } else if (closure && vclosure_isstru(closure)) {
            while (1) {
              stru = (vclosure_stru*)closure;
              closure_name = stru->closure_name;
              for (i = 0; i < closure_name->len; i++) {
                if (ubuffer_write_next(buff, closure_name->value[i]) == -1)
                {
                  uabort("buffer overflow!");
                }
              }
              if (ubuffer_write_int2ascii(buff, i) == -1) {
                uabort("buffer overflow!");
              }
              closure = stru->parent;
              if (!closure || !vclosure_isstru(closure)) {
                break;
              }
            }
          }
          if (ubuffer_write_next(buff, '\0') == -1) {
            uabort("buffer overflow!");
          }
          ubuffer_ready_read(buff);
          new_str = ustring_table_put(symtb, buff->data, -1);
        }
        break;
        case VCLOSURE_TYPE_STRU: {
          int i;
          stru = (vclosure_stru*)ptr;
          closure_name = stru->closure_name;
          ubuffer_ready_write(buff);
          for (i = 0; i < closure_name->len; i++) {
            if (ubuffer_write_next(buff, closure_name->value[i]) == -1)
            {
              uabort("buffer overflow!");
            }
          }
          if (res_sub_type == VCLOSURE_TYPE_STRU) {
            if (ubuffer_write_next(buff, '.') == -1) {
              uabort("buffer overflow!");
            }
          } else if (res_sub_type == VCLOSURE_TYPE_DATA) {
            if (ubuffer_write_int2ascii(buff, i) == -1) {
              uabort("buffer overflow!");
            }
          } else {
            uabort("res_sub_type error!");
          }
          closure = stru->parent;
          while (1) {
            if (closure && vclosure_isstru(closure)) {
              stru = (vclosure_stru*)closure;
              closure_name = stru->closure_name;
              for (i = 0; i < closure_name->len; i++) {
                if (ubuffer_write_next(buff, closure_name->value[i]) == -1)
                {
                  uabort("buffer overflow!");
                }
              }
              if (ubuffer_write_int2ascii(buff, i) == -1) {
                uabort("buffer overflow!");
              }
              closure = stru->parent;
            } else if (closure && vclosure_isfunc(closure)) {
              func = (vclosure_func*)closure;
              closure_name = stru->closure_name;
              for (i = 0; i < closure_name->len; i++) {
                if (ubuffer_write_next(buff, closure_name->value[i]) == -1)
                {
                  uabort("buffer overflow!");
                }
              }
              if (ubuffer_write_int2ascii(buff, i) == -1) {
                uabort("buffer overflow!");
              }
              closure = func->parent;
            } else {
              if (ubuffer_write_next(buff, '\0') == -1) {
                uabort("buffer overflow!");
              }
              ubuffer_ready_read(buff);
              new_str = ustring_table_put(symtb, buff->data, -1);
              break;
            }
          }
        }
        break;
      default:
        new_str = UNULL;
      }
      break;
    default:
      new_str = UNULL;
  }
  
  return new_str;
UEND

UDEFUN(UFNAME vm_m1_h1_alloc,
       UARGS (uallocator* allocator,
              int vm_gc_ssz,
              int vm_gc_asz,
              int vm_gc_rsz),
       URET vapi vm_m1* vcall)
UDECLARE
  vm_m1* m1;
  vgc_heap* heap;
  vcontext* ctx;
  uhstb_vmod* mods;
  ustring_table* symtb;
  ustring_table* strtb;
UBEGIN
  m1 = allocator->alloc(allocator,sizeof(vm_m1));
  if (!m1) {
    uabort("vm m1 new error!");
  }

  heap = vgc_heap_new(vm_gc_ssz,
		      vm_gc_asz,
		      vm_gc_rsz);
  if (!heap) {
    uabort("new heap error!");
  }

  ctx = vcontext_new(m1,heap);
  if (!ctx) {
    uabort("new context error!");
  }

  mods = uhstb_vmod_alloc(allocator,-1);
  if (!mods) {
    uabort("mods new error!");
  }
    
  symtb = ustring_table_alloc(allocator,-1);
  if(!symtb){
    uabort("symtb new error!");
  }

  strtb = ustring_table_alloc(allocator,-1);
  if(!strtb){
    uabort("strtb new error!");
  }

  vps_cntr_init(&m1->vps);
  
  m1->vps.symgen = vm_sym_decoration;

  ctx->mods = mods;
  ctx->symtb = symtb;
  ctx->strtb = strtb;

  m1->allocator = allocator;
  m1->heap = heap;
  m1->ctx_main = ctx;
  m1->ctx_count = 1;
  m1->mods = mods;
  m1->mod_main = UNULL;
  m1->mod_fuse = VMOD_FUSE_NO;
  m1->loader = UNULL;
  m1->symtb = symtb;
  m1->strtb = strtb;

  return m1;
UEND

UDEFUN(UFNAME vm_m1_mod_loader_set,
       UARGS (vm_m1* m1,
              vmod_loader* loader),
       URET vapi void vcall)
UDECLARE
  
UBEGIN
  m1->loader = loader;
  m1->ctx_main->loader = loader;
UEND

UDEFUN(UFNAME bc_constant_get,
       UARGS (vcontext* ctx,
              usize_t index),
       URET vslot)
UDECLARE
  vgc_call* calling;
  vgc_subr* subr;
  vgc_array* consts;
  vslot slot;
  vgc_obj* obj;
  vgc_ref* ref;
UBEGIN
  calling = vgc_obj_ref_get(ctx, calling, vgc_call);
  subr = vgc_obj_ref_get(calling, subr, vgc_subr);
  consts = vgc_obj_ref_get(subr, consts, vgc_array);

  if(vgc_obj_ref_check(consts,index))
    vcontext_exit(ctx, "vm:constant error!");
  slot = consts->objs[index];
  /* whether vslot is ref and not UNULL */
  if (vslot_is_ref(slot)) {
    if (!vslot_is_null(slot)) {
      /* strip the vgc_ref */
      obj = vslot_ref_get(slot,vgc_obj);
      if (vgc_obj_typeof(obj,vgc_obj_type_ref)) {
        ref = (vgc_ref*)obj;
        slot = vgc_slot_get(ref,ref);
      }      
    }
  }
  vslot_const_no(slot);
  return slot;
UEND

UDEFUN(UFNAME bc_constant_set,
       UARGS (vcontext* ctx,
              usize_t index,
              vslot value),
       URET void)
UDECLARE
  vgc_call* calling;
  vgc_subr* subr;
  vgc_array* consts;
  vslot slot;
  vgc_obj* obj;
  vgc_ref* ref;
  vslot slot_ref;
UBEGIN
  calling = vgc_obj_ref_get(ctx,calling,vgc_call);
  subr = vgc_obj_ref_get(calling,subr,vgc_subr);
  consts = vgc_obj_ref_get(subr,consts,vgc_array);

  if(vgc_obj_ref_check(consts,index))
    vcontext_exit(ctx, "vm:constant error!");
  slot = consts->objs[index];
  /* whether vslot is ref and not UNULL */
  if (vslot_is_ref(slot)) {
    if (!vslot_is_null(slot)) {
      /* strip the vgc_ref */
      obj = vslot_ref_get(slot,vgc_obj);
      if (vgc_obj_typeof(obj,vgc_obj_type_ref)) {
        ref = (vgc_ref*)obj;
        slot_ref = vgc_slot_get(ref,ref);
        vslot_const_no(value);
        /* whether constant */
        if (vslot_is_const(slot_ref)) {
          if (!vslot_is_null(slot_ref)) {
            vcontext_exit(ctx, "vm:can not modify constant!");
          }
          vslot_const_yes(value);
        }
        vgc_slot_set(ref,ref,value);
        return;
      }
    }
  }
  consts->objs[index] = value;
UEND

UDEFUN(UFNAME bc_top,
       UARGS (vcontext* ctx,usize_t index),
       URET void)
UDECLARE
UBEGIN
  vcontext_stack_top_set(ctx,index);
UEND

UDEFUN(UFNAME bc_push,
       UARGS (vcontext* ctx,
             vslot slot),
       URET void)
UDECLARE
UBEGIN
  vcontext_stack_push(ctx,slot);
UEND

UDEFUN(UFNAME bc_pop,
       UARGS (vcontext* ctx,usize_t index),
       URET void)
UDECLARE
  vslot slot;
UBEGIN
  vcontext_stack_pop(ctx,&slot);
  bc_constant_set(ctx,index,slot);
UEND

UDEFUN(UFNAME bc_popv,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vslot slot;
UBEGIN
  vcontext_stack_pop(ctx,&slot);
UEND

UDEFUN(UFNAME bc_locals,
       UARGS (vcontext* ctx,
              usize_t index),
       URET vslot)
UDECLARE
  vgc_call* calling = vgc_obj_ref_get(ctx,calling,vgc_call);
  vgc_subr* subr = vgc_obj_ref_get(calling,subr,vgc_subr);
  usize_t apc = calling->params_count;
  usize_t dpc = subr->params_count;
  usize_t dlc = subr->locals_count;
  usize_t base = calling->base;
  usize_t count;
  usize_t change_index;
  usize_t real_index;
  vslot slot;
UBEGIN
  if (apc > dpc) {
    count = apc + dlc;
    if (index >= dpc) {
      change_index = index + apc - dpc;
    } else {
      change_index = index;
    }
  } else {
    count = dpc + dlc;
    change_index = index;
  }
  real_index = base - count + change_index;
  
  if (change_index < 0 || change_index >= count)
    vcontext_exit(ctx, "vm:local varable error:index %d,count %d!",
      change_index, count);
  slot = vcontext_stack_get(ctx, real_index);
  return slot;
UEND

UDEFUN(UFNAME bc_params,
       UARGS (vcontext* ctx,
              usize_t index),
       URET vslot)
UDECLARE
  vgc_call* calling = vgc_obj_ref_get(ctx,calling,vgc_call);
  vgc_subr* subr = vgc_obj_ref_get(calling,subr,vgc_subr);
  usize_t apc = calling->params_count;
  usize_t dpc = subr->params_count;
  usize_t dlc = subr->locals_count;
  usize_t base = calling->base;
  usize_t params_count;
  usize_t real_index;
  vslot slot;
UBEGIN
  if (apc > dpc) {
    params_count = apc;
  } else {
    params_count = dpc;
  }
  real_index = base - (params_count + dlc) + index;
  
  if (index < 0 || index >= params_count)
    vcontext_exit(ctx, "vm:params index error:index %d,count %d!",
                  index, params_count);
  slot = vcontext_stack_get(ctx, real_index);
  return slot;
UEND

UDEFUN(UFNAME bc_store_locals,
       UARGS (vcontext* ctx,
              usize_t index),
       URET void)
UDECLARE
  vgc_call* calling = vgc_obj_ref_get(ctx,calling,vgc_call);
  vgc_subr* subr = vgc_obj_ref_get(calling,subr,vgc_subr);
  usize_t apc = calling->params_count;
  usize_t dpc = subr->params_count;
  usize_t dlc = subr->locals_count;
  usize_t base = calling->base;
  usize_t count;
  usize_t change_index;
  usize_t real_index;
  vslot slot;
UBEGIN
  if (apc > dpc) {
    count = apc + dlc;
    if (index >= dpc) {
      change_index = index + apc - dpc;
    }
    else {
      change_index = index;
    }
  }
  else {
    count = dpc + dlc;
    change_index = index;
  }
  real_index = base - count + change_index;

  if(change_index < 0 || change_index >= count)
    vcontext_exit(ctx, "vm:local varable error:index %d,count %d!",
      change_index, count);
  vcontext_stack_pop(ctx,&slot);
  vcontext_stack_set(ctx,real_index,slot);
UEND

UDEFUN(UFNAME bc_store_params,
       UARGS (vcontext* ctx,
              usize_t index),
       URET void)
UDECLARE
  vgc_call* calling = vgc_obj_ref_get(ctx,calling,vgc_call);
  vgc_subr* subr = vgc_obj_ref_get(calling,subr,vgc_subr);
  usize_t apc = calling->params_count;
  usize_t dpc = subr->params_count;
  usize_t dlc = subr->locals_count;
  usize_t base = calling->base;
  usize_t params_count;
  usize_t real_index;
  vslot slot;
UBEGIN
  if (apc > dpc) {
    params_count = apc;
  } else {
    params_count = dpc;
  }
  real_index = base - (params_count + dlc) + index;

  if(index < 0 || index >= params_count)
    vcontext_exit(ctx, "vm:store params error:index %d,count %d!",
                  index, params_count);
  vcontext_stack_pop(ctx,&slot);
  vcontext_stack_set(ctx,real_index,slot);
UEND

UDEFUN(UFNAME bc_jmp,
       UARGS (vcontext* ctx,
              usize_t offset),
       URET void)
UDECLARE
UBEGIN
  vgc_call* calling = vgc_obj_ref_get(ctx,calling,vgc_call);
  vgc_subr* subr = vgc_obj_ref_get(calling,subr,vgc_subr);
  vgc_string* bytecode = vgc_obj_ref_get(subr,bytecode,vgc_string);
  if(!vgc_str_bound_check(bytecode,offset))
    vcontext_exit(ctx, "vm:jmp error!");
  calling->pc = offset;
UEND

UDEFUN(UFNAME bc_jmpi,
       UARGS (vcontext* ctx,
              usize_t offset),
       URET void)
UDECLARE
  vslot slot;
UBEGIN
  vcontext_stack_pop(ctx,&slot);
  if(vslot_is_true(slot)){
    bc_jmp(ctx,offset);
  }
UEND

UDEFUN(UFNAME bc_add,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vslot slot1;
  vslot slot2;
  vslot num;
UBEGIN
  vcontext_stack_pop(ctx,&slot1);
  vcontext_stack_pop(ctx,&slot2);
  
  if (vslot_is_num(slot1)) {
    double num1 = vslot_num_get(slot1);
    if (vslot_is_num(slot2)) {
      double num2 = vslot_num_get(slot2);
      vslot_num_set(num,num1 + num2);
    } else if (vslot_is_int(slot2)) {
      int int2 = vslot_int_get(slot2);
      vslot_num_set(num,num1 + int2);
    } else {
      vcontext_exit(ctx, "first is not a numberical!");
      return;
    }
  } if (vslot_is_int(slot1)) {
    int int1 = vslot_int_get(slot1);
    if (vslot_is_num(slot2)) {
      double num2 = vslot_num_get(slot2);
      vslot_num_set(num,int1 + num2);
    } else if (vslot_is_int(slot2)) {
      int int2 = vslot_int_get(slot2);
      vslot_int_set(num,int1 + int2);
    } else {
      vcontext_exit(ctx, "first is not a numberical!");
      return;
    }
  } else {
    vcontext_exit(ctx, "second is not a numberical!");
    return;
  }
  vcontext_stack_push(ctx,num);
UEND

UDEFUN(UFNAME bc_sub,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vslot slot1;
  vslot slot2;
  vslot num;
UBEGIN
  vcontext_stack_pop(ctx,&slot1);
  vcontext_stack_pop(ctx,&slot2);

  if (vslot_is_num(slot1)) {
    double num1 = vslot_num_get(slot1);
    if (vslot_is_num(slot2)) {
      double num2 = vslot_num_get(slot2);
      vslot_num_set(num,num1 - num2);
    } else if (vslot_is_int(slot2)) {
      int int2 = vslot_int_get(slot2);
      vslot_num_set(num,num1 - int2);
    } else {
      vcontext_exit(ctx, "first is not a numberical!");
      return;
    }
  } if (vslot_is_int(slot1)) {
    int int1 = vslot_int_get(slot1);
    if (vslot_is_num(slot2)) {
      double num2 = vslot_num_get(slot2);
      vslot_num_set(num,int1 - num2);
    } else if (vslot_is_int(slot2)) {
      int int2 = vslot_int_get(slot2);
      vslot_int_set(num,int1 - int2);
    } else {
      vcontext_exit(ctx, "first is not a numberical!");
      return;
    }
  } else {
    vcontext_exit(ctx, "second is not a numberical!");
    return;
  }
  vcontext_stack_push(ctx,num);
UEND

UDEFUN(UFNAME bc_mul,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vslot slot1;
  vslot slot2;
  vslot num;
UBEGIN
  vcontext_stack_pop(ctx,&slot1);
  vcontext_stack_pop(ctx,&slot2);
  
  if (vslot_is_num(slot1)) {
    double num1 = vslot_num_get(slot1);
    if (vslot_is_num(slot2)) {
      double num2 = vslot_num_get(slot2);
      vslot_num_set(num,num1 * num2);
    } else if (vslot_is_int(slot2)) {
      int int2 = vslot_int_get(slot2);
      vslot_num_set(num,num1 * int2);
    } else {
      vcontext_exit(ctx, "first is not a numberical!");
      return;
    }
  } if (vslot_is_int(slot1)) {
    int int1 = vslot_int_get(slot1);
    if (vslot_is_num(slot2)) {
      double num2 = vslot_num_get(slot2);
      vslot_num_set(num,int1 * num2);
    } else if (vslot_is_int(slot2)) {
      int int2 = vslot_int_get(slot2);
      vslot_int_set(num,int1 * int2);
    } else {
      vcontext_exit(ctx, "first is not a numberical!");
      return;
    }
  } else {
    vcontext_exit(ctx, "second is not a numberical!");
    return;
  }
  vcontext_stack_push(ctx,num);
UEND

UDEFUN(UFNAME bc_div,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vslot slot1;
  vslot slot2;
  double num1;
  double num2;
  vslot num;
  vgc_ref* ref;
  vslot slot_ex;
UBEGIN
  vcontext_stack_pop(ctx,&slot1);
  vcontext_stack_pop(ctx,&slot2);
  
  if (vslot_is_num(slot1)) {
    num1 = vslot_num_get(slot1);
  } else if (vslot_is_int(slot1)) {
    num1 = vslot_int_get(slot1);
  } else {
    vcontext_exit(ctx, "first is not a numberical!");
    return;
  }
  if (vslot_is_num(slot2)) {
    num2 = vslot_num_get(slot2);
  } else if (vslot_is_int(slot2)) {
    num2 = vslot_int_get(slot2);
  } else {
    vcontext_exit(ctx, "second is not a numberical!");
    return;
  }
  if (num2 == 0) {
    ustring sym_name = ustring_init("exception-div-zero18");
    vsymbol* sym = vmod_symbol_get(ctx,UNULL,&sym_name);
    if (!sym) {
      vcontext_exit(ctx, "get exception error!");
    }
    ref = vslot_ref_get(sym->slot,vgc_ref);
    slot_ex = vgc_slot_get(ref,ref);
    vslot_const_no(slot_ex);
    vcontext_exception_throw(ctx,slot_ex);
  }
  vslot_num_set(num,(num1 / num2));
  vcontext_stack_push(ctx,num);
UEND

UDEFUN(UFNAME bc_alen,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vslot slot;
  vgc_call* calling = vgc_obj_ref_get(ctx,calling,vgc_call);
UBEGIN
  vslot_int_set(slot,calling->params_count);
  vcontext_stack_push(ctx,slot);
UEND

UDEFUN(UFNAME bc_eq,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vslot slot1;
  vslot slot2;
  double num1;
  double num2;
  vslot bool;
UBEGIN
  vcontext_stack_pop(ctx,&slot1);
  vcontext_stack_pop(ctx,&slot2);
  
  if(vslot_is_ref(slot1) && vslot_is_ref(slot2)){
    bool = vslot_ref_eq(slot1,slot2);
  }else if(vslot_is_true(slot1) && vslot_is_true(slot2)){
    vslot_bool_set(bool,vbool_true);
  }else{
    if(vslot_is_num(slot1)){
      num1 = vslot_num_get(slot1);
    } else if(vslot_is_int(slot1)){
      num1 = vslot_int_get(slot1);
    } else if(vslot_is_char(slot1)){
      num1 = vslot_char_get(slot1);
    } else {
      vslot_bool_set(bool,vbool_false);
      vcontext_stack_push(ctx,bool);
      return;
    }
    if(vslot_is_num(slot2)){
      num2 = vslot_num_get(slot2);
    } else if(vslot_is_int(slot2)){
      num2 = vslot_int_get(slot2);
    } else if(vslot_is_char(slot2)){
      num2 = vslot_char_get(slot2);
    } else {
      vslot_bool_set(bool,vbool_false);
      vcontext_stack_push(ctx,bool);
      return;
    }
    if(num1 == num2){
      vslot_bool_set(bool,vbool_true);
    }else{
      vslot_bool_set(bool,vbool_false);
    }
  }
  vcontext_stack_push(ctx,bool);
UEND

UDEFUN(UFNAME bc_gt,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vslot slot1;
  vslot slot2;
  double num1;
  double num2;
  vslot bool;
UBEGIN
  vcontext_stack_pop(ctx,&slot1);
  vcontext_stack_pop(ctx,&slot2);

  if(vslot_is_num(slot1)){
    num1 = vslot_num_get(slot1);
  } else if(vslot_is_int(slot1)){
    num1 = vslot_int_get(slot1);
  } else if(vslot_is_char(slot1)){
    num1 = vslot_char_get(slot1);
  } else {
    vslot_bool_set(bool,vbool_false);
    vcontext_stack_push(ctx,bool);
    return;
  }
  if(vslot_is_num(slot2)){
    num2 = vslot_num_get(slot2);
  } else if(vslot_is_int(slot2)){
    num2 = vslot_int_get(slot2);
  } else if(vslot_is_char(slot2)){
    num2 = vslot_char_get(slot2);
  } else {
    vslot_bool_set(bool,vbool_false);
    vcontext_stack_push(ctx,bool);
    return;
  }
  if(num1 > num2){
    vslot_bool_set(bool,vbool_true);
  }else{
    vslot_bool_set(bool,vbool_false);
  }
  vcontext_stack_push(ctx,bool);
UEND

UDEFUN(UFNAME bc_lt,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vslot slot1;
  vslot slot2;
  double num1;
  double num2;
  vslot bool;
UBEGIN
  vcontext_stack_pop(ctx,&slot1);
  vcontext_stack_pop(ctx,&slot2);

  if(vslot_is_num(slot1)){
    num1 = vslot_num_get(slot1);
  } else if(vslot_is_int(slot1)){
    num1 = vslot_int_get(slot1);
  } else if(vslot_is_char(slot1)){
    num1 = vslot_char_get(slot1);
  } else {
    vslot_bool_set(bool,vbool_false);
    vcontext_stack_push(ctx,bool);
    return;
  }
  if(vslot_is_num(slot2)){
    num2 = vslot_num_get(slot2);
  } else if(vslot_is_int(slot2)){
    num2 = vslot_int_get(slot2);
  } else if(vslot_is_char(slot2)){
    num2 = vslot_char_get(slot2);
  } else {
    vslot_bool_set(bool,vbool_false);
    vcontext_stack_push(ctx,bool);
    return;
  }
  if(num1 < num2){
    vslot_bool_set(bool,vbool_true);
  }else{
    vslot_bool_set(bool,vbool_false);
  }
  vcontext_stack_push(ctx,bool);
UEND

UDEFUN(UFNAME bc_and,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vslot slot1;
  vslot slot2;
  vslot bool;
UBEGIN
  vcontext_stack_pop(ctx,&slot1);
  vcontext_stack_pop(ctx,&slot2);

  if(vslot_is_bool(slot1) && vslot_is_bool(slot2)){
    int bool1 = vslot_bool_get(slot1);
    int bool2 = vslot_bool_get(slot2);
    if(bool1 && bool2){
      vslot_bool_set(bool,vbool_true);
    }else{
      vslot_bool_set(bool,vbool_false);
    }
  }else{
    vslot_bool_set(bool,vbool_false);
  }
  vcontext_stack_push(ctx,bool);    
UEND

UDEFUN(UFNAME bc_or,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vslot slot1;
  vslot slot2;
  vslot bool;
UBEGIN
  vcontext_stack_pop(ctx,&slot1);
  vcontext_stack_pop(ctx,&slot2);

  if(vslot_is_bool(slot1) && vslot_is_bool(slot2)){
    int bool1 = vslot_bool_get(slot1);
    int bool2 = vslot_bool_get(slot2);
    if(bool1 || bool2){
      vslot_bool_set(bool,vbool_true);
    }else{
      vslot_bool_set(bool,vbool_false);
    }
  }else{
    vslot_bool_set(bool,vbool_false);
  }
  vcontext_stack_push(ctx,bool);  
UEND

UDEFUN(UFNAME bc_not,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vslot slot;
  vslot bool;
UBEGIN
  vcontext_stack_pop(ctx,&slot);

  if(vslot_is_bool(slot)){
    int b = vslot_bool_get(slot);
    if(!b){
      vslot_bool_set(bool,vbool_true);
    }else{
      vslot_bool_set(bool,vbool_false);
    }
  }else{
    vslot_bool_set(bool,vbool_false);
  }
  vcontext_stack_push(ctx,bool);  
UEND

UDEFUN(UFNAME bc_call,
       UARGS (vcontext* ctx,
              int isapc, /* is actual params count ? */
              usize_t apc), /* actual params count */
       URET void)
UDECLARE
  vgc_call* calling;
  vslot slot;
  vgc_obj* obj;
UBEGIN
  calling = vgc_obj_ref_get(ctx,calling,vgc_call);
  vcontext_stack_pop(ctx,&slot);
  if(!vslot_is_ref(slot)){
    vcontext_exit(ctx, "bc_call:not refrence can not execute!");
  }
  obj = vslot_ref_get(slot,vgc_obj);
  if (vgc_obj_typeof(obj,vgc_obj_type_closure)) {
    vgc_closure* closure;
    vgc_subr* subr;
    vgc_array* field;
    vgc_call* call;
    usize_t params_count;
    usize_t base;
    
    closure = (vgc_closure*)obj;
    subr = vgc_obj_ref_get(closure,subr,vgc_subr);
    params_count = subr->params_count;
    if (subr->params_type == vgc_subr_params_flex) {
      if (!isapc) {
        vcontext_exit(ctx, "bc_call:actual params count much be provide!");
      }
      if (apc < params_count) {
        usize_t i = apc;
        vslot slot_null;
        vslot_null_set(slot_null);
        while (i < params_count) {
          vcontext_stack_push(ctx, slot_null);
          i++;
        }
      } else {
        params_count = apc;
      }
    } else {
      if (isapc) {
        if (apc > params_count) {
          vcontext_exit(ctx, "bc_call:actual params count too much!");
        }
        else if (apc < params_count) {
          vcontext_exit(ctx, "bc_call:actual params count too few!");
        }
      }
    }
    field = vgc_obj_ref_get(closure,field,vgc_array);
    vcontext_obj_push(ctx,field);
    base = vcontext_stack_top_get(ctx) + subr->locals_count;
    vcontext_stack_top_set(ctx,base);
    vcontext_obj_push(ctx,calling);
    vcontext_obj_push(ctx,obj);
    call = vgc_call_new(ctx,
                        vgc_call_type_subr,
                        params_count + 1,
                        base);
    if(!call){
      vcontext_exit(ctx, "bc_call:subr out of memory!");
    }

    vgc_obj_ref_set(ctx,calling,call);
    vcontext_obj_push(ctx, call);
  } else if(vgc_obj_typeof(obj,vgc_obj_type_subr)){
    vgc_subr* subr;
    vgc_call* call;
    usize_t params_count;
    usize_t base;

    subr = (vgc_subr*)obj;
    params_count = subr->params_count;
    if (subr->params_type == vgc_subr_params_flex) {
      if (!isapc) {
        vcontext_exit(ctx, "bc_call:actual params count much be provide!");
      }
      if (apc < params_count) {
        usize_t i = apc;
        vslot slot_null;
        vslot_null_set(slot_null);
        while (i < params_count) {
          vcontext_stack_push(ctx, slot_null);
          i++;
        }
      } else {
        params_count = apc;
      }
    } else {
      if (isapc) {
        if (apc > params_count) {
          vcontext_exit(ctx, "bc_call:actual params count too much!");
        }
        else if (apc < params_count) {
          vcontext_exit(ctx, "bc_call:actual params count too few!");
        }
      }
    }
    base = vcontext_stack_top_get(ctx) + subr->locals_count;
    vcontext_stack_top_set(ctx,base);
    vcontext_obj_push(ctx,calling);
    vcontext_obj_push(ctx,obj);
    call = vgc_call_new(ctx,
                        vgc_call_type_subr,
                        params_count,
                        base);
    if(!call){
      vcontext_exit(ctx, "bc_call:subr out of memory!");
    }

    vgc_obj_ref_set(ctx,calling,call);
    vcontext_obj_push(ctx, call);
  } else if(vgc_obj_typeof(obj,vgc_obj_type_cfun)){
    vgc_call* call;
    vgc_cfun* cfun;
    int has_retval;
    usize_t params_count;
    usize_t after_base;
    usize_t before_base;
    usize_t base;
    vslot slot_val;

    cfun = (vgc_cfun*)obj;
    params_count = cfun->params_count;
    if (isapc) {
      if (apc > params_count) {
        vcontext_exit(ctx, "bc_call:actual params count too much!");
      } else if (apc < params_count) {
        vcontext_exit(ctx, "bc_call:actual params count too few!");
      }
    }
    before_base = vcontext_stack_top_get(ctx);
    vcontext_obj_push(ctx,calling);
    vcontext_obj_push(ctx,obj);
    call = vgc_call_new(ctx,
                        vgc_call_type_cfun,
                        params_count,
                        before_base);
    if(!call){
      vcontext_exit(ctx, "bc_call:cfun out of memory!");
    }

    vgc_obj_ref_set(ctx,calling,call);
    cfun = vgc_obj_ref_get(call,cfun,vgc_cfun);
    has_retval = cfun->has_retval;
    (cfun->entry)(ctx);
    after_base = vcontext_stack_top_get(ctx);
    if(has_retval){
      if (after_base - before_base != 1) {
        vcontext_exit(ctx, "cfun claim return value but has no return!");
      }
      vcontext_stack_pop(ctx, &slot_val);
    }else{
      if (after_base != before_base) {
        vcontext_exit(ctx, "cfun claim has no return value!");
      }
    }
    call = vgc_obj_ref_get(ctx,calling,vgc_call);
    calling = vgc_obj_ref_get(call,caller,vgc_call);
    vgc_obj_ref_set(ctx,calling,calling);
    base = before_base - params_count;
    vcontext_stack_top_set(ctx,base);
    if (has_retval) {
      vcontext_stack_push(ctx,slot_val);
    }
  }else{
    vgc_obj_log(obj);
    vcontext_exit(ctx, "bc_call:can not execute!");
  }
UEND

UDEFUN(UFNAME bc_return,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vgc_call* calling;
  vgc_call* called;
  vgc_subr* subr;
  vslot slot_val;
  usize_t params_count;
  usize_t top;
  usize_t base;
UBEGIN
  calling = vgc_obj_ref_get(ctx,calling,vgc_call);
  if(calling->call_type == vgc_call_type_subr){
    subr = vgc_obj_ref_get(calling,subr,vgc_subr);
  }else{
    vcontext_exit(ctx, "bc_return:calling not a subr!");
    return;
  }
  top = vcontext_stack_top_get(ctx);
  if (top == calling->base + 1) {
    vslot_null_set(slot_val);
  } else {
    vcontext_stack_pop(ctx, &slot_val);
  }
  if (subr->params_count > calling->params_count) {
    params_count = subr->params_count;
  } else {
    params_count = calling->params_count;
  }
  base = calling->base - params_count - subr->locals_count;
  vcontext_stack_top_set(ctx,base);
  vcontext_stack_push(ctx,slot_val);
  called = vgc_obj_ref_get(calling,caller,vgc_call);
  vgc_obj_ref_set(ctx,calling,called);
UEND

UDEFUN(UFNAME bc_return_void,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vgc_call* calling;
  vgc_call* called;
  vgc_subr* subr;
  usize_t params_count;
  usize_t base;
UBEGIN
  calling = vgc_obj_ref_get(ctx,calling,vgc_call);
  if(calling->call_type == vgc_call_type_subr){
    subr = vgc_obj_ref_get(calling,subr,vgc_subr);
    if (subr->params_count > calling->params_count) {
      params_count = subr->params_count;
    }
    else {
      params_count = calling->params_count;
    }
    base = calling->base - params_count - subr->locals_count;
  }else{
    vcontext_exit(ctx, "bc_return_void:calling not a subr!");
    return;
  }
  vcontext_stack_top_set(ctx,base);
  called = vgc_obj_ref_get(calling,caller,vgc_call);
  vgc_obj_ref_set(ctx,calling,called);
UEND

UDEFUN(UFNAME bc_ref,
       UARGS (vcontext* ctx,
              usize_t index),
       URET void)
UDECLARE
  vslot slot_obj;
  vslot slot;
  vslot* slot_list;
  vgc_obj* obj;
  vgc_list* list;
  vgc_string* str;
UBEGIN
  vcontext_stack_pop(ctx,&slot_obj);
  if (vslot_is_ref(slot_obj)) {
    obj = vslot_ref_get(slot_obj,vgc_obj);
    if(!obj) {
      vcontext_exit(ctx, "vm:ref is UNULL object!");
    }
    if (vgc_obj_typeof(obj,vgc_obj_type_string)) {
      str = (vgc_string*)obj;
      if (!vgc_str_bound_check(str,index)) {
        vcontext_exit(ctx, "vm:ref string out of bounds!");        
      }
      vslot_char_set(slot,str->u.b[index]);
    } else {
      if(vgc_obj_ref_check(obj,index)) {
        vcontext_exit(ctx, "vm:ref object or array out of bounds!");
      }
      list = (vgc_list*)obj;
      slot_list = vgc_obj_slot_list(list);
      slot = slot_list[index];      
    }
    vcontext_stack_push(ctx,slot);    
  } else {
    vcontext_exit(ctx, "vm:ref obj is not reference!");
  }
UEND

UDEFUN(UFNAME bc_refs,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vslot slot_index;
  vslot slot_obj;
  usize_t index;
  vslot slot;
  vslot* slot_list;
  vgc_obj* obj;
  vgc_list* list;
  vgc_string* str;
UBEGIN
  vcontext_stack_pop(ctx,&slot_index);
  vcontext_stack_pop(ctx,&slot_obj);
  if (vslot_is_ref(slot_obj)) {
    obj = vslot_ref_get(slot_obj,vgc_obj);
    if(!obj) {
      vcontext_exit(ctx, "vm:ref is UNULL object!");
    }
    if (!vslot_is_int(slot_index)) {
      vcontext_exit(ctx, "vm:ref index not a integer!");
    }
    index = vslot_int_get(slot_index);
    if (vgc_obj_typeof(obj,vgc_obj_type_string)) {
      str = (vgc_string*)obj;
      if (!vgc_str_bound_check(str,index)) {
        vcontext_exit(ctx, "vm:ref string out of bounds!");        
      }
      vslot_char_set(slot,str->u.b[index]);
    } else {
      if(vgc_obj_ref_check(obj,index)) {
        vcontext_exit(ctx, "vm:ref object or array out of bounds!");
      }
      list = (vgc_list*)obj;
      slot_list = vgc_obj_slot_list(list);
      slot = slot_list[index];      
    }
    vcontext_stack_push(ctx,slot);    
  } else {
    vcontext_exit(ctx, "vm:ref obj is not reference!");
  }
UEND

UDEFUN(UFNAME bc_ldas,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vslot slot_index;
  usize_t index;
UBEGIN
  vcontext_stack_pop(ctx,&slot_index);
  if (!vslot_is_int(slot_index)) {
    vcontext_exit(ctx, "vm:ldas index not a integer!");
  }
  index = vslot_int_get(slot_index);
  bc_push(ctx,bc_params(ctx,index));
UEND

UDEFUN(UFNAME bc_stas,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vslot slot_index;
  usize_t index;
UBEGIN
  vcontext_stack_pop(ctx,&slot_index);
  if (!vslot_is_int(slot_index)) {
    vcontext_exit(ctx, "vm:stas index not a integer!");
  }
  index = vslot_int_get(slot_index);
  bc_store_params(ctx,index);
UEND

UDEFUN(UFNAME bc_set,
       UARGS (vcontext* ctx,
              usize_t index),
       URET void)
UDECLARE
  vslot slot_obj;
  vslot slot_val;
  vslot* slot_list;
  vgc_obj* obj;
  vgc_list* list;
  vgc_string* str;
  int val;
UBEGIN
  vcontext_stack_pop(ctx,&slot_obj);
  vcontext_stack_pop(ctx,&slot_val);
  if (vslot_is_ref(slot_obj)) {
    obj = vslot_ref_get(slot_obj,vgc_obj);
    if (!obj) {
      vcontext_exit(ctx, "vm:set is UNULL object!");
    }
    if (vgc_obj_typeof(obj,vgc_obj_type_string)) {
      str = (vgc_string*)obj;
      if (!vgc_str_bound_check(str,index)) {
        vcontext_exit(ctx, "vm:set obj error!");        
      }
      if (!vslot_is_int(slot_val)) {
        vcontext_exit(ctx, "vm:set val is not integer!");
      }
      val = vslot_int_get(slot_val);
      str->u.b[index] = val;
    } else {
      if(vgc_obj_ref_check(obj,index)) {
        vcontext_exit(ctx, "vm:set obj error!");
      }
      list = (vgc_list*)obj;
      slot_list = vgc_obj_slot_list(list);
      slot_list[index] = slot_val;
    }
  } else {
    vcontext_exit(ctx, "vm:set obj is not reference!");
  }
UEND

UDEFUN(UFNAME bc_sets,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vslot slot_index;
  vslot slot_obj;
  vslot slot_val;
  usize_t index;
  vslot* slot_list;
  vgc_obj* obj;
  vgc_list* list;
  vgc_string* str;
  int val;
UBEGIN
  vcontext_stack_pop(ctx,&slot_index);
  vcontext_stack_pop(ctx,&slot_obj);
  vcontext_stack_pop(ctx,&slot_val);
  if (vslot_is_ref(slot_obj)) {
    obj = vslot_ref_get(slot_obj,vgc_obj);
    if (!obj) {
      vcontext_exit(ctx, "vm:set is UNULL object!");
    }
    if (!vslot_is_int(slot_index)) {
      vcontext_exit(ctx, "vm:set index not a integer!");
    }
    index = vslot_int_get(slot_index);
    if (vgc_obj_typeof(obj,vgc_obj_type_string)) {
      str = (vgc_string*)obj;
      if (!vgc_str_bound_check(str,index)) {
        vcontext_exit(ctx, "vm:set obj error!");        
      }
      if (!vslot_is_int(slot_val)) {
        vcontext_exit(ctx, "vm:set val is not integer!");
      }
      val = vslot_int_get(slot_val);
      str->u.b[index] = val;
    } else {
      if(vgc_obj_ref_check(obj,index)) {
        vcontext_exit(ctx, "vm:set obj error!");
      }
      list = (vgc_list*)obj;
      slot_list = vgc_obj_slot_list(list);
      slot_list[index] = slot_val;
    }
  } else {
    vcontext_exit(ctx, "vm:set obj is not reference!");
  }
UEND

#define CHECK_CURR_CALL \
  do{ \
    calling = vgc_obj_ref_get(ctx,calling,vgc_call); \
    if(!calling || vgc_call_is_cfun(calling)) { \
      goto context_exit; \
    } \
    subr = vgc_obj_ref_get(calling,subr,vgc_subr); \
    bytecode = vgc_obj_ref_get(subr,bytecode,vgc_string); \
  }while(0)

#define FETCH *(unsigned char*)(bytecode->u.b + calling->pc)

#define NEXT \
  do { \
    op = FETCH; \
    calling->pc++; \
  } while(0)

#define NEXT2 \
  do{ \
    op = FETCH; \
    calling->pc++; \
    op += FETCH << 8; \
    calling->pc++; \
  } while(0)

UDEFUN(UFNAME vcontext_execute,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  vgc_call* calling;
  vgc_subr* subr;
  vgc_string* bytecode;
UBEGIN
  bc_call(ctx,0,0);
  ustdc_setjmp(ctx->envbuf);
  while(1){
    int op;
    CHECK_CURR_CALL;
    NEXT;
    switch(op){
    case Bnop:
      udebug0("Bnop");
      break;
    case Bload:
      NEXT;
      udebug1("Bload %d",op);
      bc_push(ctx,bc_locals(ctx,op));
      break;
    case Blda:
      NEXT;
      udebug1("Blda %d",op);
      bc_push(ctx,bc_params(ctx,op));
      break;
    case Bldas:
      udebug0("Bldas");
      bc_ldas(ctx);
      break;
    case Bstore:
      NEXT;
      udebug1("Bstore %d",op);
      bc_store_locals(ctx,op);
      break;
    case Bsta:
      NEXT;
      udebug1("Bsta %d",op);
      bc_store_params(ctx,op);
      break;
    case Bstas:
      udebug0("Bstas");
      bc_stas(ctx);
      break;
    case Bpush:
      NEXT;
      udebug1("Bpush %d",op);
      bc_push(ctx,bc_constant_get(ctx,op));
      break;
    case Btop:
      NEXT;
      op = (signed char)op;
      udebug1("Btop %d",op);
      bc_top(ctx,op);
      break;
    case Bpop:
      NEXT;
      udebug1("Bpop %d",op);
      bc_pop(ctx,op);
      break;
    case Bpopv:
      udebug0("Bpopv");
      bc_popv(ctx);
      break;
    case Bjmp:
      NEXT2;
      udebug1("Bjmp %d",op);
      bc_jmp(ctx,op);
      break;
    case Bjmpi:
      NEXT2;
      udebug1("Bjmpi %d",op);
      bc_jmpi(ctx,op);
      break;
    case Balen:
      udebug0("Balen");
      bc_alen(ctx);
      break;
    case Beq:
      udebug0("Beq");
      bc_eq(ctx);
      break;
    case Bgt:
      udebug0("Bgt");
      bc_gt(ctx);
      break;
    case Blt:
      udebug0("Blt");
      bc_lt(ctx);
      break;
    case Band:
      udebug0("Band");
      bc_and(ctx);
      break;
    case Bor:
      udebug0("Bor");
      bc_or(ctx);
      break;
    case Bnot:
      udebug0("Bnot");
      bc_not(ctx);
      break;
    case Badd:
      udebug0("Badd");
      bc_add(ctx);
      break;
    case Bsub:
      udebug0("Bsub");
      bc_sub(ctx);
      break;
    case Bmul:
      udebug0("Bmul");
      bc_mul(ctx);
      break;
    case Bdiv:
      udebug0("Bdiv");
      bc_div(ctx);
      break;
    case Bcall:
      udebug0("Bcall");
      bc_call(ctx,0,0);
      break;
    case Bcallx:
      NEXT;
      op = (signed char)op;
      udebug1("Bcallx %d",op);
      bc_call(ctx,1,op);
      break;
    case Breturn:
      udebug0("Breturn");
      bc_return(ctx);
      break;
    case Bretvoid:
      udebug0("Bretvoid");
      bc_return_void(ctx);
      break;
    case Brefs:
      udebug0("Brefs");
      bc_refs(ctx);
      break;
    case Brefg:
      {
        vslot slot;
        NEXT;
        udebug1("Brefg %d",op);
        slot = bc_constant_get(ctx,op);
        if (!vslot_is_int(slot)) {
          vcontext_exit(ctx, "not a integer!");
        }
        bc_ref(ctx,vslot_int_get(slot));
        break;
      }
    case Brefl:
      {
        vslot slot;
        NEXT2;
        udebug1("Brefl %d",op);
        slot = bc_locals(ctx,op);
        if (!vslot_is_int(slot)) {
          vcontext_exit(ctx, "not a integer!");
        }
        bc_ref(ctx,vslot_int_get(slot));
        break;
      }
    case Bref:
      NEXT2;
      udebug1("Bref %d",op);
      bc_ref(ctx,op);
      break;
    case Bsets:
      udebug0("Bsets");
      bc_sets(ctx);
      break;
    case Bsetg:
      {
        vslot slot;
        NEXT;
        udebug1("Bsetg %d",op);
        slot = bc_constant_get(ctx,op);
        if (!vslot_is_int(slot)) {
          vcontext_exit(ctx, "not a integer!");
        }
        bc_set(ctx,vslot_int_get(slot));
        break;
      }
    case Bsetl:
      {
        vslot slot;
        NEXT2;
        udebug1("Bsetl %d",op);
        slot = bc_locals(ctx,op);
        if (!vslot_is_int(slot)) {
          vcontext_exit(ctx, "not a integer!");
        }
        bc_set(ctx,vslot_int_get(slot));
        break;
      }
    case Bset:
      NEXT2;
      udebug1("Bset %d",op);
      bc_set(ctx,op);
      break;
    default:
      vcontext_exit(ctx, "bad byte code!");
    }
  }
  context_exit:
  return;
UEND

#ifndef _VM_H_
#define _VM_H_

#include "ulist.h"
#include "vgc_obj.h"
#include "vgenbc.h"
#include "vm_base.h"
#include "vcontext.h"

#define VMOD_FUSE_YES 1
#define VMOD_FUSE_NO 0

struct _vm_m1{
  VMHEADER;
  vgc_heap* heap;
  vcontext* ctx_main;
  usize_t ctx_count;
  uhstb_vmod* mods;
  vmod* mod_main;
  int mod_fuse;
  vmod_loader* loader;
  ustring_table* symtb;
  ustring_table* strtb;
};

#define vm_m1_mod_fuse(vm,fuse) \
  (vm)->mod_fuse = fuse

UDECLFUN(UFNAME vm_m1_h1_alloc,
         UARGS (uallocator* allocator,
                int vm_gc_ssz,
                int vm_gc_asz,
                int vm_gc_rsz),
         URET vapi vm_m1* vcall);

UDECLFUN(UFNAME vm_m1_mod_loader_set,
         UARGS (vm_m1* m1,
                vmod_loader* loader),
         URET vapi void vcall);

#endif

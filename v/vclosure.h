#ifndef _VCLOSURE_H_
#define _VCLOSURE_H_

#include "ulist_tpl.h"
#include "vmacro.h"
#include "vpass.h"

typedef struct _vclosure_base vclosure_base;
typedef struct _vclosure vclosure,*vclosurep;
typedef struct _vclosure_named vclosure_named;
typedef struct _vclosure_prog vclosure_prog;
typedef struct _vclosure_file vclosure_file, vclosure_main;
typedef struct _vclosure_func vclosure_func;
typedef struct _vclosure_none vclosure_none;
typedef struct _vclosure_stru vclosure_stru;
typedef struct _vclosure_code vclosure_code;
typedef struct _vclosure_data vclosure_data,*vclosure_datap;
typedef struct _vps_closure_req vps_closure_req;

#ifndef _UMAP_TPL_VCLOSUREP_
umap_decl_tpl(vclosurep);
#endif
#ifndef _ULIST_TPL_VCLOSUREP_
ulist_decl_tpl(v,vclosurep);
#endif
#ifndef _UMAP_TPL_VCLOSURE_DATAP_
umap_decl_tpl(vclosure_datap);
#endif
#ifndef _ULIST_TPL_VCLOSURE_DATAP_
ulist_decl_tpl(v,vclosure_datap);
#endif

#define VCLOSURE_TYPE_FILE 0
#define VCLOSURE_TYPE_FUNC 1
#define VCLOSURE_TYPE_MAIN 2
#define VCLOSURE_TYPE_NONE 3
#define VCLOSURE_TYPE_STRU 4
#define VCLOSURE_TYPE_CODE 5
#define VCLOSURE_TYPE_DATA 6

#define VCLOSURE_STORE_DECL 0
#define VCLOSURE_STORE_IMPL 1

#define VCLOSURE_BASE_HEADER                    \
  unsigned char closure_type;                   \
  unsigned char named_flag;                     \
  unsigned char prog_flag;                      \
  unsigned char change_flag;                    \
  unsigned char scope;                          \
  vclosure* parent

struct _vclosure_base{
  VCLOSURE_BASE_HEADER;
};

#define VCLOSURE_HEADER                         \
  VCLOSURE_BASE_HEADER;                         \
  ulist_vclosure_datap* fields;                 \
  ulist_vclosurep* childs

struct _vclosure{
  VCLOSURE_HEADER;
};

#define vclosure_is_change(closure)             \
  ((closure)->change_flag)

#define VCLOSURE_NAMED_HEADER                   \
  VCLOSURE_HEADER;                              \
  ustring* closure_name

struct _vclosure_named{
  VCLOSURE_NAMED_HEADER;
};

#define vclosure_isnamed(closure)               \
  (closure->named_flag)

struct _vclosure_none{
  VCLOSURE_HEADER;
};

#define VCLOSURE_PROG_HEADER                    \
  VCLOSURE_NAMED_HEADER;                        \
  vcfg_graph* init

struct _vclosure_prog{
  VCLOSURE_PROG_HEADER;
};

#define vclosure_isprog(closure)                \
  (closure->prog_flag)

struct _vclosure_file{
  VCLOSURE_PROG_HEADER;
  ustring* closure_path;
};

#define VCLOSURE_FUNC_YES 1
#define VCLOSURE_FUNC_NO 0

struct _vclosure_func{
  VCLOSURE_PROG_HEADER;
  unsigned char store_type;
  unsigned char closure_flag;
};

struct _vclosure_stru{
  VCLOSURE_NAMED_HEADER;
};

struct _vclosure_code{
  VCLOSURE_HEADER;
  vclosure_prog* prog;
};

struct _vclosure_data{
  VCLOSURE_BASE_HEADER;
  unsigned char store_type;
  ustring* closure_name;
  vps_data* data;
};

#define VPS_CLOSURE_REQ_HEADER \
  VAST_ATTR_REQ_HEADER; \
  vps_cntr* vps; \
  vclosure* closure

struct _vps_closure_req{
  VPS_CLOSURE_REQ_HEADER;
};

#define vclosure_isfile(closure)                        \
  ((closure)->closure_type == VCLOSURE_TYPE_FILE)

#define vclosure_ismain(closure)                        \
  ((closure)->closure_type == VCLOSURE_TYPE_MAIN)

#define vclosure_isfunc(closure)                        \
  ((closure)->closure_type == VCLOSURE_TYPE_FUNC)

#define vclosure_isnone(closure)                        \
  ((closure)->closure_type == VCLOSURE_TYPE_NONE)

#define vclosure_isstru(closure)                        \
  ((closure)->closure_type == VCLOSURE_TYPE_STRU)

#define vclosure_iscode(closure)                        \
  ((closure)->closure_type == VCLOSURE_TYPE_CODE)

#define vclosure_isdecl(closure)                        \
  ((closure)->store_type == VCLOSURE_STORE_DECL)

#define vclosure_init_get(closure) \
  vclosure_isprog(closure) ? ((vclosure_prog*)(closure))->init : \
    (vclosure_iscode(closure) ? ((vclosure_code*)(closure))->prog->init : UNULL)

#define vclosure_base_obj_new(vps,type) \
  (type*)vclosure_base_new(vps,sizeof(type))

#define vclosure_obj_new(vps,type) \
  (type*)vclosure_new(vps,sizeof(type))

#define vclosure_is_source(closure) \
  (vclosure_ismain(closure) || \
   vclosure_isfile(closure))
   
#define vclosure_is_body(closure) \
  (vclosure_ismain(closure) || \
   vclosure_isfile(closure) || \
   vclosure_isfunc(closure) || \
   vclosure_iscode(closure))

UDECLFUN(UFNAME vclosure_base_new,
         UARGS (vps_cntr* vps,
                int struct_size),
         URET vapi vclosure_base* vcall);

UDECLFUN(UFNAME vclosure_new,
         UARGS (vps_cntr* vps,
                int struct_size),
         URET vapi vclosure* vcall);

UDECLFUN(UFNAME vclosure_file_new,
         UARGS (vps_cntr* vps,
                int closure_type,
                ustring* name,
                ustring* path),
         URET vapi vclosure_file* vcall);

UDECLFUN(UFNAME vclosure_file_name_set,
         UARGS (vclosure_file* file,
                ustring* closure_name),
         URET vapi void vcall);

UDECLFUN(UFNAME vclosure_func_new,
         UARGS (vps_cntr* vps,
                ustring* name,
                int store_type,
                int scope),
         URET vapi vclosure_func* vcall);

UDECLFUN(UFNAME vclosure_none_new,
         UARGS (vps_cntr* vps),
         URET vapi vclosure_none* vcall);

UDECLFUN(UFNAME vclosure_stru_new,
         UARGS (vps_cntr* vps,
                ustring* name,
                int scope),
         URET vapi vclosure_stru* vcall);

UDECLFUN(UFNAME vclosure_code_new,
         UARGS (vps_cntr* vps,
                vclosure_prog* prog),
         URET vapi vclosure_code* vcall);

UDECLFUN(UFNAME vclosure_data_new_by_vps_data,
         UARGS (vps_cntr* vps,
                int store_type,
                vps_data* dt),
         URET vapi vclosure_data* vcall);

UDECLFUN(UFNAME vclosure_data_new,
         UARGS (vps_cntr* vps,
                ustring* name,
                int store_type,
                int scope),
         URET vapi vclosure_data* vcall);

UDECLFUN(UFNAME vfile2closure,
         UARGS (vps_closure_req* req,
                vclosure* parent,
                vreader* reader,
                ustring* name,
                ustring* path,
                vps_cntr* vps,
                vps_data* cmd_args,
                int closure_type),
         URET vapi vclosure* vcall);
         
UDECLFUN(UFNAME vstream2closure,
         UARGS (vps_closure_req* req,
                vclosure* parent,
                vreader* reader,
                ustring* name,
                ustring* path,
                vps_cntr* vps,
                ustream* stream),
         URET vapi vclosure* vcall);
       
UDECLFUN(UFNAME vclosure2mod,
         UARGS (vclosure* closure,vps_cntr* vps,vps_mod* mod),
         URET vapi void vcall);

UDECLFUN(UFNAME vclosure2vps,
         UARGS (vps_closure_req* req,
                vreader* reader,
                ustring* name,
                ustring* path,
                vps_cntr* vps),
         URET vapi vps_mod* vcall);

UDECLFUN(UFNAME vclosure_field_add,
         UARGS (vclosure* closure,vclosure_data* field),
         URET vapi int vcall);

UDECLFUN(UFNAME vclosure_child_add,
         UARGS (vclosure* closure,vclosure* child),
         URET vapi int vcall);

UDECLFUN(UFNAME vclosure_field_get,
         UARGS (vclosure* closure,ustring* name),
         URET vapi vclosure_data* vcall);

UDECLFUN(UFNAME vclosure_curr_field_get,
         UARGS (vclosure* closure,ustring* name),
         URET vapi vclosure_data* vcall);

UDECLFUN(UFNAME vclosure_func_get,
         UARGS (vclosure* closure,ustring* name),
         URET vapi vclosure* vcall);

UDECLFUN(UFNAME vclosure_func_args_count,
         UARGS (vclosure* closure),
         URET vapi int vcall);

UDECLFUN(UFNAME vclosure_file_get,
         UARGS (vclosure* closure,ustring* path),
         URET vapi vclosure* vcall);

UDECLFUN(UFNAME vclosure_stru_field_get,
         UARGS (vclosure* closure,ustring* name),
         URET vapi vclosure_data* vcall);

UDECLFUN(UFNAME vclosure_stru_child_get,
         UARGS (vclosure* closure,ustring* name),
         URET vapi vclosure* vcall);

UDECLFUN(UFNAME vclosure_stru_get,
         UARGS (vclosure* closure,ustring* name),
         URET vapi vclosure* vcall);

UDECLFUN(UFNAME vclosure_child_index_of,
         UARGS (vclosure* index_child),
         URET vapi int vcall);

UDECLFUN(UFNAME vclosure_field_index_of,
         UARGS (vclosure_data* field),
         URET vapi int vcall);

UDECLFUN(UFNAME vclosure_size_of,
         UARGS (vclosure* closure),
         URET vapi int vcall);

UDECLFUN(UFNAME vclosure_path_get,
         UARGS (vreader* reader,ustring* name),
         URET vapi ustring* vcall);

UDECLFUN(UFNAME vclosure_code_apd,
         UARGS(vclosure_code* code,vps_inst* inst),
         URET vapi void vcall);

UDECLFUN(UFNAME vclosure_func_params_add,
         UARGS(vclosure* closure, vclosure_data* data),
         URET vapi int vcall);

UDECLFUN(UFNAME vclosure_func_locals_add,
         UARGS(vclosure* closure, vclosure_data* data),
         URET vapi int vcall);

UDECLFUN(UFNAME vclosure_func_locals_push,
         UARGS(vclosure* closure, vclosure_data* data),
         URET vapi int vcall);

UDECLFUN(UFNAME vclosure_func_locals_pop,
         UARGS(vclosure* closure),
         URET vapi void vcall);

#endif

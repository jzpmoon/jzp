#ifndef _VCONTEXT_H_
#define _VCONTEXT_H_

#include "ustring.h"
#include "vmacro.h"
#include "vgc_obj.h"
#include "vpass.h"
#include "vgenbc.h"

typedef struct _vm_m1 vm_m1;
typedef struct _vmod vmod;
typedef struct _vcontext vcontext;
typedef struct _vreloc vreloc;
typedef struct _vsymbol vsymbol;
typedef struct _vcat vcat;
typedef struct _vgc_subr vgc_subr;
typedef struct _vgc_cfun vgc_cfun;
typedef struct _vmod_loader vmod_loader;
typedef struct _vgc_call vgc_call;
typedef struct _vgc_closure vgc_closure;

struct _vreloc{
  ustring* ref_name;
  vgc_array* rel_obj;
  int rel_idx;
};

struct _vsymbol{
  ustring* name;
  vslot slot;
};

#define vreloc_log(reloc) \
  udebug1("ref_name:%s",reloc->ref_name->value); \
  udebug1("rel_obj:%p",reloc->rel_obj); \
  udebug1("rel_idx:%d",reloc->rel_idx);

#ifndef _UMAP_TPL_VRELOC_
umap_decl_tpl(vreloc);
#endif
#ifndef _UMAP_TPL_VSYMBOL_
umap_decl_tpl(vsymbol);
#endif
#ifndef _ULIST_TPL_VRELOC_
ulist_decl_tpl(v,vreloc);
#endif
#ifndef _UHSTB_TPL_VSYMBOL_
uhstb_decl_tpl(v,vsymbol);
#endif

#define VMOD_STATUS_LOADED 0x1
#define VMOD_STATUS_INITED 0x2

struct _vmod{
  ulist_vreloc* rells;
  uhstb_vsymbol* gobjtb;
  uhstb_vsymbol* lobjtb;
  struct _vgc_subr* init;
  ustring* name;
  ustring* path;
  unsigned char status;
};

#define vmod_loaded(mod) \
  ((mod)->status |= VMOD_STATUS_LOADED)

#define vmod_unload(mod) \
  ((mod)->status &= ~VMOD_STATUS_LOADED)

#define vmod_isload(mod) \
  ((mod)->status & VMOD_STATUS_LOADED)

#define vmod_inited(mod) \
  ((mod)->status |= VMOD_STATUS_INITED)

#define vmod_uninit(mod) \
  ((mod)->status &= ~VMOD_STATUS_INITED)

#define vmod_isinit(mod) \
  ((mod)->status & VMOD_STATUS_INITED)

#ifndef _UMAP_TPL_VMOD_
umap_decl_tpl(vmod);
#endif
#ifndef _UHSTB_TPL_VMOD_
uhstb_decl_tpl(v,vmod);
#endif

typedef int(vcall *vmod_load_ft)(vmod_loader* loader,vmod* mod);

#define VMOD_LOADER_HEADER \
  vmod_load_ft load

struct _vmod_loader{
  VMOD_LOADER_HEADER;
};

struct _vcontext{
  VGCLHEADER;
  vm_m1* vm;
  vgc_heap* heap;
  ustack_vslot* stack;
  uhstb_vmod* mods;
  vmod_loader* loader;
  ustring_table* symtb;
  ustring_table* strtb;
  ustdc_jmp_buf envbuf;
  vslot_define_begin
    vslot_define(vgc_call,calling);
    vslot_define(vgc_obj,eobj);
  vslot_define_end
};

#define vcontext_alloc_get(ctx) \
  (ctx)->vm->allocator

struct _vcat {
  usize_t begin;
  usize_t end;
  usize_t todo;
};

enum {
  vgc_subr_params_flex,
  vgc_subr_params_fixed,
};

struct _vgc_subr{
  VGCLHEADER;
  unsigned char params_type;
  usize_t params_count;
  usize_t locals_count;
  usize_t catb_count;
  ustring* subr_name;
  vslot_define_begin
    vslot_define(vgc_array,consts);
    vslot_define(vgc_string,bytecode);
  vslot_define_end
  vcat catb[1];
};

UDECLFUN(UFNAME vgc_subr_new,
         UARGS (vcontext* ctx,
                ustring* subr_name,
                usize_t params_count,
                usize_t locals_count,
                usize_t catb_count,
                unsigned char params_type,
                int area_type),
         URET vgc_subr*);

UDECLFUN(UFNAME vgc_subr_cat_set,
         UARGS (vgc_subr* subr,
                usize_t index,
                vcat cat),
         URET void);

UDECLFUN(UFNAME vgc_subr_cat_get,
         UARGS (vgc_subr* subr,
                usize_t index),
         URET vcat);

typedef int(*vcfun_ft)(vcontext*);

struct _vgc_cfun{
  VGCHEADER;
  vcfun_ft entry;
  usize_t params_count;
  ustring* cfun_name;
  int has_retval;
};

UDECLFUN(UFNAME vgc_cfun_new,
         UARGS (vgc_heap* heap,
                ustring* cfun_name,
                vcfun_ft entry,
                usize_t params_count,
                int has_retval,
                int area_type),
         URET vapi vgc_cfun* vcall);

enum {
  vgc_call_type_cfun,
  vgc_call_type_subr,
};

struct _vgc_call{
  VGCLHEADER;
  unsigned char call_type;
  usize_t params_count;
  usize_t base;
  usize_t pc;
  vslot_define_begin
    vslot_define(vgc_cfun,cfun);
    vslot_define(vgc_subr,subr);
    vslot_define(vgc_call,caller);
  vslot_define_end
};

UDECLFUN(UFNAME vgc_call_new,
         UARGS (vcontext* ctx,
                unsigned char call_type,
                usize_t params_count,
                usize_t base),
         URET vgc_call*);

#define vgc_call_is_cfun(call) \
  ((call)->call_type == vgc_call_type_cfun)

struct _vgc_closure{
  VGCLHEADER;
  vslot_define_begin
    vslot_define(vgc_array,field);
    vslot_define(vgc_subr,subr);
  vslot_define_end
};

UDECLFUN(UFNAME vgc_closure_new,
         UARGS (vgc_heap* heap,
                vgc_array* field,
                vgc_subr* subr),
         URET vgc_closure*);
  
UDECLFUN(UFNAME vcontext_new,
         UARGS (vm_m1* vm,
                vgc_heap* heap),
         URET vapi vcontext* vcall);

UDECLFUN(UFNAME vcontext_params_get,
         UARGS (vcontext* ctx,int index),
         URET vapi vslot vcall);

UDECLFUN(UFNAME vcontext_vps_load,
         UARGS (vcontext* ctx,vps_cntr* vps),
         URET vapi int vcall);

UDECLFUN(UFNAME vcontext_mod_load,
         UARGS (vcontext* ctx,vps_mod* mod),
         URET vapi int vcall);

UDECLFUN(UFNAME vcontext_graph_load,
         UARGS (vcontext* ctx,vmod* mod,vcfg_graph* grp),
         URET vgc_subr*);

UDECLFUN(UFNAME vcontext_execute,
         UARGS (vcontext* ctx),
         URET void);

UDECLFUN(UFNAME vcontext_relocate,
         UARGS (vcontext* ctx),
         URET void);

UDECLFUN(UFNAME vcontext_mod_log,
         UARGS (vcontext* ctx),
         URET vapi void vcall);

UDECLFUN(UFNAME vcontext_mod_gobjtb_log,
         UARGS (vcontext* ctx,ustring* name),
         URET vapi void vcall);
         
UDECLFUN(UFNAME vcontext_mod_lobjtb_log,
         UARGS (vcontext* ctx,ustring* name),
         URET vapi void vcall);
         
UDECLFUN(UFNAME vcontext_mod_add,
         UARGS (vcontext* ctx,ustring* name,ustring* path),
         URET vapi vmod* vcall);

UDECLFUN(UFNAME vcontext_mod_get,
         UARGS (vcontext* ctx,
                ustring* name,
                ustring* path),
         URET vapi vmod* vcall);

UDECLFUN(UFNAME vcontext_mod2mod,
         UARGS (vcontext* ctx,vmod* dest_mod,vps_mod* src_mod),
         URET vapi int vcall);

UDECLFUN(UFNAME vmod_add_reloc,
         UARGS (vmod* mod,vreloc reloc),
         URET void);

UDECLFUN(UFNAME vmod_symbol_get,
         UARGS (vcontext* ctx,vmod* mod,ustring* name),
         URET vapi vsymbol* vcall);

UDECLFUN(UFNAME vmod_gobj_put,
         UARGS (vgc_heap* heap,vmod* mod,ustring* name,vgc_obj* obj),
         URET vapi vsymbol* vcall);

UDECLFUN(UFNAME vmod_lobj_put,
         UARGS (vgc_heap* heap,vmod* mod,ustring* name,vgc_obj* obj),
         URET vapi vsymbol* vcall);

UDECLFUN(UFNAME vmod_gslot_put,
         UARGS (vgc_heap* heap,vmod* mod,ustring* name,vslot slot),
         URET vapi vsymbol* vcall);

UDECLFUN(UFNAME vmod_lslot_put,
         UARGS (vgc_heap* heap,vmod* mod,ustring* name,vslot slot),
         URET vapi vsymbol* vcall);

UDECLFUN(UFNAME vcontext_stack_get,
         UARGS (vcontext* ctx,usize_t index),
         URET vslot);

UDECLFUN(UFNAME vcontext_stack_set,
         UARGS (vcontext* ctx,usize_t index,vslot slot),
         URET void);

UDECLFUN(UFNAME vcontext_stack_top_get,
         UARGS (vcontext* ctx),
         URET usize_t);

UDECLFUN(UFNAME vcontext_stack_top_set,
         UARGS (vcontext* ctx,usize_t index),
         URET void);

UDECLFUN(UFNAME vcontext_stack_push,
         UARGS (vcontext* ctx, vslot slot),
         URET vapi void vcall);

UDECLFUN(UFNAME vcontext_stack_pop,
         UARGS (vcontext* ctx, vslot* slotp),
         URET vapi void vcall);

UDECLFUN(UFNAME vcontext_stack_pushv,
         UARGS (vcontext* ctx),
         URET vapi void vcall);

#define vcontext_obj_push(ctx,obj) \
  do{ \
    vslot __slot; \
    vslot_ref_set(__slot,obj); \
    vcontext_stack_push(ctx,__slot); \
  } while(0)

#define vcontext_obj_pop(ctx,obj,obj_type) \
  do{ \
    vslot __slot; \
    vcontext_stack_pop(ctx,&__slot); \
    obj = vslot_ref_get(__slot,obj_type); \
  } while(0)

#define vcontext_obj_slot_get(ctx,obj,slot) \
  vcontext_stack_push(ctx,(obj)->_u.slot);
  
#define vcontext_obj_slot_set(ctx,obj,slot) \
  vcontext_stack_pop(ctx,&((obj)->_u.slot));

UDECLFUN(UFNAME vcontext_exit,
         UARGS(vcontext * ctx, char* msg, ...),
         URET vapi void vcall);

UDECLFUN(UFNAME vcontext_exception_throw,
         UARGS (vcontext* ctx,vslot eobj),
         URET vapi void vcall);

#endif

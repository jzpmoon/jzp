#include "uref.h"
#include "uhstb_tpl.c"
#include "ulist_tpl.c"
#include "vcontext.h"
#include "vm.h"

#ifndef _ULIST_TPL_VRELOC_
ulist_def_tpl(v,vreloc);
#endif
#ifndef _UHSTB_TPL_VMOD_
uhstb_def_tpl(v,vmod);
#endif
#ifndef _UHSTB_TPL_VSYMBOL_
uhstb_def_tpl(v,vsymbol);
#endif

UDECLFUN(UFNAME gdata_load,
         UARGS (vcontext* ctx,vmod* mod,vps_data* data),
         URET static void);

UDECLFUN(UFNAME ldata_load,
         UARGS (vgc_heap* heap,vgc_array* consts,vps_data* data),
         URET static void);

UDECLFUN(UFNAME data2data,
         UARGS (vgc_heap* heap,vps_data* data),
         URET static vslot);

UDECLFUN(UFNAME insts_concat,
         UARGS (vgc_array* consts,
                ulist_vps_cfgp* cfgs,
                vcfg_block* blk,
                vmod* mod,
                ulist_vps_instp* insts),
         URET static int);

UDEFUN(UFNAME vcontext_new,
       UARGS (vm_m1* vm,
              vgc_heap* heap),
       URET vapi vcontext* vcall)
UDECLARE
  vcontext* ctx;
UBEGIN
  ctx = vgc_heap_obj_new(heap,
                         vcontext,
                         vgc_obj_type_ctx,
                         vgc_heap_area_static);

  if(!ctx){
    uabort("vcontext_new: ctx new error!");
  }

  ctx->vm = vm;
  ctx->heap = heap;
  ctx->stack = &heap->root_set;
  ctx->mods = UNULL;
  ctx->loader = UNULL;
  ctx->symtb = UNULL;
  ctx->strtb = UNULL;
  vgc_obj_null_set(ctx,calling);
  vgc_obj_null_set(ctx,eobj);

  return ctx;
UEND

UDEFUN(UFNAME vgc_subr_new,
       UARGS (vcontext* ctx,
              ustring* subr_name,
              usize_t params_count,
              usize_t locals_count,
              usize_t catb_count,
              unsigned char params_type,
              int area_type),
       URET vgc_subr*)
UDECLARE
  vgc_subr* subr;
  usize_t subr_size;
  usize_t offset;
  usize_t count;
UBEGIN
  subr_size = TYPE_SIZE_OF(vgc_subr,vcat,catb_count);
  offset = vgc_obj_solt_offset(vgc_subr);
  count = vgc_obj_slot_count(vgc_subr);
  subr = (vgc_subr*)vgc_heap_data_new(ctx->heap,
                                      subr_size,
                                      offset,
                                      count,
                                      vgc_obj_type_subr,
                                      area_type);
  if(subr){
    subr->params_count = params_count;
    subr->locals_count = locals_count;
    subr->catb_count = catb_count;
    subr->params_type = params_type;
    subr->subr_name = subr_name;
    vcontext_obj_slot_set(ctx,subr,consts);
    vcontext_obj_slot_set(ctx,subr,bytecode);
  }
  return subr;
UEND

UDEFUN(UFNAME vgc_subr_cat_set,
       UARGS (vgc_subr* subr,
              usize_t index,
              vcat cat),
       URET void)
UDECLARE

UBEGIN
  subr->catb[index] = cat;
UEND

UDEFUN(UFNAME vgc_subr_cat_get,
       UARGS (vgc_subr* subr,
              usize_t index),
       URET vcat)
UDECLARE

UBEGIN
  return subr->catb[index];
UEND

UDEFUN(UFNAME vgc_cfun_new,
       UARGS (vgc_heap* heap,
              ustring* cfun_name,
              vcfun_ft entry,
              usize_t params_count,
              int has_retval,
              int area_type),
       URET vapi vgc_cfun* vcall)
UDECLARE
  vgc_cfun* cfun;
UBEGIN
  if(params_count < 0){
    return UNULL;
  }
  cfun = (vgc_cfun*)vgc_heap_data_new(heap,
                                      sizeof(vgc_cfun),
                                      0,
                                      0,
                                      vgc_obj_type_cfun,
                                      area_type);
  if(cfun){
    cfun->entry = entry;
    cfun->params_count = params_count;
    cfun->has_retval = has_retval;
    cfun->cfun_name = cfun_name;
  }
  return cfun;
UEND

UDEFUN(UFNAME vgc_call_new,
       UARGS (vcontext* ctx,
              unsigned char call_type,
              usize_t params_count,
              usize_t base),
       URET vgc_call*)
UDECLARE
  vgc_heap* heap;
  vgc_call* call;
  vgc_subr* subr;
UBEGIN
  heap = ctx->heap;
  call = vgc_heap_obj_new(heap,
                          vgc_call,
                          vgc_obj_type_call,
                          vgc_heap_area_active);
  if(call){
    call->call_type = call_type;
    call->params_count = params_count;
    call->base = base;
    call->pc = 0;
    if(call_type == vgc_call_type_cfun){
      vcontext_obj_slot_set(ctx,call,cfun);
      vgc_obj_null_set(call,subr);
    }else{
      vcontext_obj_pop(ctx,subr,vgc_subr);
      vcontext_obj_push(ctx,subr);
      vcontext_obj_slot_set(ctx,call,subr);
      vgc_obj_null_set(call,cfun);
    }
    vcontext_obj_slot_set(ctx,call,caller);
  }
  return call;
UEND

UDEFUN(UFNAME vgc_closure_new,
       UARGS (vgc_heap* heap,
              vgc_array* field,
              vgc_subr* subr),
       URET vgc_closure*)
UDECLARE
vgc_closure* closure;
UBEGIN
  closure = vgc_heap_obj_new(heap, vgc_closure,
                             vgc_obj_type_closure,
                             vgc_heap_area_static);
  if (closure) {
    vgc_obj_ref_set(closure,field,field);
    vgc_obj_ref_set(closure,subr,subr);
  }
  return closure;
UEND

static int ucall vobjtb_key_comp
(vsymbol* sym1,vsymbol* sym2)
{
  return ustring_comp(sym1->name, sym2->name);
}

static int get_insts_count(ulist_vps_cfgp* cfgs,vps_id id)
{
  ucursor cursor;
  int find_flag = 0;
  int insts_count = 0;

  cfgs->iterate(&cursor);
  while(1){
    vps_cfgp* cfgp = cfgs->next((uset_vps_cfgp*)cfgs,&cursor);
    vps_cfg* cfg; 
    vcfg_block* blk;
    if(!cfgp){
      break;
    }
    cfg = *cfgp;
    if(cfg->t != vcfgk_blk){
      uabort("vps_cfg not a block!");
    }
    blk = (vcfg_block*)cfg;
    if (!vps_id_comp(blk->id,id)) {
      find_flag = 1;
      break;
    }
    insts_count += blk->insts->len;
  }
  if (!find_flag) {
    uabort("can not find label!");
  }
  return insts_count;
}

UDEFUN(UFNAME insts_concat,
       UARGS (vgc_array* consts,
              ulist_vps_cfgp* cfgs,
              vcfg_block* blk,
              vmod* mod,
              ulist_vps_instp* insts),
       URET static int)
UDECLARE
  /*part insts*/
  ulist_vps_instp* pinsts;
  ucursor cursor;
UBEGIN
  pinsts = blk->insts;
  insts->iterate(&cursor);

  while(1){
    vps_instp* instp = insts->next((uset_vps_instp*)pinsts,&cursor);
    vps_inst* pinst;

    if (!instp) {
      break;
    }
    pinst = *instp;
#define DF(code,name,value,len,oct) \
    case code: \
      if (len > 1) { \
        goto l1; \
      }else { \
        goto l2; \
      } \
      break;
    switch (pinst->opc.opcode) {
      VBYTECODE
#undef DF
    }
  l1:
    switch (vps_inst_opek_get(pinst)) {
    case vinstk_imm:
      if(pinst->ope[0].data){
        vps_inst_imm_set(pinst,pinst->ope[0].data->idx);
      }
      ulist_vps_instp_append(insts,pinst);
      udebug0("inst imm");
      break;
    case vinstk_locdt:
      {
        vps_t* cfg = blk->parent;
        vcfg_graph* grp;
        vps_data* data;
        
        if (pinst->ope[0].data) {
          data = pinst->ope[0].data;
        } else {
          if (!cfg || cfg->t != vcfgk_grp) {
            uabort("vcfg_block have no parent!");
          }
          grp = (vcfg_graph*)cfg;
          data = vcfg_grp_dtget(grp, pinst->ope[0].id.name);
          if (!data) {
            vps_id_log(pinst->ope[0].id);
            uabort("local variable: not find");
          }
        }
        vps_inst_imm_set(pinst,data->idx);
        ulist_vps_instp_append(insts,pinst);
        udebug0("inst local data");
      }
      break;
    case vinstk_glodt:
      {
        vps_data* data = pinst->ope[0].data;
        vreloc reloc;

        reloc.ref_name = data->id.decoration;
        reloc.rel_idx = data->idx;
        reloc.rel_obj = consts;
        vmod_add_reloc(mod,reloc);
        vps_inst_imm_set(pinst,data->idx);
        ulist_vps_instp_append(insts,pinst);
        udebug0("inst global data");
      }
      break;
    case vinstk_code:
      {
        int insts_count;

        insts_count = get_insts_count(cfgs,pinst->ope[0].id);
        vps_inst_imm_set(pinst,insts_count);
        ulist_vps_instp_append(insts,pinst);
        udebug0("inst code");
      }
      break;
    case vinstk_non:
      {
        ulist_vps_instp_append(insts,pinst);
        udebug0("inst non");
      }
      break;
    default:
      break;
    }
    continue;
  l2:
    ulist_vps_instp_append(insts,pinst);
    udebug0("inst non operand");
  }
  return 0;
UEND

static void ldata_load(vgc_heap* heap,vgc_array* consts,vps_data* data)
{
  vslot slot;
  int top;

  slot = data2data(heap,data);
  top = vgc_array_push(consts,slot);
  if(top == -1){
    uabort("vcontext_load consts overflow!");
  }
  data->idx = top;
}

static vslot data2data(vgc_heap* heap,vps_data* data)
{
  vslot slot;

  if (data->dtk == vdtk_num) {
    vslot_num_set(slot,data->u.number);      
  } else if (data->dtk == vdtk_int) {
    vslot_int_set(slot,data->u.integer);
  } else if (data->dtk == vdtk_str) {
    vgc_string* vstr;
    ustring* ustr;
    
    ustr = data->u.string;
    vstr = vgc_string_new(heap,
                          ustr->len,
                          vgc_heap_area_static);
    if (!vstr) {
      uabort("new vgc_string error!");
    }
    vgc_ustr2vstr(vstr,ustr);
    vslot_ref_set(slot,vstr);
  } else if (data->dtk == vdtk_char) {
    vslot_char_set(slot,data->u.character);
  } else if (data->dtk == vdtk_bool) {
    vslot_bool_set(slot,data->u.boolean);
  } else if (data->dtk == vdtk_any) {
    vslot_null_set(slot);
  } else if (data->dtk == vdtk_arr) {
    vgc_array* array;
    uset_vps_datap* set;
    ucursor c;
    vps_data* el_data;
    vslot el_slot;
    int i;
    array = vgc_array_new(heap,
                          data->u.array->len,
                          vgc_heap_area_static);
    if (!array) {
      uabort("new vgc_array error!");
    }
    set = (uset_vps_datap*)data->u.array;
    set->iterate(&c);
    for (i = 0; ;i++) {
      unext_vps_datap next = set->next(set,&c);
      if (!next) {
        break;
      }
      el_data = unext_get(next);
      el_slot = data2data(heap,el_data);
      vgc_array_set(array,i,el_slot);
    }
    vslot_ref_set(slot,array);
  } else {
    uabort("vcontext_load unknow data type");
    vslot_null_set(slot);
  }
  if (vps_data_is_const(data)) {
    vslot_const_yes(slot);
  } else {
    vslot_const_no(slot);
  }
  return slot;
}

UDEFUN(UFNAME vps_cat2cat,
       UARGS (ulist_vps_instp* insts,vps_cat* cat),
       URET static vcat)
UDECLARE
  vcat c;
UBEGIN
  if (cat->begin)
    c.begin = vps_inst_to_str_pos(insts,cat->begin);
  else
    c.begin = 0;

  if (cat->end)
    c.end = vps_inst_to_str_pos(insts,cat->end);
  else
    c.end = 0;

  if(cat->handle)
    c.todo = vps_inst_to_str_pos(insts,cat->handle);
  else
    c.todo = 0;

  return c;
UEND

UDEFUN(UFNAME vcontext_graph_load,
       UARGS (vcontext* ctx,vmod* mod,vcfg_graph* grp),
       URET vgc_subr*)
UDECLARE
  vgc_heap* heap;
  vgc_array* consts;
  uallocator* allocator;
  ulist_vps_datap* imms;
  ulist_vps_cfgp* cfgs;
  ulist_vps_instp* insts;
  vgc_string* bytecode;
  vgc_subr* subr;
  vslot slot;
  ucursor cursor;
  int params_type;
  usize_t i;
UBEGIN
  if (grp->scope == VPS_SCOPE_UNKNOW) {
    uabort("graph scope unknow!");
  }
  heap = ctx->heap;
  consts = vgc_array_new(heap,
                         grp->imms->len,
                         vgc_heap_area_static);
  if (!consts) {
    uabort("subr consts new error!");
  }
  /* load code */
  cfgs = grp->cfgs;

  allocator = vcontext_alloc_get(ctx);
  insts = ulist_vps_instp_alloc(allocator);
  
  cfgs->iterate(&cursor);
  while (1) {
    vps_cfgp* cfgp = cfgs->next((uset_vps_cfgp*)cfgs,&cursor);
    vcfg_block* blk;
    if (!cfgp) {
      break;
    }
    if ((*cfgp)->t != vcfgk_blk) {
      uabort("vps_cfg not a block!");
    }
    blk = (vcfg_block*)(*cfgp);
    if (insts_concat(consts,cfgs,blk,mod,insts)) {
      uabort("insts concat error!");
    }
  }

  bytecode = vps_inst_to_str(heap,insts);
  if (!bytecode) {
    uabort("vps inst to str error!");
  }
  vcontext_obj_push(ctx,bytecode);
  vcontext_obj_push(ctx,consts);
  if (grp->rest == VPS_GRP_REST_YES) {
    params_type = vgc_subr_params_flex;
  } else {
    params_type = vgc_subr_params_fixed;
  }
  subr = vgc_subr_new(ctx,
                      grp->id.decoration,
                      grp->params_count,
                      grp->locals_count,
                      grp->cats->len,
                      params_type,
                      vgc_heap_area_static);
  if (!subr) {
    uabort("new subr error!");
  }
  /* catch table */
  grp->cats->iterate(&cursor);
  for (i = 0; ; i++) {
    vps_catp* catp = grp->cats->next((uset_vps_catp*)grp->cats,&cursor);
    vps_cat* cat;
    vcat ca;
    if (!catp) {
      break;
    }
    cat = *catp;
    ca = vps_cat2cat(insts,cat);
    vgc_subr_cat_set(subr,i,ca);
  }
  /* add to vm mod symbol table */
  if (grp->scope != VPS_SCOPE_ENTRY &&
      grp->scope != VPS_SCOPE_DECL) {
    if (!grp->id.decoration) {
      uabort("graph not a entry but has no name!");
    }
    slot = vgc_obj2ref2slot(ctx->heap,(vgc_obj*)subr,UTRUE);
    /*
     * add to local symbol table
     */
    vmod_lslot_put(heap,mod,grp->id.decoration,slot);
    /*
     * if scope is global then also add to global symbol table
     */
    if (grp->scope == VPS_SCOPE_GLOBAL) {
      vmod_gslot_put(heap,mod,grp->id.decoration,slot);
    }
  }
  allocator->clean(allocator);
  /* load imm data */
  imms = grp->imms;
  imms->iterate(&cursor);
  while(1){
    vps_datap* datap = imms->next((uset_vps_datap*)imms,&cursor);
    vps_data* data;
    if (!datap) {
      break;
    }
    data = *datap;
    ldata_load(heap,consts,data);
  }

  return subr;
UEND

UDEFUN(UFNAME vcontext_mod_log,
       UARGS (vcontext* ctx),
       URET vapi void vcall)
UDECLARE
  uhstb_vmod* mods;
  ucursor cursor;
UBEGIN
  mods = ctx->mods;
  ulog("vcontext_mod_log: symbol count %d",mods->count);
  mods->iterate(&cursor);
  while (1) {
    vmod* next = mods->next((uset_vmod*)mods,&cursor);
    if (!next) {
      break;
    }
    ulog("vcontext_mod_log:%s",next->name->value);
  }
UEND

UDEFUN(UFNAME vcontext_mod_gobjtb_log,
       UARGS (vcontext* ctx,ustring* name),
       URET vapi void vcall)
UDECLARE
  vmod* mod;
  ucursor cursor;
  uhstb_vsymbol* gobjtb;
UBEGIN
  mod = vcontext_mod_get(ctx,name,UNULL);
  if (!mod) {
    ulog("vcontext_mod_gobjtb_log: mod %s not exists",name->value);
    return;
  }
  gobjtb = mod->gobjtb;
  ulog("vcontext_mod_gobjtb_log: symbol count %d",gobjtb->count);
  gobjtb->iterate(&cursor);
  while (1) {
    vsymbol* next = gobjtb->next((uset_vsymbol*)gobjtb,&cursor);
    if (!next) {
      break;
    }
    ulog("vcontext_mod_gobjtb_log:%s",next->name->value);
  }
UEND

UDEFUN(UFNAME vcontext_mod_lobjtb_log,
       UARGS (vcontext* ctx,ustring* name),
       URET vapi void vcall)
UDECLARE
  vmod* mod;
  ucursor cursor;
  uhstb_vsymbol* lobjtb;
UBEGIN
  mod = vcontext_mod_get(ctx,name,UNULL);
  if (!mod) {
    ulog("vcontext_mod_lobjtb_log: mod %s not exists",name->value);
    return;
  }
  lobjtb = mod->lobjtb;
  ulog("vcontext_mod_lobjtb_log: symbol count %d",lobjtb->count);
  lobjtb->iterate(&cursor);
  while (1) {
    vsymbol* next = lobjtb->next((uset_vsymbol*)lobjtb,&cursor);
    if (!next) {
      break;
    }
    ulog("vcontext_mod_lobjtb_log:%s",next->name->value);
  }
UEND

/* return 0 success,1 src_mod unload */
UDEFUN(UFNAME vcontext_mod2mod,
       UARGS (vcontext* ctx,vmod* dest_mod,vps_mod* src_mod),
       URET vapi int vcall)
UDECLARE
  ucursor cursor;
  uhstb_vps_datap* data = UNULL;
  uhstb_vcfg_graphp* code = UNULL;
  vgc_subr* init_subr;
UBEGIN

  if (vps_mod_isload(src_mod)) {
    vmod_loaded(dest_mod);
  } else {
    return 1;
  }

  vmod_uninit(dest_mod);

  data = src_mod->data;
  data->iterate(&cursor);
  while(1){
    vps_datap* dp;
    vps_data* d;
    dp = (data->next)((uset_vps_datap*)data,&cursor);
    if(!dp){
      break;
    }
    d = *dp;
    gdata_load(ctx,dest_mod,d);
  }

  code = src_mod->code;
  code->iterate(&cursor);
  while(1){
    vcfg_graphp* gp;
    vcfg_graph* g;
    gp = (code->next)((uset_vcfg_graphp*)code,&cursor);
    if(!gp){
      break;
    }
    g = *gp;
    udebug0("load vps graph name:");
    vps_id_log(g->id);
    vcontext_graph_load(ctx,dest_mod,g);
  }

  if (src_mod->entry) {
    udebug0("load vps entry graph");
    vps_id_log(src_mod->entry->id);
    /* i don't know why crash in this line
     * dest_mod->init = vcontext_graph_load(ctx,dest_mod,src_mod->entry);
    */
    init_subr = vcontext_graph_load(ctx,dest_mod,src_mod->entry);
    dest_mod->init = init_subr;
  }

  if (src_mod->mod_type == VPS_MOD_TYPE_ENTRY) {
    vm_m1* vm = ctx->vm;
    if (vm->mod_main &&
        vm->mod_main != dest_mod) {
      uabort("context mod main already exists!");
    }
    vm->mod_main = dest_mod;
  }

  return 0;
UEND

UDEFUN(UFNAME vcontext_mod_load,
       UARGS (vcontext* ctx,vps_mod* mod),
       URET vapi int vcall)
UDECLARE
  vmod* dest_mod;
  int retval;
UBEGIN
  dest_mod = vcontext_mod_add(ctx,mod->name,mod->path);
  retval = vcontext_mod2mod(ctx,dest_mod,mod);
  if (retval == 1) {
    uabort("mod2mod error unload!");
  } else if (retval) {
    uabort("mod2mod error!");
  }
  return 0;
UEND

static void vcontext_mod_init(vcontext* ctx)
{
  vm_m1* vm;
  vmod* mod_main;
  uhstb_vmod* mods;
  ucursor cursor;
  vslot slot;

  vm = ctx->vm;
  mod_main = vm->mod_main;
  mods = ctx->mods;
  mods->iterate(&cursor);
  while (1) {
    vmod* next = mods->next((uset_vmod*)mods,&cursor);
    if (!next) {
      break;
    }
    if (next->init &&
        !vmod_isinit(next) &&
        mod_main != next) {
      vslot_ref_set(slot,next->init);
      vcontext_stack_push(ctx,slot);
      vcontext_execute(ctx);
      vmod_inited(next);
    }
  }
  if (mod_main && !vmod_isinit(mod_main)) {
    vslot_ref_set(slot,mod_main->init);
    vcontext_stack_push(ctx,slot);
    vcontext_execute(ctx);
    vmod_inited(mod_main);
  }
}

UDEFUN(UFNAME vcontext_vps_load,
       UARGS (vcontext* ctx,vps_cntr* vps),
       URET vapi int vcall)
UDECLARE
  ucursor cursor;
  uhstb_vps_modp* mods;
UBEGIN
  mods = vps->mods;
  mods->iterate(&cursor);
  while (1) {
    vps_modp* next = mods->next((uset_vps_modp*)mods,&cursor);
    vps_mod* mod;
    if (!next) {
      break;
    }
    mod = *next;
    udebug1("load vps mod:%s",mod->name->value);
    vcontext_mod_load(ctx,mod);
  }
  vps_cntr_clean(vps);
  vcontext_relocate(ctx);
  vcontext_mod_init(ctx);
  return 0;
UEND

static void gdata_load(vcontext* ctx,vmod* mod,vps_data* data)
{
  vslot slot;
  if (vps_data_scope_of(data, VPS_SCOPE_DECL)) {
    return;
  }
  slot = data2data(ctx->heap,data);
  slot = vslot2ref2slot(ctx->heap,slot);
  vmod_lslot_put(ctx->heap,mod,data->id.decoration,slot);
  if (vps_data_scope_of(data, VPS_SCOPE_GLOBAL)) {
    vmod_gslot_put(ctx->heap,mod,data->id.decoration,slot);
  }
}

UDEFUN(UFNAME vmod_symbol_get,
       UARGS (vcontext* ctx,vmod* mod,ustring* name),
       URET vapi vsymbol* vcall)
UDECLARE
  vsymbol symbol_in;
  vsymbol* symbol_out;
  uhstb_vmod* mods;
  ucursor cursor;
UBEGIN
  symbol_in.name = name;
  if (mod) {
    uhstb_vsymbol_get(mod->lobjtb,
                      name->hash_code,
                      &symbol_in,
                      &symbol_out,
                      vobjtb_key_comp);
    if (symbol_out) {
      return symbol_out;
    }
  }
  mods = ctx->mods;
  mods->iterate(&cursor);
  while (1) {
    vmod* next = mods->next((uset_vmod*)mods,&cursor);
    if (!next) {
      break;
    }
    if (!vmod_isload(next) && ctx->loader) {
      ctx->loader->load(ctx->loader,next);
    }
    uhstb_vsymbol_get(next->gobjtb,
                      name->hash_code,
                      &symbol_in,
                      &symbol_out,
                      vobjtb_key_comp);
    if (symbol_out) {
      break;
    }
  }
  return symbol_out;
UEND

UDEFUN(UFNAME vmod_relocate,
       UARGS (vcontext* ctx,vmod* mod),
       URET void)
UDECLARE
  ulist_vreloc* rells;
  ucursor cursor;
UBEGIN
  rells = mod->rells;
  rells->iterate(&cursor);
  while (1) {
    vsymbol* symbol;
    vreloc* reloc;
    reloc = rells->next((uset_vreloc*)rells,&cursor);
    if(!reloc){
      break;
    }
    symbol = vmod_symbol_get(ctx,mod,reloc->ref_name);
    if(!symbol){
      uabort("relocate global symbol:%s not find!",
             reloc->ref_name->value);
    }
    vgc_array_set(reloc->rel_obj,reloc->rel_idx,symbol->slot);
  }
UEND

UDEFUN(UFNAME vcontext_relocate,
       UARGS (vcontext* ctx),
       URET void)
UDECLARE
  uhstb_vmod* mods;
  ucursor cursor;
UBEGIN
  mods = ctx->mods;
  mods->iterate(&cursor);
  while(1){
    vmod* mod = mods->next((uset_vmod*)mods,&cursor);
    if(!mod){
      break;
    }
    if (vmod_isload(mod)) {
      vmod_relocate(ctx,mod);
    } else if (ctx->loader) {
      ctx->loader->load(ctx->loader,mod);
      vmod_relocate(ctx,mod);
    }
  }
UEND

UDEFUN(UFNAME vcontext_params_get,
       UARGS (vcontext* ctx,int index),
       URET vapi vslot vcall)
UDECLARE
  vgc_call* calling;
  vgc_cfun* cfun;
  int count;
  usize_t base;
  vslot slot;
  usize_t real_index;
UBEGIN
  calling = vgc_obj_ref_get(ctx,calling,vgc_call);
  if(calling->call_type != vgc_call_type_cfun){
    uabort("vcontext_params_get:current call not a cfun!");
  }
  cfun = vgc_obj_ref_get(calling,cfun,vgc_cfun);
  count = cfun->params_count;
  base = calling->base;
  real_index = base - count + index;

  if(index < 0 || index >= count)
    uabort("vm:local varable error!");
  slot = vcontext_stack_get(ctx,real_index);
  return slot;
UEND

static int ucall vcontext_mod_comp(vmod* mod1,vmod* mod2){
  return ustring_comp(mod1->name,mod2->name);
}

static void vmod_init(vmod* mod,ustring* name,ustring* path)
{
  ulist_vreloc* rells;
  uhstb_vsymbol* gobjtb;
  uhstb_vsymbol* lobjtb;
  
  rells = ulist_vreloc_new();
  if (!rells) {
    uabort("vmod rells new error!");
  }
  
  gobjtb = uhstb_vsymbol_new(-1);
  if (!gobjtb) {
    uabort("vmod gobjtb new error!");
  }

  lobjtb = uhstb_vsymbol_new(-1);
  if (!lobjtb) {
    uabort("vmod lobjtb new error!");
  }

  mod->rells = rells;
  mod->gobjtb = gobjtb;
  mod->lobjtb = lobjtb;
  mod->init = UNULL;
  mod->name = name;
  mod->path = path;
  mod->status = 0;
}

UDEFUN(UFNAME vcontext_mod_put,
       UARGS (vmod* mod),
       URET static vmod ucall)
UDECLARE

UBEGIN
  vmod_init(mod,mod->name,mod->path);
  return *mod;
UEND

UDEFUN(UFNAME vcontext_mod_add,
       UARGS (vcontext* ctx,
              ustring* name,
              ustring* path),
       URET vapi vmod* vcall)
UDECLARE
  vm_m1* vm;
  vmod in_mod;
  vmod* out_mod;
  int retval;
UBEGIN
  vm = ctx->vm;
  /*vmod_init(&in_mod,name,path);*/
  in_mod.name = name;
  in_mod.path = path;
  retval = uhstb_vmod_put(ctx->mods,
                          name->hash_code,
                          &in_mod,
                          &out_mod,
                          vcontext_mod_put,
                          vcontext_mod_comp);
  if (retval == 1) {
    if (vm->mod_fuse == VMOD_FUSE_NO) {
      uabort("vcontext add mod exists!");
    }
  } else if (retval == -1) {
    uabort("vcontext add mod error!");
  }
  if (vm->mod_fuse == VMOD_FUSE_YES) {
    umap_replace_enable(out_mod->gobjtb);
    umap_replace_enable(out_mod->lobjtb);
  }
  return out_mod;
UEND

UDEFUN(UFNAME vcontext_mod_get,
       UARGS (vcontext* ctx,
              ustring* name,
              ustring* path),
       URET vapi vmod* vcall)
UDECLARE
  vmod in_mod;
  vmod* out_mod;
UBEGIN
  in_mod.name = name;
  in_mod.path = path;
  uhstb_vmod_get(ctx->mods,
                 name->hash_code,
                 &in_mod,
                 &out_mod,
                 vcontext_mod_comp);
  return out_mod;
UEND

UDEFUN(UFNAME vmod_add_reloc,
       UARGS (vmod* mod,vreloc reloc),
       URET void)
UDECLARE
UBEGIN
  if (ulist_vreloc_append(mod->rells,reloc)){
    uabort("vmod add reloc error!");
  }
UEND

UDEFUN(UFNAME vmod_gobj_put,
       UARGS (vgc_heap* heap,
              vmod* mod,
              ustring* name,
              vgc_obj* obj),
       URET vapi vsymbol* vcall)
UDECLARE
  vslot slot;
  vsymbol* symbol;
UBEGIN
  vslot_ref_set(slot,obj);
  symbol = vmod_gslot_put(heap,mod,name,slot);
  
  return symbol;
UEND

UDEFUN(UFNAME vmod_lobj_put,
       UARGS (vgc_heap* heap,
              vmod* mod,
              ustring* name,
              vgc_obj* obj),
       URET vapi vsymbol* vcall)
UDECLARE
  vslot slot;
  vsymbol* symbol;
UBEGIN
  vslot_ref_set(slot,obj);
  symbol = vmod_lslot_put(heap,mod,name,slot);
  
  return symbol;
UEND

UDEFUN(UFNAME vmod_gslot_put,
       UARGS (vgc_heap* heap,
              vmod* mod,
              ustring* name,
              vslot slot),
       URET vapi vsymbol* vcall)
UDECLARE
  vsymbol symbol;
  vsymbol* new_symbol;
  int retval;
UBEGIN
  symbol.name = name;
  symbol.slot = slot;
  retval = uhstb_vsymbol_put(mod->gobjtb,
                             name->hash_code,
                             &symbol,
                             &new_symbol,
                             UNULL,
                             vobjtb_key_comp);
  if(retval == -1){
    uabort("vmod_gslot_put error!");
  }
  return new_symbol; 
UEND

UDEFUN(UFNAME vmod_lslot_put,
       UARGS (vgc_heap* heap,
              vmod* mod,
              ustring* name,
              vslot slot),
       URET vapi vsymbol* vcall)
UDECLARE
  vsymbol symbol;
  vsymbol* new_symbol;
  int retval;
UBEGIN
  symbol.name = name;
  symbol.slot = slot;
  retval = uhstb_vsymbol_put(mod->lobjtb,
                             name->hash_code,
                             &symbol,
                             &new_symbol,
                             UNULL,
                             vobjtb_key_comp);
  if(retval == -1){
    uabort("vmod_lslot_put error!");
  }
  return new_symbol;
UEND

UDEFUN(UFNAME vcontext_stack_get,
       UARGS (vcontext* ctx,usize_t index),
       URET vslot)
UDECLARE
  vslot slot;
UBEGIN
  if(ustack_vslot_get(ctx->stack,index,&slot)){
    uabort("vcontext_stack:index over of bound!");
  }
  return slot;
UEND

UDEFUN(UFNAME vcontext_stack_set,
       UARGS (vcontext* ctx,usize_t index,vslot slot),
       URET void)
UDECLARE
UBEGIN
  if(ustack_vslot_set(ctx->stack,index,slot)){
    uabort("vcontext_stack:index over of bound!");
  }
UEND

UDEFUN(UFNAME vcontext_stack_top_get,
       UARGS (vcontext* ctx),
       URET usize_t)
UDECLARE
UBEGIN
  return ustack_vslot_top_get(ctx->stack);
UEND

UDEFUN(UFNAME vcontext_stack_top_set,
       UARGS (vcontext* ctx,usize_t index),
       URET void)
UDECLARE
UBEGIN
  if(ustack_vslot_top_set(ctx->stack,index)){
    uabort("vcontext_stack:index over of bound!");
  }
UEND

UDEFUN(UFNAME vcontext_stack_push,
       UARGS (vcontext* ctx, vslot slot),
       URET vapi void vcall)
UDECLARE
UBEGIN
    if (ustack_vslot_push(ctx->stack, slot)) {
        uabort("vgc_heap_stack: overflow!");
    }
UEND

UDEFUN(UFNAME vcontext_stack_pop,
       UARGS (vcontext* ctx, vslot* slotp),
       URET vapi void vcall)
UDECLARE
UBEGIN
    if (ustack_vslot_pop(ctx->stack, slotp)) {
        uabort("vgc_heap_stack: empty!");
    }
UEND

UDEFUN(UFNAME vcontext_stack_pushv,
       UARGS (vcontext* ctx),
       URET vapi void vcall)
UDECLARE
UBEGIN
    if (ustack_vslot_pushv(ctx->stack)) {
        uabort("vgc_heap_stack: overflow!");
    }
UEND

/*
 * find catch table goto handle exception
*/
UDEFUN(UFNAME vcontext_exception_catch,
       UARGS(vcontext * ctx),
       URET static void)
UDECLARE
  ustack_vslot* stack;
  vslot slot;
  vgc_obj* obj;
  vgc_call* call;
  int last_call_pos;
  int i;
  usize_t j;
UBEGIN
  stack = ctx->stack;
  last_call_pos = stack->block_pos;
  for (i = stack->block_pos - 1; i > -1; i--) {
    slot = stack->curr_block->ptr[i];
    if (vslot_is_ref(slot)) {
      if (!vslot_is_null(slot)) {
        obj = vslot_ref_get(slot,vgc_obj);
        if (vgc_obj_typeof(obj, vgc_obj_type_call)) {
          call = (vgc_call*)obj;
          if (call->call_type == vgc_call_type_subr) {
            vgc_subr* subr = vgc_obj_ref_get(call,subr,vgc_subr);
            for (j = 0; j < subr->catb_count; j++) {
              vcat cat = subr->catb[j];
              ulog("begin %d,end %d,handle %d,pc %d",
                   cat.begin,cat.end,cat.todo,call->pc);
              if (call->pc >= cat.begin && call->pc <= cat.end) {
                stack->block_pos = last_call_pos;
                vgc_obj_ref_set(ctx,calling,call);
                vcontext_obj_slot_get(ctx,ctx,eobj);
                call->pc = cat.todo;
                ustdc_longjmp(ctx->envbuf,1);
              }
            }
          }
          last_call_pos = i;
        }
      }
    }
  }
UEND

UDEFUN(UFNAME vcontext_exit,
       UARGS(vcontext * ctx, char* msg, ...),
       URET vapi void vcall)
UDECLARE
  ustack_vslot* stack;
  ustdc_va_list ap1;
  ustdc_va_list ap2;
  vslot slot;
  vgc_obj* obj;
  vgc_call* call;
  int i;
UBEGIN
  stack = ctx->stack;

  ustdc_va_start(ap1, msg);
  ustdc_vfprintf(ustdc_stderr, msg, ap1);
  ustdc_fprintf(ustdc_stderr, "\n");
  ustdc_va_end(ap1);

  ustdc_va_start(ap2, msg);
  uverror(msg, ap2);
  ustdc_va_end(ap2);
  
  for (i = stack->block_pos - 1;i > -1;i--) {
    slot = stack->curr_block->ptr[i];
    if (vslot_is_ref(slot)) {
      if (!vslot_is_null(slot)) {
        obj = vslot_ref_get(slot,vgc_obj);
        if (vgc_obj_typeof(obj, vgc_obj_type_call)) {
          char* str;
          call = (vgc_call*)obj;
          if (call->call_type == vgc_call_type_cfun) {
            vgc_cfun* cfun = vgc_obj_ref_get(call,cfun,vgc_cfun);
            char* name = UNULL;
            if (cfun->cfun_name) {
                name = cfun->cfun_name->value;
            }
            str = "stack %d trace call cfun %s@%p";
            ustdc_fprintf(ustdc_stderr, str, i, name, obj);
            ustdc_fprintf(ustdc_stderr, "\n");
            ulog(str, i, name, obj);
          } else if (call->call_type == vgc_call_type_subr) {
            vgc_subr* subr = vgc_obj_ref_get(call,subr,vgc_subr);
            char* name = UNULL;
            if (subr->subr_name) {
                name = subr->subr_name->value;
            }
            str = "stack %d trace call subr %s@%p";
            ustdc_fprintf(ustdc_stderr, str, i, name, obj);
            ustdc_fprintf(ustdc_stderr, "\n");
            ulog(str, i, name, obj);
          } else {
            str = "stack %d trace call unknow@%p";
            ustdc_fprintf(ustdc_stderr, str, i ,obj);
            ustdc_fprintf(ustdc_stderr, "\n");
            ulog(str, i, obj);
          }
        }
      }
    }
  }
  ustdc_exit(0);
UEND

/*
 * throw exception
 */
UDEFUN(UFNAME vcontext_exception_throw,
       UARGS (vcontext* ctx,vslot eobj),
       URET vapi void vcall)
UDECLARE
  char* msg = "";
UBEGIN
  msg = vslot_to_str(eobj);
  vgc_slot_set(ctx,eobj,eobj);
  vcontext_exception_catch(ctx);
  vcontext_exit(ctx,msg);
UEND

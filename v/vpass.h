#ifndef _VPASS_H_
#define _VPASS_H_

#include "ulist_tpl.h"
#include "ugraph.h"
#include "vgc_obj.h"
#include "vreader.h"

struct _vps_t;
typedef struct _vps_t vps_t,* vpsp;
struct _vps_cntr;
typedef struct _vps_cntr vps_cntr;
struct _vps_data;
typedef struct _vps_data vps_data,*vps_datap;
struct _vps_id;
typedef struct _vps_id vps_id;
struct _vps_opc;
typedef struct _vps_opc vps_opc;
struct _vps_ope;
typedef struct _vps_ope vps_ope;
struct _vps_inst;
typedef struct _vps_inst vps_inst,*vps_instp;
struct _vps_cfg;
typedef struct _vps_cfg vps_cfg,*vps_cfgp;
struct _vcfg_block;
typedef struct _vcfg_block vcfg_block;
struct _vps_cat;
typedef struct _vps_cat vps_cat,*vps_catp;
struct _vcfg_graph;
typedef struct _vcfg_graph vcfg_graph,*vcfg_graphp;
struct _vps_mod;
typedef struct _vps_mod vps_mod,*vps_modp;
struct _vps_jzp_req;
typedef struct _vps_jzp_req vps_jzp_req;

#ifndef _UMAP_TPL_VPSP_
umap_decl_tpl(vpsp);
#endif
#ifndef _UMAP_TPL_VPS_DATAP_
umap_decl_tpl(vps_datap);
#endif
#ifndef _UMAP_TPL_VPS_INSTP_
umap_decl_tpl(vps_instp);
#endif
#ifndef _UMAP_TPL_VPS_CFGP_
umap_decl_tpl(vps_cfgp);
#endif
#ifndef _UMAP_TPL_VPS_CATP_
umap_decl_tpl(vps_catp);
#endif
#ifndef _UMAP_TPL_VPS_MODP_
umap_decl_tpl(vps_modp);
#endif
#ifndef _UMAP_TPL_VCFG_GRAPHP_
umap_decl_tpl(vcfg_graphp);
#endif
#ifndef _ULIST_TPL_VPSP_
ulist_decl_tpl(v,vpsp);
#endif
#ifndef _UHSTB_TPL_VPS_DATAP_
uhstb_decl_tpl(v,vps_datap);
#endif
#ifndef _ULIST_TPL_VPS_INSTP_
ulist_decl_tpl(v,vps_instp);
#endif
#ifndef _ULIST_TPL_VPS_DATAP_
ulist_decl_tpl(v,vps_datap);
#endif
#ifndef _ULIST_TPL_VPS_CFGP_
ulist_decl_tpl(v,vps_cfgp);
#endif
#ifndef _ULIST_TPL_VPS_CATP_
ulist_decl_tpl(v,vps_catp);
#endif
#ifndef _UHSTB_TPL_VPS_MODP_
uhstb_decl_tpl(v,vps_modp);
#endif
#ifndef _UHSTB_TPL_VCFG_GRAPHP_
uhstb_decl_tpl(v,vcfg_graphp);
#endif

#define VPSHEADER \
  int t

enum vpsk
  {
    vpsk_dt,
    vpsk_mod,
    vpsk_inst,
    vcfgk_blk,
    vcfgk_grp,
  };

enum vdtk
  {
    vdtk_arr,
    vdtk_num,
    vdtk_int,
    vdtk_char,
    vdtk_str,
    vdtk_any,
    vdtk_code,
    vdtk_bool,
  };

struct _vps_t
{
  VPSHEADER;
};

struct _vps_id
{
  ustring* name;
  ustring* decoration;
  long num;
};

UDECLFUN(UFNAME vps_id_get,
         UARGS (vps_cntr* vps,ustring* name),
         URET vapi vps_id vcall);

UDECLFUN(UFNAME vps_id_comp,
         UARGS (vps_id id1,vps_id id2),
         URET int);

#define vps_id_log(id)                                  \
  do {                                                  \
    if (id.name) {                                      \
      udebug2("id=%s:%d",id.name->value,id.num);        \
    } else {                                            \
      udebug1("id=:%d",id.num);                         \
    }                                                   \
  } while(0)

#define VPSIDHEADER \
  VPSHEADER; \
  vps_id id

#define VPS_SCOPE_UNKNOW 0
#define VPS_SCOPE_GLOBAL 1
#define VPS_SCOPE_STATIC 2
#define VPS_SCOPE_LOCAL 3
#define VPS_SCOPE_ENTRY 4
#define VPS_SCOPE_DECL 5
#define VPS_SCOPE_FIELD 6

#define VPS_DATA_CONST 1
#define VPS_DATA_MDF 0

struct _vps_data
{
  VPSIDHEADER;
  unsigned char dtk;
  unsigned char scope;
  unsigned char mdf;
  int idx;
  union {
    double number;
    int integer;
    int character;
    int boolean;
    ustring* string;
    ulist_vps_datap* array;
    struct _vps_cfg* code;
  } u;
};

#define vps_data_scope_set(DATA,SCOPE) \
  ((DATA)->scope = SCOPE)
  
#define vps_data_scope_get(DATA) \
  ((DATA)->scope)
  
#define vps_data_scope_of(DATA,SCOPE) \
  ((DATA)->scope == (SCOPE))

#define vps_data_is_const(DATA) \
  ((DATA)->mdf == VPS_DATA_CONST)

#define vps_data_const_set(DATA) \
  ((DATA)->mdf = VPS_DATA_CONST)

#define vps_data_mdf_set(DATA) \
  ((DATA)->mdf = VPS_DATA_MDF)

/*
 * instruction opcode kind
 */
enum viopck
  {
    viopck_branch,
    viopck_entry,
    viopck_non,
    viopck_ret,
  };

/*
 * instruction operand kind
 */
enum viopek
  {
    vinstk_imm,
    vinstk_glodt,
    vinstk_locdt,
    vinstk_code,
    vinstk_non,
  };

struct _vps_opc
{
  unsigned char iopck;
  usize_t opcode;
};

struct _vps_ope
{
  unsigned char iopek;
  usize_t operand;
  vps_id id;
  vps_data* data;
};

struct _vps_inst
{
  VPSHEADER;
  vps_opc opc;
  vps_ope ope[1];
};

void vps_inst_log(vps_inst* inst);

#define vps_inst_opck_get(inst)                 \
  (inst)->opc.iopck

#define vps_inst_opck_set(inst,k)               \
  (inst)->opc.iopck = k

#define vps_inst_opek_get(inst)                 \
  (inst)->ope[0].iopek

#define vps_inst_opek_set(inst,k)               \
  (inst)->ope[0].iopek = k

#define vps_inst_isret(inst)                    \
  ((inst)->opc.iopck == viopck_ret)

#define vps_inst_imm_set(inst,imm)              \
  (inst)->ope[0].operand = imm

#define vps_inst_imm_get(inst)                  \
  (inst)->ope[0].operand

#define vps_bool_true (1)
#define vps_bool_false (0)

#define VCFGHEADER                              \
  VPSIDHEADER;                                  \
  struct _vps_t* parent

struct _vps_cfg{
  VCFGHEADER;
};

struct _vcfg_block
{
  VCFGHEADER;
  ugnode node;
  ulist_vps_instp* insts;
};

struct _vps_cat{
  vps_inst* begin;
  vps_inst* end;
  vps_inst* handle;
};

UDECLFUN(UFNAME vps_cat_new,
         UARGS (vps_cntr* vps,
                vps_inst* begin,
                vps_inst* end,
                vps_inst* handle),
         URET vapi vps_cat* vcall);

#define VPS_GRP_REST_YES 1
#define VPS_GRP_REST_NO 0

struct _vcfg_graph
{
  VCFGHEADER;
  uhstb_vps_datap* locals;
  ulist_vps_instp* insts;
  ulist_vps_datap* imms;
  ulist_vps_cfgp* cfgs;
  ulist_vps_catp* cats;
  vps_cfg* entry;
  int params_count;
  int locals_count;
  int locals_index;
  unsigned char scope;
  unsigned char rest;
};

#define VPS_MOD_STATUS_LOADED 1
#define VPS_MOD_STATUS_UNLOAD 0

#define VPS_MOD_TYPE_ENTRY 1
#define VPS_MOD_TYPE_NORMAL 0

struct _vps_mod
{
  VPSHEADER;
  struct _vps_cntr* vps;
  uhstb_vps_datap* data;
  uhstb_vcfg_graphp* code;
  vcfg_graph* entry;
  ustring* name;
  ustring* path;
  int status;
  int mod_type;
};

enum vfe_base_type{
  vfe_token = 0,
  vfe_ast,
  vfe_closure,
  vfe_vps,
};

typedef ustring* 
  (*vps_sym_ft)(
    ustring_table* symtb,
    ubuffer* buff,
    void* ptr,
    enum vfe_base_type req_base_type,
    int req_sub_type,
    enum vfe_base_type res_base_type,
    int res_sub_type);

#define VPS_CHECK_SEVERE 1
#define VPS_CHECK_TOLERANT 2

struct _vps_cntr
{
  umem_pool mp;
  uhstb_vps_modp* mods;
  vps_mod* entry;
  vps_sym_ft symgen;
  int seqnum;
  int check_mode;
};

#define vps_cntr_check_mode(cntr,mode) \
  (cntr)->check_mode = mode

#define vps_cntr_check_is_severe(cntr) \
  ((cntr)->check_mode == VPS_CHECK_SEVERE)

#define vps_cntr_check_is_tolerant(cntr) \
  ((cntr)->check_mode == VPS_CHECK_TOLERANT)

#define vps_cntr_alloc_get(vps) \
  &(vps)->mp.allocator

struct _vps_jzp_req
{
  VAST_ATTR_REQ_HEADER;
  vps_cntr* vps;
  vps_mod* top;
  vps_cfg* parent;
};

#define VATTR_CONTEXT_FILE(parent)                      \
  (parent->t == vcfgk_grp &&                            \
   ((vcfg_graph*)parent)->scope == VPS_SCOPE_ENTRY)

#define VATTR_CONTEXT_SUBR(parent)                      \
  (parent->t == vcfgk_grp &&                            \
   ((vcfg_graph*)parent)->scope != VPS_SCOPE_ENTRY)

UDECLFUN(UFNAME vfile2vps,
         UARGS (vreader* reader,
                ustring* name,
                ustring* path,
                vps_cntr* vps),
         URET vapi vps_mod* vcall);

UDECLFUN(UFNAME vps_inst_new,
         UARGS (vps_cntr* vps,
                int iopck,
                int iopek,
                usize_t opcode),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_inop,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_iloadimm,
         UARGS (vps_cntr* vps,
                int imm),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_iloaddt,
         UARGS (vps_cntr* vps,
                ustring* name),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_iloadv,
         UARGS (vps_cntr* vps,
                vps_data* data),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ildaimm,
         UARGS (vps_cntr* vps,
                int imm),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ildadt,
         UARGS (vps_cntr* vps,
                ustring* name),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ildav,
         UARGS (vps_cntr* vps,
                vps_data* data),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ildas,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_istoreimm,
         UARGS (vps_cntr* vps,
                int imm),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_istoredt,
         UARGS (vps_cntr* vps,
                ustring* name),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_istorev,
         UARGS (vps_cntr* vps,
                vps_data* data),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_istas,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_istaimm,
         UARGS (vps_cntr* vps,
                int imm),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_istadt,
         UARGS (vps_cntr* vps,
                ustring* name),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_istav,
         UARGS (vps_cntr* vps,
                vps_data* data),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ipushint,
         UARGS (vps_cntr* vps,
                vcfg_graph* grp,
                ustring* name,
                int imm),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ipushchar,
         UARGS (vps_cntr* vps,
                vcfg_graph* grp,
                int imm),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ipushnum,
         UARGS (vps_cntr* vps,
                vcfg_graph* grp,
                ustring* name,
                double dnum),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ipushdt,
         UARGS (vps_cntr* vps,
                vcfg_graph* graph,
                ustring* name),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ipushstr,
         UARGS (vps_cntr* vps,
                vcfg_graph* graph,
                ustring* string),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ipushnil,
         UARGS (vps_cntr* vps,
                vcfg_graph* grp),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ipushbool,
         UARGS (vps_cntr* vps,
                vcfg_graph* grp,
                int bool),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_itop,
         UARGS (vps_cntr* vps,
                int imm),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ipopdt,
         UARGS (vps_cntr* vps,
                vcfg_graph* graph,
                ustring* name),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ipopv,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_iadd,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_isub,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_imul,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_idiv,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ijmpiimm,
         UARGS (vps_cntr* vps,
                int imm),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ijmpilb,
         UARGS (vps_cntr* vps,
                vps_id id),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ijmpimm,
         UARGS (vps_cntr* vps,
                int imm),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ijmplb,
         UARGS (vps_cntr* vps,
                vps_id id),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ialen,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ieq,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_igt,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ilt,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_iand,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ior,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_inot,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_icall,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_icallx,
         UARGS (vps_cntr* vps,int imm),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ireturn,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_iretvoid,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_irefimm,
         UARGS (vps_cntr* vps,
                int imm),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_irefdt,
         UARGS (vps_cntr* vps,
                ustring* name),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_irefv,
         UARGS (vps_cntr* vps,
                vps_data* data),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_irefgdt,
         UARGS (vps_cntr* vps,
                vcfg_graph* graph,
                ustring* name),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_irefs,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_isetimm,
         UARGS (vps_cntr* vps,
                int imm),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_isetdt,
         UARGS (vps_cntr* vps,
                ustring* name),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_isetv,
         UARGS (vps_cntr* vps,
                vps_data* data),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_isetgdt,
         UARGS (vps_cntr* vps,
                vcfg_graph* graph,
                ustring* name),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_isets,
         UARGS (vps_cntr* vps),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_ilabel,
         UARGS (vps_cntr* vps,
                vps_id id),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vps_num_new,
         UARGS (vps_cntr* vps,
                ustring* name,
                double num),
         URET vapi vps_data* vcall);

UDECLFUN(UFNAME vps_int_new,
         UARGS (vps_cntr* vps,
                ustring* name,
                int inte),
         URET vapi vps_data* vcall);

UDECLFUN(UFNAME vps_int_set,
         UARGS (vps_data* dt,
                int inte),
         URET vapi void vcall);

UDECLFUN(UFNAME vps_char_new,
         UARGS (vps_cntr* vps,
                int chara),
         URET vapi vps_data* vcall);

UDECLFUN(UFNAME vps_str_new,
         UARGS (vps_cntr* vps,
                ustring* name,
                ustring* string),
         URET vapi vps_data* vcall);

UDECLFUN(UFNAME vps_any_new,
         UARGS (vps_cntr* vps,
                ustring* name),
         URET vapi vps_data* vcall);

UDECLFUN(UFNAME vps_dtcd_new,
         UARGS (vps_cntr* vps,
                ustring* name,
                vps_cfg* code),
         URET vapi vps_data* vcall);

UDECLFUN(UFNAME vps_bool_new,
         UARGS (vps_cntr* vps,
                int bool),
         URET vapi vps_data* vcall);

UDECLFUN(UFNAME vps_array_new,
         UARGS (vps_cntr* vps,
                ustring* name),
         URET vapi vps_data* vcall);

UDECLFUN(UFNAME vps_array_apd,
         UARGS (vps_data* array,vps_data* data),
         URET vapi int vcall);

UDECLFUN(UFNAME vcfg_block_new,
         UARGS (vps_cntr* vps,
                vps_id id),
         URET vapi vcfg_block* vcall);

UDECLFUN(UFNAME vcfg_blk_apd,
         UARGS (vcfg_block* blk,
                vps_inst* inst),
         URET vapi void vcall);

UDECLFUN(UFNAME vcfg_blk_linst_get,
         UARGS (vcfg_block* blk),
         URET vps_inst*);

UDECLFUN(UFNAME vcfg_graph_new,
         UARGS (vps_cntr* vps,
                ustring* name),
         URET vapi vcfg_graph* vcall);

UDECLFUN(UFNAME vps_graph_cat_add_by_cat,
         UARGS (vps_cntr* vps,
                vcfg_graph* grp,
                vps_cat* cat),
         URET vapi void vcall);

UDECLFUN(UFNAME vps_graph_cat_add,
         UARGS (vps_cntr* vps,
                vcfg_graph* grp,
                vps_inst* begin,
                vps_inst* end,
                vps_inst* handle),
         URET vapi void vcall);

UDECLFUN(UFNAME vcfg_grp_inst_apd,
         UARGS (vcfg_graph* grp,
                vps_inst* inst),
         URET vapi void vcall);

UDECLFUN(UFNAME vcfg_grp_finst_get,
         UARGS (vcfg_graph* grp),
         URET vapi vps_inst* vcall);

UDECLFUN(UFNAME vcfg_grp_linst_get,
         UARGS (vcfg_graph* grp),
         URET vapi vps_inst* vcall);

#define vps_inst_cursor ulsnd_vps_instp

UDECLFUN(UFNAME vcfg_grp_finst_cursor_get,
         UARGS (vcfg_graph* grp),
         URET vapi vps_inst_cursor* vcall);

UDECLFUN(UFNAME vcfg_grp_linst_cursor_get,
         UARGS (vcfg_graph* grp),
         URET vapi vps_inst_cursor* vcall);

UDECLFUN(UFNAME vcfg_grp_inst_cursor_next,
         UARGS (vcfg_graph* grp,
                vps_inst_cursor* ic),
         URET vapi vps_inst_cursor* vcall);

UDECLFUN(UFNAME vps_inst_cursor_value,
         UARGS (vps_inst_cursor* ic),
         URET vapi vps_inst* vcall);

#define vcfg_grp_inst_count(grp)                \
  ((grp)->insts->len)

#define vcfg_grp_inst_isempty(grp)              \
  (vcfg_grp_inst_count(grp) == 0)

UDECLFUN(UFNAME vcfg_grp_cdapd,
         UARGS (vps_cntr* vps,vcfg_graph* grp,vps_cfg* cfg),
         URET vapi void vcall);

UDECLFUN(UFNAME vcfg_grp_cdget,
         UARGS (vps_cntr* vps,vcfg_graph* grp,vps_id id),
         URET vapi vps_data* vcall);

UDECLFUN(UFNAME vcfg_grp_build,
         UARGS (vps_cntr* vps,vcfg_graph* grp),
         URET vapi void vcall);

UDECLFUN(UFNAME vcfg_grp_connect,
         UARGS (vps_cntr* vps,vcfg_graph* grp),
         URET vapi void vcall);

UDECLFUN(UFNAME vcfg_grp_params_apd,
         UARGS (vcfg_graph* grp,
                vps_data* dt),
         URET vapi void vcall);

UDECLFUN(UFNAME vcfg_grp_locals_apd,
         UARGS (vcfg_graph* grp,
                vps_data* dt),
         URET vapi void vcall);

UDECLFUN(UFNAME vcfg_grp_dtget,
         UARGS (vcfg_graph* grp,
                ustring* name),
         URET vapi vps_data* vcall);

UDECLFUN(UFNAME vcfg_grp_conts_put,
         UARGS (vcfg_graph* grp,
                vps_data* data),
         URET vapi int vcall);

UDECLFUN(UFNAME vps_mod_new,
         UARGS (vps_cntr* vps,
                ustring* name,
                ustring* path),
         URET vapi vps_mod* vcall);

#define vps_mod_loaded(mod)                     \
  ((mod)->status = VPS_MOD_STATUS_LOADED)

#define vps_mod_isload(mod)                     \
  ((mod)->status == VPS_MOD_STATUS_LOADED)

#define vps_mod_entry_set(mod)                  \
  ((mod)->mod_type = VPS_MOD_TYPE_ENTRY)

#define vps_mod_normal_set(mod)                 \
  ((mod)->mod_type = VPS_MOD_TYPE_NORMAL)

UDECLFUN(UFNAME vps_mod_data_put,
         UARGS (vps_mod* mod,
                vps_data* data),
         URET vapi void vcall);

UDECLFUN(UFNAME vps_mod_code_put,
         UARGS (vps_mod* mod,
                 vcfg_graph* code),
         URET vapi void vcall);

UDECLFUN(UFNAME vps_mod_data_get,
         UARGS (vps_mod* mod,
                ustring* name),
         URET vapi vps_data* vcall);

UDECLFUN(UFNAME vps_mod_code_get,
         UARGS (vps_mod* mod,
                ustring* name),
         URET vapi vcfg_graph* vcall);

UDECLFUN(UFNAME vps_cntr_init,
         UARGS (vps_cntr* cntr),
         URET vapi void vcall);

UDECLFUN(UFNAME vps_cntr_load,
         UARGS (vps_cntr* vps,
                vps_mod* mod),
         URET vapi int vcall);

UDECLFUN(UFNAME vps_cntr_data_get,
         UARGS (vps_cntr* vps,ustring* name),
         URET vapi vps_data* vcall);

UDECLFUN(UFNAME vps_cntr_code_get,
         UARGS (vps_cntr* vps,ustring* name),
         URET vapi vcfg_graph* vcall);

UDECLFUN(UFNAME vps_cntr_clean,
         UARGS (vps_cntr* vps),
         URET vapi void vcall);

#endif

#ifndef _L5EVAL_H_
#define _L5EVAL_H_

#include "vclosure.h"
#include "l5macro.h"
#include "leval.h"

typedef struct _l5eval l5eval;

struct _l5eval{
  leval* base_eval;
  vclosure* top_closure;
};

enum l5kwk{
  #define DF(no,str) no,
  #include "l5kw.h"
  #undef DF
};

UDECLFUN(UFNAME l5startup,
         UARGS (char* self_path,
                int vm_gc_asz,
                int vm_gc_ssz,
                int vm_gc_rsz),
         URET l5api l5eval* l5call);

UDECLFUN(UFNAME l5eval_init,
         UARGS (l5eval* eval),
         URET l5api void l5call);
         
UDECLFUN(UFNAME l5eval_interactive_enable,
         UARGS (l5eval* eval),
         URET l5api void l5call);

UDECLFUN(UFNAME l5eval_cmd_arg_put,
         UARGS (l5eval* eval,char* arg),
         URET l5api int l5call);

UDECLFUN(UFNAME l5eval_src_path_set,
         UARGS (l5eval* eval,char* file_path),
         URET l5api int l5call);

UDECLFUN(UFNAME l5eval_src_load,
         UARGS (l5eval* eval),
         URET l5api int l5call);

UDECLFUN(UFNAME l5eval_cmd_load,
         UARGS (l5eval* eval),
         URET l5api int l5call);

UDECLFUN(UFNAME l5eval_lib_load_s,
         UARGS (l5eval* eval,
                char* mod_name,
                char* file_path,
                int conf_level),
         URET l5api int l5call);

UDECLFUN(UFNAME l5eval_lib_load_us,
         UARGS (l5eval* eval,
                ustring* mod_name,
                ustring* file_path,
                int conf_level),
         URET l5api int l5call);

#define l5eval_sys_conf_load(eval,file_path,s) \
  l5eval_conf_load_##s(eval,file_path,LEVAL_CONF_LEVEL_SYS);

#define l5eval_usr_conf_load(eval,file_path,s) \
  l5eval_conf_load_##s(eval,file_path,LEVAL_CONF_LEVEL_USR);

UDECLFUN(UFNAME l5eval_conf_load_s,
         UARGS (l5eval* eval,char* file_path,int conf_level),
         URET l5api int l5call);

UDECLFUN(UFNAME l5eval_conf_load_us,
         UARGS (l5eval* eval,ustring* file_path,int conf_level),
         URET l5api int l5call);

#define l5eval_local_name_get(ts,src_name) \
  leval_local_name_get(ts,src_name,'.',".l5");

#endif

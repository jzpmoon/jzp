bin = libl5$(suf_so)
obj = l5eval.obj

temp_attr_file = _l5temp.attr

libl_path = ..\..\l\

libu_path = ..\..\u\

libv_path = ..\..\v\

l_somk = makefile

CFLAGS = $(STDC98) $(WALL) $(WEXTRA) $(WNO_UNUSED_PARAMETER) $(DEBUG_MODE)

ATTR = l5base

$(bin):$(temp_attr_file) libl.lib $(obj)
	$(LINK) $(obj) $(L)$(libl_path) libl.lib $(L)$(libv_path) libv.lib $(L)$(libu_path) libu.lib $(OUT)$(bin) $(SHARED)
.c.obj:
	$(CC) $(C) $(COUT)$@ $< $(I) $(libl_path) $(I) $(libv_path) $(I) $(libu_path) $(CFLAGS) $(FPIC) $(D)_EXPORT_LIBL5_
$(temp_attr_file):
	$(libv_path)\attr.bat --attr=$(ATTR) --out=$(temp_attr_file) --callback=l5attr_file_concat_init
libl.lib:
	cd $(libl_path)
	nmake /f $(l_somk) DEBUG_MODE=$(DEBUG_MODE)
	cd $(currdir)
install:
	copy $(bin) $(prefix)
uninstall:
	del $(prefix)\$(bin)
clean:
	del $(bin) $(obj) $(temp_attr_file) *.lib *.exp

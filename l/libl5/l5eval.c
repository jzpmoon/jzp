#include "l5eval.h"
#include "l5.conf.attr"

static void vcall l5conf_init(vreader* reader)
{
  _vattr_file_init_l5_conf(reader);
}

UDEFUN(UFNAME leval5_closure_arg_call,
  UARGS(vclosure* closure, vast_eval_req* req, vast_obj* obj, unsigned char has_retval),
  URET static void)
UDECLARE
  vcfg_graph* grp;
  vps_inst* inst;
UBEGIN
  grp = ((vclosure_prog*)closure)->init;
  if (!vast_keywordp(obj)) {
    vast_keyword* kw;
    kw = (vast_keyword*)obj;
    if (kw->kw.kw_type == lkw_true) {
      inst = vps_ipushbool(req->vps, grp, vps_bool_true);
      vcfg_grp_inst_apd(grp, inst);
    }
    else if (kw->kw.kw_type == lkw_false) {
      inst = vps_ipushbool(req->vps, grp, vps_bool_false);
      vcfg_grp_inst_apd(grp, inst);
    }
    else if (kw->kw.kw_type == lkw_nil) {
      inst = vps_ipushnil(req->vps, grp);
      vcfg_grp_inst_apd(grp, inst);
    }
    else {
      uabort("keyword not define!");
    }
  } else {
    leval_closure_arg_call(closure, req, obj, has_retval);
  }

UEND

UDEFUN(UFNAME symcall_action,
       UARGS (vast_attr_req* req,
              vast_attr_res* res),
       URET static int vcall)
UDECLARE
  vast_obj* ast_obj;
  vast_eval_req* jreq;
  vps_cntr* vps;
  vclosure* closure;
  vcfg_graph* grp;
  vps_inst* inst;
  vast_obj* obj;
  vast_obj* next;
  vast_symbol* symbol;
  vclosure* func;
  vclosure_data* data;
  vcfg_graph* func_grp;
  int cac; /* calling args count */
  int dac; /* declare args count */
UBEGIN
  jreq = (vast_eval_req*)req;
  ast_obj = jreq->ast_obj;
  vps = jreq->vps;
  closure = jreq->closure;
  if (!vclosure_is_body(closure)) {
    uabort("closure haven't init!");
  }
  grp = vclosure_init_get(closure);
  obj = vast_car(ast_obj);
  symbol = (vast_symbol*)obj;
  func = vclosure_func_get(closure,symbol->name);
  if (!func) {
    data = vclosure_field_get(closure, symbol->name);
    if (vps_cntr_check_is_severe(vps) && !data) {
      uabort("calling function not finding:%s!", symbol->name->value);
    }
  } else {
    data = UNULL;
  }
  /* push params */
  cac = 0;
  next = vast_cdr(ast_obj);
  while (next) {
    obj = vast_car(next);
    leval5_closure_arg_call(closure, jreq, obj, VATTR_RETVAL_YES);
    next = vast_cdr(next);
    cac++;
  }

  if (data) {
    leval_data_receive_call(jreq, grp, data, symbol);
    /* call variable function */
    inst = vps_icallx(vps,cac);
    vcfg_grp_inst_apd(grp,inst);
  } else if (func) {
    func_grp = vclosure_init_get(func);
    /* push subr name */
    inst = vps_ipushdt(vps, grp, func_grp->id.decoration);
    vcfg_grp_inst_apd(grp, inst);
    /* whether rest args */
    if (vps_cntr_check_is_severe(vps) && func_grp->rest == VPS_GRP_REST_NO) {
      dac = vclosure_func_args_count(func);
      if (dac > cac) {
        uabort("calling function args too few!");
      }
      else if (dac < cac) {
        uabort("calling function args too much!");
      }
      /* call function */
      inst = vps_icall(vps);
      vcfg_grp_inst_apd(grp, inst);
    } else {
      /* call function */
      inst = vps_icallx(vps, cac);
      vcfg_grp_inst_apd(grp, inst);
    }
  } else if (vps_cntr_check_is_tolerant(vps)) {
    ustring* decoration = vps->symgen(jreq->reader->symtb, jreq->reader->buff,
				      symbol, vfe_ast, vastk_symbol,
				      vfe_ast, vastk_symbol);
    if (!decoration) {
      uabort("decoration error!");
    }
    inst = vps_ipushdt(vps, grp, decoration);
    vcfg_grp_inst_apd(grp, inst);
    inst = vps_icallx(vps, cac);
    vcfg_grp_inst_apd(grp, inst);
  }

  return 0;
UEND

static vast_attr lclosure_attr_symcall = { UNULL, UNULL, symcall_action, VATTR_RETVAL_YES };

UDEFUN(UFNAME l5eval_struct_rtti_size,
       UARGS (vast_eval_req* jreq,
              vast_obj* begin,
              vast_obj* end),
       URET static ustring*)
UDECLARE
  vreader* reader;
  ustring_table* symtb;
  ubuffer* buff;
  vast_obj* car;
  vast_symbol* sym;
  ustring* sym_name;
  int count = 0;
UBEGIN
  reader = jreq->reader;
  symtb = reader->symtb;
  buff = reader->buff;
  ubuffer_ready_write(buff);
  while (1) {
    int i = 0;
    car = vast_car(end);
    if (vast_symbolp(car)) {
      uabort("not a symbol!");
    }
    sym = (vast_symbol*)car;
    sym_name = sym->name;
    for (i = 0; i < sym_name->len; i++) {
      if (ubuffer_write_next(buff, sym_name->value[i]) == -1) {
        uabort("buffer overflow!");
      }
    }
    if (count == 0) {
      if (ubuffer_write_next(buff, '.') == -1) {
	      uabort("buffer overflow!");
	    }
    } else {
      if (ubuffer_write_int2ascii(buff, i) == -1) {
	      uabort("buffer overflow!");
	    }
    }
    if (begin == end) {
      break;
    }
    end = vast_pdr(end);
    count++;
  }
  if (ubuffer_write_next(buff, '\0') == -1) {
    uabort("buffer overflow!");
  }
  ubuffer_ready_read(buff);
  return ustring_table_put(symtb, buff->data, -1);
UEND

UDEFUN(UFNAME l5eval_struct_rtti_index,
       UARGS (vast_eval_req* jreq,
              vast_obj* begin,
              vast_obj* end),
       URET static ustring*)
UDECLARE
  vreader* reader;
  ustring_table* symtb;
  ubuffer* buff;
  vast_obj* car;
  vast_symbol* sym;
  ustring* sym_name;
UBEGIN
  reader = jreq->reader;
  symtb = reader->symtb;
  buff = reader->buff;
  ubuffer_ready_write(buff);
  while (1) {
    int i = 0;
    car = vast_car(end);
    if (vast_symbolp(car)) {
      uabort("not a symbol!");
    }
    sym = (vast_symbol*)car;
    sym_name = sym->name;
    for (i = 0; i < sym_name->len; i++) {
      if (ubuffer_write_next(buff, sym_name->value[i]) == -1) {
        uabort("buffer overflow!");
      }
    }
    if (ubuffer_write_int2ascii(buff, i) == -1) {
      uabort("buffer overflow!");
    }
    if (begin == end) {
      break;
    }
    end = vast_pdr(end);
  }
  if (ubuffer_write_next(buff, '\0') == -1) {
    uabort("buffer overflow!");
  }
  ubuffer_ready_read(buff);
  return ustring_table_put(symtb, buff->data, -1);
UEND

#include "_l5temp.attr"

static void vcall l5attr_init(vreader* reader)
{
  l5attr_file_concat_init(reader);
}

UDEFUN(UFNAME l5kw_init,
       UARGS (vreader* reader),
       URET static void)
UDECLARE
  vast_kw kw;
UBEGIN
  /*keyword init*/
  #define DF(no,str) \
  kw.kw_type = no; \
  kw.kw_str = str; \
  if (vreader_keyword_put(reader,kw)) { \
    uabort("keyword put error!"); \
  }
  #include "l5kw.h"
  #undef DF
UEND

UDEFUN(UFNAME l5_conf_kw_init,
       UARGS (vreader* reader),
       URET static void)
UDECLARE
  vast_kw kw;
UBEGIN
  /*keyword init*/
  #define DF(no,str) \
  kw.kw_type = no; \
  kw.kw_str = str; \
  if (vreader_keyword_put(reader,kw)) { \
    uabort("keyword put error!"); \
  }
  #include "l5.conf.kw.h"
  #undef DF
UEND

UDEFUN(UFNAME l5startup,
       UARGS (char* self_path,
              int vm_gc_asz,
              int vm_gc_ssz,
              int vm_gc_rsz),
       URET l5api l5eval* l5call)
UDECLARE
  l5eval* eval;
  leval* base_eval;
UBEGIN
  base_eval = lstartup(self_path,
                       vm_gc_asz,
                       vm_gc_ssz,
                       vm_gc_rsz,
                       l5attr_init,
                       l5conf_init,
                       l5kw_init,
                       l5_conf_kw_init,
                       UNULL,
                       &lclosure_attr_symcall);
  if (!base_eval) {
    uabort("startup base eval error!");
  }
  
  eval = ualloc(sizeof(l5eval));
  if (!eval) {
    uabort("new eval error!");
  }
  eval->base_eval = base_eval;
  eval->top_closure = UNULL;
  vm_m1_mod_fuse(base_eval->vm, VMOD_FUSE_YES);
  
  return eval;
UEND

UDEFUN(UFNAME l5eval_init,
       UARGS (l5eval* eval),
       URET l5api void l5call)
UDECLARE
  vps_cntr* vps;
  vclosure* top_closure;
UBEGIN
  leval_init(eval->base_eval);
  vps = &eval->base_eval->vm->vps;
  top_closure = (vclosure*)vclosure_none_new(vps);
  if (!top_closure) {
    uabort("top closure new error!");
  }
  eval->top_closure = top_closure;
UEND

UDEFUN(UFNAME l5eval_interactive_enable,
       UARGS (l5eval* eval),
       URET l5api void l5call)
UDECLARE

UBEGIN
  leval_interactive_enable(eval->base_eval);
UEND

UDEFUN(UFNAME l5eval_cmd_arg_put,
       UARGS (l5eval* eval,char* arg),
       URET l5api int l5call)
UDECLARE

UBEGIN
  return leval_cmd_arg_put(eval->base_eval, arg);
UEND

UDEFUN(UFNAME l5eval_src_path_set,
       UARGS (l5eval* eval,char* file_path),
       URET l5api int l5call)
UDECLARE
  leval* base_eval;
UBEGIN
  base_eval = eval->base_eval;
  leval_src_load(base_eval,file_path);
  return 0;
UEND

UDEFUN(UFNAME l5eval_src_load,
       UARGS (l5eval* eval),
       URET l5api int l5call)
UDECLARE
  vreader* reader;
  leval* base_eval;
  vast_eval_req req;
UBEGIN
  base_eval = eval->base_eval;
  reader = base_eval->src_reader;
  req.eval = (leval*)eval;
  req.conf_level = LEVAL_CONF_LEVEL_USR;
  vfile2closure((vps_closure_req*)&req,
                eval->top_closure,
                reader,
                reader->fi.file_name,
                reader->fi.file_path,
                &base_eval->vm->vps,
                base_eval->cmd_args,
                VCLOSURE_TYPE_MAIN);
  vclosure2mod(eval->top_closure,&base_eval->vm->vps,UNULL);
  eval->top_closure = UNULL;
  vreader_clean(reader);
  vcontext_vps_load(base_eval->vm->ctx_main,&base_eval->vm->vps);
  return 0;
UEND

UDEFUN(UFNAME l5eval_cmd_easy_syntax,
       UARGS (ustream* stream),
       URET l5api void l5call)
UDECLARE
  ubuffer* buff;
  int c;
  long limit;
UBEGIN
  buff = ustream_buff_get(stream);
  c = ubuffer_read_at(buff,0);
  if (c == '(') {
    return;
  }
  ubuffer_insert_next(buff,0,'(');
  limit = ubuffer_limit_get(buff);
  ubuffer_insert_next(buff,limit,')');
UEND

UDEFUN(UFNAME l5eval_cmd_load,
       UARGS (l5eval* eval),
       URET l5api int l5call)
UDECLARE
  URI_DEFINE;
  vreader* reader;
  leval* base_eval;
  ustream* cmd_stream;
  ustream* stdin_stream;
  vast_eval_req req;
UBEGIN
  base_eval = eval->base_eval;
  req.eval = (leval*)eval;
  req.conf_level = LEVAL_CONF_LEVEL_USR;
  reader = base_eval->cmd_reader;
  cmd_stream = base_eval->cmd_stream;
  
  stdin_stream = ustream_stdin(URI_REF);
  URI_ERROR;
    uabort(URI_DESC);
  URI_END;
  
  ustream_read_to_stream(cmd_stream,stdin_stream,URI_REF);
  URI_ERROR;
    uabort(URI_DESC);
  URI_END;
  
  /* easy cmd syntax */
  l5eval_cmd_easy_syntax(cmd_stream);
  
  vstream2closure((vps_closure_req*)&req,
                  eval->top_closure,
                  reader,
                  &ustring_empty,
                  &ustring_empty,
                  &base_eval->vm->vps,
                  cmd_stream);
  vclosure2mod(eval->top_closure,&base_eval->vm->vps,UNULL);
  eval->top_closure = UNULL;
  vreader_clean(reader);
  vcontext_vps_load(base_eval->vm->ctx_main,&base_eval->vm->vps);
  return 0;
UEND

UDEFUN(UFNAME l5eval_lib_load_s,
       UARGS (l5eval* eval,
              char* mod_name,
              char* file_path,
              int conf_level),
       URET l5api int l5call)
UDECLARE
  vreader* reader;
  leval* base_eval;
  ustring* mod_name_us;
  vast_eval_req req;
UBEGIN
  base_eval = eval->base_eval;
  req.eval = (leval*)eval;
  req.conf_level = conf_level;
  reader = base_eval->lib_reader;
  mod_name_us = ustring_table_put(reader->symtb,mod_name,-1);
  if (!mod_name_us) {
    uabort("module name put symtb error!");
  }
  leval_lib_load_s(base_eval,file_path);
  vfile2closure((vps_closure_req*)&req,
                eval->top_closure,
                reader,
                mod_name_us,
                reader->fi.file_path,
                &base_eval->vm->vps,
                UNULL,
                VCLOSURE_TYPE_FILE);
  vreader_clean(reader);
  return 0;
UEND

UDEFUN(UFNAME l5eval_lib_load_us,
       UARGS (l5eval* eval,
              ustring* mod_name,
              ustring* file_path,
              int conf_level),
       URET l5api int l5call)
UDECLARE
  vreader* reader;
  leval* base_eval;
  vast_eval_req req;
UBEGIN
  base_eval = eval->base_eval;
  req.eval = (leval*)eval;
  req.conf_level = conf_level;
  reader = base_eval->lib_reader;
  leval_lib_load_us(base_eval,file_path);
  vfile2closure((vps_closure_req*)&req,
                eval->top_closure,
                reader,
                mod_name,
                reader->fi.file_path,
                &base_eval->vm->vps,
                UNULL,
                VCLOSURE_TYPE_FILE);
  vreader_clean(reader);
  return 0;
UEND

UDEFUN(UFNAME l5eval_conf_load_s,
       UARGS (l5eval* eval,char* file_path,int conf_level),
       URET l5api int l5call)
UDECLARE
  vreader* reader;
  vast_conf_req req;
  vast_attr_res res;
UBEGIN
  reader = eval->base_eval->conf_reader;
  leval_conf_load_s(eval->base_eval,file_path);
  vast_attr_req_init(req,reader);
  req.eval = (leval*)eval;
  req.conf_level = conf_level;
  if (vfile2obj(reader,reader->fi.file_path,(vast_attr_req*)&req,&res)) {
    uabort("file2obj error!");
  }
  return 0;
UEND

UDEFUN(UFNAME l5eval_conf_load_us,
       UARGS (l5eval* eval,ustring* file_path,int conf_level),
       URET l5api int l5call)
UDECLARE
  vreader* reader;
  vast_conf_req req;
  vast_attr_res res;
UBEGIN
  reader = eval->base_eval->conf_reader;
  leval_conf_load_us(eval->base_eval,file_path);
  vast_attr_req_init(req,reader);
  req.eval = (leval*)eval;
  req.conf_level = conf_level;
  if (vfile2obj(reader,reader->fi.file_path,(vast_attr_req*)&req,&res)) {
    uabort("file2obj error!");
  }
  return 0;
UEND

bin = libl$(suf_so)
obj = leval.obj

libu_path=..\u\

libv_path=..\v\

v_somk=makefile

CFLAGS = $(STDC98) $(WALL) $(WEXTRA) $(WNO_UNUSED_PARAMETER) $(DEBUG_MODE)

$(bin):libv.lib $(obj)
	$(LINK) $(obj) $(L)$(libv_path) libv.lib $(L)$(libu_path) libu.lib $(OUT)$(bin) $(SHARED)
.c.obj:
	$(CC) $(C) $(COUT)$@ $< $(I) $(libv_path) $(I) $(libu_path) $(CFLAGS) $(FPIC) $(D)_EXPORT_LIBL_
libv.lib:
	cd $(libv_path)
	nmake /f $(v_somk) DEBUG_MODE=$(DEBUG_MODE)
	cd $(currdir)
install:
	copy $(bin) $(prefix)
uninstall:
	del $(prefix)\$(bin)
clean:
	del $(bin) $(obj) *.lib *.exp

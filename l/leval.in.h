#ifndef _LEVAL_IN_H_
#define _LEVAL_IN_H_

#include "ulib.h"
#include "ulist_tpl.h"
#include "vm.h"
#include "vparser.h"
#include "vcfun.h"
#include "vclosure.h"
#include "lmacro.h"

typedef struct _leval leval;
typedef struct _leval_loader leval_loader;
typedef struct _vast_eval_req vast_eval_req;
typedef struct _vast_conf_req vast_conf_req;

typedef void(*lmod_init_ft)(vcontext* ctx,vmod* mod);
typedef void(*lkw_init_ft)(vreader* reader);

#ifndef _UMAP_TPL_USTRINGP_
umap_decl_tpl(ustringp);
#endif

#ifndef _ULIST_TPL_USTRINGP_
ulist_decl_tpl(l,ustringp);
#endif

struct _leval_loader{
  VMOD_LOADER_HEADER;
  leval* eval;
};

#define LEVAL_MODE_ACTIVE 1
#define LEVAL_MODE_STATIC 2

struct _leval{
  vm_m1* vm;
  ulist_ustringp* path_list;
  ustring* self_path;
  ustring* src_path;
  ustring* curr_path;
  vreader* src_reader;
  vreader* cmd_reader;
  ustream* cmd_stream;
  vreader* lib_reader;
  vreader* conf_reader;
  leval_loader loader;
  vps_data* cmd_args;
  lkw_init_ft kw_init;
  int mode;
};

#define LEVAL_CONF_LEVEL_SYS 1
#define LEVAL_CONF_LEVEL_USR 2

struct _vast_eval_req{
  VPS_CLOSURE_REQ_HEADER;
  leval* eval;
  unsigned char conf_level;
};

struct _vast_conf_req{
  VAST_ATTR_REQ_HEADER;
  leval* eval;
  unsigned char conf_level;
};

UDECLFUN(UFNAME lstartup,
	 UARGS (char* self_path,
          int vm_gc_asz,
          int vm_gc_ssz,
          int vm_gc_rsz,
          vattr_init_ft attr_init,
          vattr_init_ft conf_attr_init,
          lkw_init_ft kw_init,
          lkw_init_ft conf_kw_init,
          vmod_load_ft prod,
          vast_attr* symcall),
	 URET lapi leval* lcall);

#define leval_path_self(eval) \
  ((eval)->curr_path = (eval)->self_path)

#define leval_path_client(eval) \
  ((eval)->curr_path = (eval)->src_path)
  
#define leval_path_set(eval,path) \
  ((eval)->curr_path = (path))

#define leval_path_get(eval) \
  ((eval)->curr_path)

UDECLFUN(UFNAME leval_init,
         UARGS (leval* eval),
         URET lapi void lcall);

UDECLFUN(UFNAME leval_interactive_enable,
         UARGS (leval* eval),
         URET lapi void lcall);

UDECLFUN(UFNAME leval_vps_load,
         UARGS (leval* eval,ustring* name,ustring* path),
         URET lapi vps_mod* lcall);

UDECLFUN(UFNAME leval_src_load,
	 UARGS (leval* eval,char* file_path),
	 URET lapi int lcall);

UDECLFUN(UFNAME leval_lib_load_s,
	 UARGS (leval* eval,char* file_path),
	 URET lapi int lcall);

UDECLFUN(UFNAME leval_lib_load_us,
         UARGS (leval* eval,ustring* file_path),
         URET lapi int lcall);

UDECLFUN(UFNAME leval_conf_load_s,
	 UARGS (leval* eval,char* file_path),
	 URET lapi int lcall);

UDECLFUN(UFNAME leval_conf_load_us,
	 UARGS (leval* eval,ustring* file_path),
	 URET lapi int lcall);

UDECLFUN(UFNAME leval_local_name_get,
	 UARGS(vtoken_state* ts,
	       ustring* src_name,
	       char sep,
	       char* suff),
	 URET lapi ustring* lcall);

UDECLFUN(UFNAME leval_cmd_arg_put,
         UARGS(leval* eval, char* arg),
         URET lapi int lcall);

UDECLFUN(UFNAME leval_closure_arg_call,
         UARGS (vclosure* closure,
                vast_eval_req* req,
                vast_obj* obj,
                unsigned char has_retval),
         URET lapi void lcall);

UDECLFUN(UFNAME leval_data_receive_call,
         UARGS (vast_eval_req* jreq,
                vcfg_graph* grp,
                vclosure_data* data,
                vast_symbol* sym_name),
         URET lapi void lcall);

UDECLFUN(UFNAME leval_data_send_call,
         UARGS (vast_eval_req* jreq,
                vcfg_graph* grp,
                vclosure_data* data,
                vast_symbol* sym_name),
         URET lapi void lcall);

UDECLFUN(UFNAME leval_data_set_call,
         UARGS (vast_eval_req* jreq,
                vcfg_graph* grp,
                vclosure_data* data,
                vast_symbol* sym_name),
         URET lapi void lcall);

UDECLFUN(UFNAME leval_data_ref_call,
         UARGS (vast_eval_req* jreq,
                vcfg_graph* grp,
                vclosure_data* data,
                vast_symbol* sym_name),
         URET lapi void lcall);

UDECLFUN(UFNAME leval_curr_path_match,
         UARGS (leval* eval,
                ustring* file_path,
                ustringp* out_path),
         URET lapi int lcall);

UDECLFUN(UFNAME leval_lib_path_add_s,
         UARGS (leval* eval,
                char* lib_path),
         URET lapi int lcall);

UDECLFUN(UFNAME leval_lib_path_add_us,
         UARGS (leval* eval,
                ustring* lib_path),
         URET lapi int lcall);

#endif

bin = libl3$(suf_so)
obj = l3eval.o
temp_attr_file = _l3temp.attr

libl_path=../../l/
libu_path=../../u/
libv_path=../../v/

l_sobj=l
v_sobj=v
u_sobj=u

l_somk=makefile

CFLAGS=-std=c89 -Wall -Wextra -Wno-unused-parameter $(DEBUG_MODE)

ATTR = l3base

define gen_attr_file
	$(libv_path)/attr.sh --attr=$(ATTR) --out=$(temp_attr_file) \
	--callback=l3attr_file_concat_init
endef

$(bin):$(temp_attr_file) $(obj) $(l_sobj)
	$(CC) $(obj) -L$(libl_path) -l$(l_sobj) -L$(libv_path) -l$(v_sobj) -L$(libu_path) -l$(u_sobj) \
	-o $(bin) -shared
.c.o:
	$(CC) -c -o $@ $< -I $(libl_path) -I $(libv_path) -I $(libu_path) $(CFLAGS) -fPIC
$(temp_attr_file):
	$(call gen_attr_file)
$(l_sobj):
	make -C $(libl_path) -f $(l_somk) DEBUG_MODE=$(DEBUG_MODE)
install:
	cp $(bin) $(prefix)/
uninstall:
	rm $(prefix)/$(bin)
clean:
	rm -f $(bin) $(obj) $(temp_attr_file)

#ifndef _L3EVAL_H_
#define _L3EVAL_H_

#include "vclosure.h"
#include "l3macro.h"
#include "leval.h"

typedef struct _l3eval l3eval;

struct _l3eval {
  leval* base_eval;
  vclosure* top_closure;
};

enum l3kwk{
  #define DF(no,str) no,
  #include "l3kw.h"
  #undef DF
};

UDECLFUN(UFNAME l3startup,
         UARGS(char* self_path,
               int vm_gc_asz,
               int vm_gc_ssz,
               int vm_gc_rsz),
         URET l3api l3eval* l3call);

UDECLFUN(UFNAME l3eval_cmd_arg_put,
         UARGS(l3eval* eval, char* arg),
         URET l3api int l3call);

UDECLFUN(UFNAME l3eval_src_load,
         UARGS(l3eval* eval, char* file_path),
         URET l3api int l3call);

UDECLFUN(UFNAME l3eval_lib_load,
         UARGS(l3eval* eval, char* file_path),
         URET l3api int l3call);

UDECLFUN(UFNAME l3eval_conf_load,
         UARGS(l3eval* eval, char* file_path),
         URET l3api int l3call);

#define l3eval_local_name_get(ts,src_name)		\
  leval_local_name_get(ts,src_name,'.',".l3");

#endif

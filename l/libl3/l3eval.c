#include "l3eval.h"
#include "l3.conf.attr"

static void vcall l3conf_init(vreader* reader)
{
  _vattr_file_init_l3_conf(reader);
}

UDEFUN(UFNAME leval3_closure_arg_call,
  UARGS(vclosure* closure, vps_closure_req* req, vast_obj* obj, unsigned char has_retval),
  URET static void)
UDECLARE
  vcfg_graph* grp;
  vps_inst* inst;
UBEGIN
  grp = ((vclosure_prog*)closure)->init;
  if (!vast_keywordp(obj)) {
    vast_keyword* kw;
    kw = (vast_keyword*)obj;
    if (kw->kw.kw_type == lkw_true) {
      inst = vps_ipushbool(req->vps, grp, vps_bool_true);
      vcfg_grp_inst_apd(grp, inst);
    }
    else if (kw->kw.kw_type == lkw_false) {
      inst = vps_ipushbool(req->vps, grp, vps_bool_false);
      vcfg_grp_inst_apd(grp, inst);
    }
    else if (kw->kw.kw_type == lkw_nil) {
      inst = vps_ipushnil(req->vps, grp);
      vcfg_grp_inst_apd(grp, inst);
    }
    else {
      uabort("keyword not define!");
    }
  } else {
    leval_closure_arg_call(closure, req, obj, has_retval);
  }

UEND

UDEFUN(UFNAME symcall_action,
       UARGS (vast_attr_req* req,
              vast_attr_res* res),
       URET static int vcall)
UDECLARE
  vast_obj* ast_obj;
  vps_closure_req* jreq;
  vps_cntr* vps;
  vclosure* closure;
  vcfg_graph* grp;
  vps_inst* inst;
  vast_obj* obj;
  vast_obj* next;
  vast_symbol* symbol;
  vclosure* func;
  vclosure_data* data;
  int cac; /* calling args count */
  int dac; /* declare args count */
UBEGIN
  jreq = (vps_closure_req*)req;
  ast_obj = jreq->ast_obj;
  vps = jreq->vps;
  closure = jreq->closure;
  if (!vclosure_is_body(closure)) {
    uabort("closure haven't init!");
  }
  grp = vclosure_init_get(closure);
  obj = vast_car(ast_obj);
  symbol = (vast_symbol*)obj;
  func = vclosure_func_get(closure,symbol->name);
  if (!func) {
    data = vclosure_field_get(closure, symbol->name);
    if (!data) {
      uabort("calling function not finding:%s!", symbol->name->value);
    }
  } else {
    data = UNULL;
  }
  /* push params */
  cac = 0;
  next = vast_cdr(ast_obj);
  while (next) {
    obj = vast_car(next);
    leval3_closure_arg_call(closure, jreq, obj, VATTR_RETVAL_YES);
    next = vast_cdr(next);
    cac++;
  }

  if (func) {
    dac = vclosure_func_args_count(func);
    if (dac > cac) {
      uabort("calling function args too few!");
    }
    else if (dac < cac) {
      uabort("calling function args too much!");
    }
  }

  if (data) {
    leval_data_receive_call(vps, grp, data->data);
    /* call variable function */
    inst = vps_icallx(vps,cac);
    vcfg_grp_inst_apd(grp,inst);
  } else {
    /* push subr name */
    inst = vps_ipushdt(vps, grp, symbol->name);
    vcfg_grp_inst_apd(grp, inst);
    /* call function */
    inst = vps_icall(vps);
    vcfg_grp_inst_apd(grp,inst);
  }

  return 0;
UEND

static vast_attr last_attr_symcall = { UNULL, UNULL, symcall_action, VATTR_RETVAL_YES };

#include "_l3temp.attr"

static void vcall l3attr_init(vreader* reader)
{
  l3attr_file_concat_init(reader);
}

UDEFUN(UFNAME l3kw_init,
       UARGS (vreader* reader),
       URET static void)
UDECLARE
  vast_kw kw;
UBEGIN
  /*keyword init*/
  #define DF(no,str) \
    kw.kw_type = no; \
    kw.kw_str = str; \
    if (vreader_keyword_put(reader,kw)) { \
      uabort("keyword put error!"); \
    }
  #include "l3kw.h"
  #undef DF
UEND
  
UDEFUN(UFNAME l3startup,
       UARGS (char* self_path,
              int vm_gc_asz,
              int vm_gc_ssz,
              int vm_gc_rsz),
       URET l3api l3eval* l3call)
UDECLARE
  l3eval* eval;
  leval* base_eval;
  vclosure* top_closure;
UBEGIN
  base_eval = lstartup(self_path,
                       vm_gc_asz,
                       vm_gc_ssz,
                       vm_gc_rsz,
                       l3attr_init,
                       l3conf_init,
                       l3kw_init,
                       UNULL,
                       &last_attr_symcall);
  if (!base_eval) {
    uabort("startup base eval error!");
  }
  top_closure = (vclosure*)vclosure_none_new
    (&base_eval->vm->vps);
  if (!top_closure) {
    uabort("top closure new error!");
  }

  eval = ualloc(sizeof(l3eval));
  if (!eval) {
    uabort("new eval error!");
  }
  eval->base_eval = base_eval;
  eval->top_closure = top_closure;

  return eval;
UEND

UDEFUN(UFNAME l3eval_cmd_arg_put,
       UARGS(l3eval* eval, char* arg),
       URET l3api int l3call)
UDECLARE

UBEGIN
  return leval_cmd_arg_put(eval->base_eval, arg);
UEND

UDEFUN(UFNAME l3eval_src_load,
       UARGS(l3eval* eval, char* file_path),
       URET l3api int l3call)
UDECLARE
  vreader* reader;
  leval* base_eval;
UBEGIN
  base_eval = eval->base_eval;
  reader = base_eval->src_reader;
  leval_src_load(base_eval, file_path);
  vfile2closure(eval->top_closure,
                reader,
                reader->fi.file_name,
                reader->fi.file_path,
                &base_eval->vm->vps,
                base_eval->cmd_args,
                VCLOSURE_TYPE_MAIN);
  vclosure2mod(eval->top_closure,&base_eval->vm->vps,UNULL);
  vreader_clean(reader);
  vcontext_vps_load(base_eval->vm->ctx_main, &base_eval->vm->vps);
  return 0;
UEND

UDEFUN(UFNAME l3eval_lib_load,
       UARGS(l3eval* eval, char* file_path),
       URET l3api int l3call)
  UDECLARE
  vreader* reader;
  leval* base_eval;
UBEGIN
  base_eval = eval->base_eval;
  reader = base_eval->lib_reader;
  leval_lib_load(base_eval, file_path);
  vfile2closure(eval->top_closure,
                reader,
                reader->fi.file_name,
                reader->fi.file_path,
                &base_eval->vm->vps,
                UNULL,
                VCLOSURE_TYPE_FILE);
  vreader_clean(reader);
  return 0;
UEND

UDEFUN(UFNAME l3eval_conf_load,
       UARGS(l3eval* eval, char* file_path),
       URET l3api int l3call)
UDECLARE
  vreader* reader;
  vast_conf_req req;
  vast_attr_res res;
UBEGIN
  reader = eval->base_eval->conf_reader;
  leval_conf_load(eval->base_eval, file_path);
  vast_attr_req_init(req, reader);
  req.eval = (leval*)eval;
  if (vfile2obj(reader, reader->fi.file_path, (vast_attr_req*)&req, &res)) {
    uabort("file2obj error!");
  }
  return 0;
UEND

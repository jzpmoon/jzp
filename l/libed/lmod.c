#include "uref.h"
#include "ulib.h"
#include "lobj.h"
#include "_ltemp.cfun"

edapi void edcall
lcfun_init(vcontext* ctx,vmod* mod)
{
  lcfun_file_concat_init(ctx,mod);
}

UDEFUN(UFNAME dlinit_proc,
       UARGS (void),
       URET static void)
UDECLARE
UBEGIN
  lobj_fbuffer_mem_pool_init();
UEND

udlinit_register(dlinit_proc,UNULL)
#ifndef _LOBJ_H_
#define _LOBJ_H_

#include "ustream.h"
#include "ufbuffer.h"
#include "vgc_obj.h"
#include "vparser.h"
#include "vcontext.h"
#include "edmacro.h"

typedef struct _lobj_fbuffer{
  VGCHEADEREX;
  ufbuffer* fbuffer;
} lobj_fbuffer;

extern edapi vgc_objex_t lobj_fbuffer_type;

UDECLFUN(UFNAME lobj_fbuffer_new_by_file,
         UARGS (vgc_heap* heap,
                char* file_path,
                int buf_size_max,
                int row_buf_len_max,
                int col_buf_len_max),
         URET lobj_fbuffer*);

#define lobj_fbuffer_is_close(buf) \
  ((buf)->fbuffer == UNULL)
  
UDECLFUN(UFNAME lobj_fbuffer_close,
         UARGS (lobj_fbuffer* buf),
         URET void);
void lobj_fbuffer_mem_pool_init();

#endif

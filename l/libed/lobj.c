#include <stddef.h>
#include "umempool.h"
#include "lobj.h"

edapi vgc_objex_t lobj_fbuffer_type = {"fbuffer"};

static umem_pool mem_pool;

void lobj_fbuffer_mem_pool_init()
{
  umem_pool_init(&mem_pool,-1);
}

UDEFUN(UFNAME lobj_fbuffer_new_by_file,
       UARGS (vgc_heap* heap,
              char* file_path,
              int buf_size_max,
              int row_buf_len_max,
              int col_buf_len_max),
       URET lobj_fbuffer*)
UDECLARE
  lobj_fbuffer* fbuffer;
  ufbuffer* fbuff;
UBEGIN
  fbuffer = vgc_objvex_new(heap,lobj_fbuffer,&lobj_fbuffer_type);
  if(!fbuffer){
    uabort("fbuffer new error!");
  }
  
  fbuff = ufbuffer_alloc(
    &mem_pool.allocator,
    file_path,
    buf_size_max,
    row_buf_len_max,
    col_buf_len_max);
  if (!fbuff) {
    uabort("ufbuffer_alloc error");
  }
  fbuffer->fbuffer = fbuff;
  return fbuffer;
UEND

UDEFUN(UFNAME lobj_fbuffer_close,
       UARGS (lobj_fbuffer* buf),
       URET void)
UDECLARE

UBEGIN
  if (!buf->fbuffer) {
    return;
  }
  ufbuffer_des(buf->fbuffer);
  buf->fbuffer = UNULL;
UEND
#ifndef _LOBJ_H_
#define _LOBJ_H_

#include "ustream.h"
#include "vgc_obj.h"
#include "vparser.h"
#include "vcontext.h"
#include "lmmacro.h"

typedef struct _lobj_stream{
  VGCHEADEREX;
  ustream* stream;
} lobj_stream;

extern lmapi vgc_objex_t lobj_stream_type;

lobj_stream*
lobj_istream_new_by_file(vgc_heap* heap,
                         ustring* file_path);

lobj_stream*
lobj_ostream_new_by_file(vgc_heap* heap,
                         ustring* file_path);

lobj_stream*
lobj_istream_new_by_fd(vgc_heap* heap,
                       USTDC_FILE* fd);

lobj_stream*
lobj_ostream_new_by_fd(vgc_heap* heap,
                       USTDC_FILE* fd);

lobj_stream* lobj_stream_stdin_new(vgc_heap* heap);

lobj_stream* lobj_stream_stdout_new(vgc_heap* heap);

lobj_stream* lobj_stream_stderr_new(vgc_heap* heap);

lobj_stream* 
lobj_istream_new(vcontext* ctx,
                 vgc_string* fn);

lobj_stream*
lobj_ostream_new(vcontext* ctx,
                 vgc_string* fn);

void lobj_stream_close(lobj_stream* stream);

#endif

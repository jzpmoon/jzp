#include "ulist_tpl.c"
#include "leval.in.h"

#ifndef _ULIST_TPL_USTRINGP_
ulist_def_tpl(l,ustringp);
#endif

UDEFUN(UFNAME leval_vps_load,
       UARGS (leval* eval,ustring* name,ustring* path),
       URET lapi vps_mod* lcall)
UDECLARE

UBEGIN
  return UNULL;
UEND

UDEFUN(UFNAME leval_src_load,
       UARGS (leval* eval,char* file_path),
       URET lapi int lcall)
UDECLARE
  vreader* reader;
  ustring* file_path_us;
  ulsnd_ustringp* first_nd;
UBEGIN
  reader = eval->src_reader;
  file_path_us = ustring_table_put(reader->symtb,file_path,-1);
  if (!file_path_us) {
    uabort("file path put symtb error!");
  }
  if (vreader_fi_init(reader, UNULL, file_path_us)) {
    uabort("file infor init error!");
  }
  first_nd = ulist_nd_first_get(eval->path_list);
  ulist_nd_value_set(first_nd,reader->fi.dir_name);
  eval->src_path = reader->fi.dir_name;
  return 0;
UEND

UDEFUN(UFNAME leval_lib_load_s,
       UARGS (leval* eval,char* file_path),
       URET lapi int lcall)
UDECLARE
  vreader* reader;
  ustring* file_path_us;
UBEGIN
  reader = eval->lib_reader;
  file_path_us = ustring_table_put(reader->symtb,file_path,-1);
  if (!file_path_us) {
    uabort("file path put symtb error!");
  }
  return leval_lib_load_us(eval, file_path_us);
UEND

UDEFUN(UFNAME leval_lib_load_us,
       UARGS (leval* eval,ustring* file_path),
       URET lapi int lcall)
UDECLARE
  vreader* reader;
UBEGIN
  reader = eval->lib_reader;
  if (vreader_fi_init(reader,eval->curr_path,file_path)) {
    uabort("file infor init error!");
  }
  return 0;
UEND

UDEFUN(UFNAME leval_conf_load_s,
       UARGS (leval* eval,char* file_path),
       URET lapi int lcall)
UDECLARE
  vreader* reader;
  ustring* file_path_us;
UBEGIN
  reader = eval->conf_reader;
  file_path_us = ustring_table_put(reader->symtb,file_path,-1);
  if (!file_path_us) {
    uabort("file path put symtb error!");
  }
  leval_conf_load_us(eval,file_path_us);
  return 0;
UEND

UDEFUN(UFNAME leval_conf_load_us,
       UARGS (leval* eval,ustring* file_path),
       URET lapi int lcall)
UDECLARE
  vreader* reader;
UBEGIN
  reader = eval->conf_reader;
  if (vreader_fi_init(reader,eval->curr_path,file_path)) {
    uabort("file infor init error!");
  }
  return 0;
UEND

UDEFUN(UFNAME leval_local_name_get,
       UARGS(vtoken_state * ts,
             ustring * src_name,
             char sep,
             char* suff),
       URET lapi ustring* lcall)
UDECLARE
  ubuffer* buff;
  int i;
UBEGIN
  buff = ts->buff;
  ubuffer_ready_write(buff);
  for (i = 0; i < src_name->len; i++) {
    int c = src_name->value[i];
    if (c == sep) {
      c = UDIR_SEP;
    }
    if (ubuffer_write_next(buff, c)) {
      uabort("src name to local name over of buffer!");
    }
  }
  for (i = 0; ; i++) {
    int c = suff[i];
    if (ubuffer_write_next(buff, c)) {
      uabort("src name to local name over of buffer!");
    }
    if (c == '\0') {
      break;
    }
  }
  ubuffer_ready_read(buff);
  return ustring_table_put(ts->reader->symtb, buff->data, -1);
UEND

UDEFUN(UFNAME leval_loader_load,
       UARGS (vmod_loader* loader,vmod* mod),
       URET static int vcall)
UDECLARE
  vps_mod* src_mod;
  leval_loader* eval_loader;
  leval* eval;
UBEGIN
  eval_loader = (leval_loader*)loader;
  eval = eval_loader->eval;
  src_mod = leval_vps_load(eval,mod->name,mod->path);
  vcontext_mod2mod(eval->vm->ctx_main,mod,src_mod);
  vps_cntr_clean(&eval->vm->vps);
  return 0;
UEND

static ustring* cmd_args_str = UNULL;

UDEFUN(UFNAME lstartup,
       UARGS (char* self_path,
              int vm_gc_asz,
              int vm_gc_ssz,
              int vm_gc_rsz,
              vattr_init_ft attr_init,
              vattr_init_ft conf_attr_init,
              lkw_init_ft kw_init,
              lkw_init_ft conf_kw_init,
              vmod_load_ft prod,
              vast_attr* symcall),
       URET lapi leval* lcall)
UDECLARE
  leval* eval;
  vm_m1* vm;
  vcontext* ctx;
  ulist_ustringp* path_list;
  ustring* self_path_str;
  vreader* src_reader;
  vreader* lib_reader;
  vreader* conf_reader;
  leval_loader loader;
UBEGIN
  eval = ualloc(sizeof(leval));
  if (!eval) {
    uabort("new eval error!");
  }

  vm = vm_m1_h1_alloc(&u_global_allocator,
                      vm_gc_ssz,
                      vm_gc_asz,
                      vm_gc_rsz);
  if (!vm) {
    uabort("new vm m1 error!");
  }

  ctx = vm->ctx_main;

  self_path_str = ustring_table_put(ctx->symtb,self_path,-1);
  if (!self_path_str) {
    uabort("self path new error!");
  }

  src_reader = vreader_new(ctx->symtb,
                           ctx->strtb,
                           attr_init,
                           symcall);
  if (!src_reader) {
    uabort("new source reader error!");
  }

  lib_reader = vreader_new(ctx->symtb,
                           ctx->strtb,
                           attr_init,
                           symcall);
  if (!lib_reader) {
    uabort("new library reader error!");
  }

  conf_reader = vreader_new(ctx->symtb,
                            ctx->strtb,
                            conf_attr_init,
                            UNULL);
  if (!conf_reader) {
    uabort("new configure reader error!");
  }

  /*keyword init*/
  if (kw_init) {
    kw_init(src_reader);
  }
  if (kw_init) {
    kw_init(lib_reader);
  }
  if (conf_kw_init) {
    conf_kw_init(conf_reader);
  }

  cmd_args_str = ustring_table_put(ctx->symtb,
    "cmd-args", -1);
  if (!cmd_args_str) {
    uabort("cmd-args put symtb error!");
  }

  eval->vm = vm;

  path_list = ulist_ustringp_new();
  if(!path_list) {
    uabort("path list new error!");
  }
  eval->path_list = path_list;
  if (ulist_ustringp_append(path_list,UNULL)) {
    uabort("src path append error!");
  }
  if (ulist_ustringp_append(path_list,self_path_str)) {
    uabort("self path append error!");
  }

  eval->self_path = self_path_str;
  eval->src_path = UNULL;
  eval->curr_path = UNULL;

  eval->src_reader = src_reader;
  eval->cmd_reader = UNULL;
  eval->cmd_stream = UNULL;
  eval->lib_reader = lib_reader;
  eval->conf_reader = conf_reader;
  eval->cmd_args = UNULL;
  eval->kw_init = kw_init;
  eval->mode = LEVAL_MODE_STATIC;

  if (prod) {
    loader.load = prod;
  } else {
    loader.load = leval_loader_load;
  }
  loader.eval = eval;

  eval->loader = loader;
  
  vm_m1_mod_loader_set(vm,(vmod_loader*)&eval->loader);

  return eval;
UEND

UDEFUN(UFNAME leval_init,
       UARGS (leval* eval),
       URET lapi void lcall)
UDECLARE
  vm_m1* vm;
  vps_data* cmd_args;
UBEGIN
  if (eval->mode == LEVAL_MODE_ACTIVE) {
    return;
  }
  vm = eval->vm;
  cmd_args = vps_array_new(&vm->vps, cmd_args_str);
  if (!cmd_args) {
    uabort("cmd args new error!");
  }
  vps_data_scope_set(cmd_args,VPS_SCOPE_GLOBAL);
  eval->cmd_args = cmd_args;
UEND

UDEFUN(UFNAME leval_interactive_enable,
       UARGS (leval* eval),
       URET lapi void lcall)
UDECLARE
  URI_DEFINE;
  vm_m1* vm;
  vreader* cmd_reader;
  ubuffer* cmd_buff;
  ustream* cmd_stream;
UBEGIN
  vm = eval->vm;
  vm_m1_mod_fuse(vm, VMOD_FUSE_YES);
  vps_cntr_check_mode(&vm->vps, VPS_CHECK_TOLERANT);
  if (!eval->cmd_reader) {
    cmd_reader = vreader_new(vm->ctx_main->symtb,
                             vm->ctx_main->strtb,
                             eval->src_reader->ainit,
                             eval->src_reader->dattr);
    if (!cmd_reader) {
      uabort("new cmd reader error!");
    }
    eval->cmd_reader = cmd_reader;
  }
  if (!eval->cmd_stream) {
    cmd_buff = ubuffer_new(100);
    if (!cmd_buff) {
      uabort("buffer new error!");
    }
    cmd_stream = ustream_new_by_buff(USTREAM_INPUT,cmd_buff,URI_REF);
    URI_ERROR;
      uabort(URI_DESC);
    URI_END;
    eval->cmd_stream = cmd_stream;
  }
  /*keyword init*/
  if (eval->kw_init) {
    eval->kw_init(cmd_reader);
  }
  eval->mode = LEVAL_MODE_ACTIVE;
UEND

UDEFUN(UFNAME leval_cmd_arg_put,
       UARGS (leval* eval, char* arg),
       URET lapi int lcall)
UDECLARE
  ustring* uarg;
  vps_data* data;
UBEGIN
  uarg = ustring_table_add(eval->src_reader->strtb, arg, -1);
  if (!uarg) {
    uabort("arg put strtb error!");
  }
  data = vps_str_new(&eval->vm->vps, UNULL, uarg);
  if (!data) {
    uabort("vps string new error!");
  }
  if (vps_array_apd(eval->cmd_args, data)) {
    uabort("put arg error!");
  }
  return 0;
UEND

UDEFUN(UFNAME leval_closure_arg_call,
       UARGS (vclosure* closure,
              vast_eval_req* req,
              vast_obj* obj,
              unsigned char has_retval),
       URET lapi void lcall)
UDECLARE
  vast_eval_req dreq;
  vast_attr_res dres;
  vps_cntr* vps;
  vcfg_graph* grp;
  vcfg_graph* func_grp;
  vps_inst* inst;
  ustring* decoration;
UBEGIN
  vps = req->vps;
  if (!vclosure_is_body(closure)) {
    uabort("closure haven't init!");
  }
  grp = vclosure_init_get(closure);
  if (!obj) {
    uabort("can not calling empty list!");
  }
  if (obj->t == vastk_symbol) {
    vast_symbol* sym = (vast_symbol*)obj;
    vclosure_data* data = vclosure_field_get(closure, sym->name);
    if (data) {
      leval_data_receive_call(req, grp, data, sym);
    } else {
      vclosure* func = vclosure_func_get(closure, sym->name);
      if (func) {
        func_grp = vclosure_init_get(func);
        decoration = func_grp->id.decoration;
        inst = vps_ipushdt(vps, grp, decoration);
        vcfg_grp_inst_apd(grp, inst);
      } else {
        if (vps_cntr_check_is_tolerant(vps)) {
          decoration = vps->symgen(req->reader->symtb, req->reader->buff,
                                   sym, vfe_ast, vastk_symbol,
                                   vfe_ast, vastk_symbol);
          if (!decoration) {
            uabort("decoration error!");
          }
          inst = vps_ipushdt(vps, grp, decoration);
          vcfg_grp_inst_apd(grp, inst);
        } else {
          uabort("symbol:%s not a variable or function!", sym->name->value);
        }
      }
    }
  } else if (obj->t == vastk_integer) {
    vast_integer* inte = (vast_integer*)obj;
    inst = vps_ipushint(vps, grp, inte->name, inte->value);
    vcfg_grp_inst_apd(grp, inst);
  } else if (obj->t == vastk_number) {
    vast_number* num = (vast_number*)obj;
    inst = vps_ipushnum(vps, grp, num->name, num->value);
    vcfg_grp_inst_apd(grp, inst);
  } else if (obj->t == vastk_string) {
    vast_string* str = (vast_string*)obj;
    inst = vps_ipushstr(vps, grp, str->value);
    vcfg_grp_inst_apd(grp, inst);
  } else if (obj->t == vastk_character) {
    vast_character* chara = (vast_character*)obj;
    inst = vps_ipushchar(vps, grp, chara->value);
    vcfg_grp_inst_apd(grp, inst);
  } else if (!vast_consp(obj)) {
    vast_obj* car = vast_car(obj);
    if (!vast_symbolp(car)) {
      vast_symbol* sym = (vast_symbol*)car;
      vast_attr* attr = sym->attr;
      if (attr) {
        if (!(attr->has_retval & has_retval)) {
          uabort("attr \"%s\" retval error!", attr->sname);
        }
        dreq = vast_req_dbl(req);
        dreq.ast_obj = obj;
        dreq.closure = closure;
        vast_attr_call(attr, (vast_attr_req*)&dreq, &dres);
      } else {
        uabort("symbol has no attr!");
      }
    } else {
      uabort("car not a symbol!");
    }
  } else {
    uabort("push inst error!");
  }
UEND

UDEFUN(UFNAME leval_data_receive_call,
       UARGS (vast_eval_req* jreq,
              vcfg_graph* grp,
              vclosure_data* cdata,
              vast_symbol* sym_name),
       URET lapi void lcall)
UDECLARE
  vps_data* data;
  vps_cntr* vps;
  vps_inst* inst;
  ustring* decoration;
UBEGIN
  vps = jreq->vps;
  if (cdata) {
    data = cdata->data;
    if (vps_data_scope_of(data, VPS_SCOPE_LOCAL)) {
      inst = vps_iloadv(vps, data);
      vcfg_grp_inst_apd(grp, inst);
    } else if (vps_data_scope_of(data, VPS_SCOPE_GLOBAL)) {
      inst = vps_ipushdt(vps, grp, data->id.decoration);
      vcfg_grp_inst_apd(grp, inst);
    } else if (vps_data_scope_of(data, VPS_SCOPE_STATIC)) {
      inst = vps_ipushdt(vps, grp, data->id.decoration);
      vcfg_grp_inst_apd(grp, inst);
    } else if (vps_data_scope_of(data, VPS_SCOPE_DECL)) {
      inst = vps_ipushdt(vps, grp, data->id.decoration);
      vcfg_grp_inst_apd(grp, inst);
    } else {
      uabort("variable scope error!");
    }
  } else {
    decoration = vps->symgen(jreq->reader->symtb, jreq->reader->buff,
                             sym_name, vfe_ast, vastk_symbol,
                             vfe_ast, vastk_symbol);
    if (!decoration) {
      uabort("decoration error!");
    }
    inst = vps_ipushdt(vps, grp, decoration);
    vcfg_grp_inst_apd(grp, inst);
  }
UEND

UDEFUN(UFNAME leval_data_send_call,
       UARGS (vast_eval_req* jreq,
              vcfg_graph* grp,
              vclosure_data* cdata,
              vast_symbol* sym_name),
       URET lapi void lcall)
UDECLARE
  vps_data* data;
  vps_cntr* vps;
  vps_inst* inst;
  ustring* decoration;
UBEGIN
  vps = jreq->vps;
  if (cdata) {
    data = cdata->data;
    if (vps_data_scope_of(data, VPS_SCOPE_LOCAL)) {
      inst = vps_istorev(vps, data);
      vcfg_grp_inst_apd(grp, inst);
    } else if (vps_data_scope_of(data, VPS_SCOPE_GLOBAL)) {
      inst = vps_ipopdt(vps, grp, data->id.decoration);
      vcfg_grp_inst_apd(grp, inst);
    } else if (vps_data_scope_of(data, VPS_SCOPE_STATIC)) {
      inst = vps_ipopdt(vps, grp, data->id.decoration);
      vcfg_grp_inst_apd(grp, inst);
    } else if (vps_data_scope_of(data, VPS_SCOPE_DECL)) {
      inst = vps_ipopdt(vps, grp, data->id.decoration);
      vcfg_grp_inst_apd(grp, inst);
    } else {
      uabort("variable scope error!");
    }
  } else {
    decoration = vps->symgen(jreq->reader->symtb, jreq->reader->buff,
			     sym_name, vfe_ast, vastk_symbol,
			     vfe_ast, vastk_symbol);
    if (!decoration) {
      uabort("decoration error!");
    }
    inst = vps_ipopdt(vps, grp, decoration);
    vcfg_grp_inst_apd(grp, inst);
  }
UEND

UDEFUN(UFNAME leval_data_set_call,
       UARGS (vast_eval_req* jreq,
              vcfg_graph* grp,
              vclosure_data* cdata,
              vast_symbol* sym_name),
       URET lapi void lcall)
UDECLARE
  vps_data* data;
  vps_cntr* vps;
  vps_inst* inst;
  ustring* decoration;
UBEGIN
  vps = jreq->vps;
  if (cdata) {
    data = cdata->data;
    if (vps_data_scope_of(data, VPS_SCOPE_LOCAL)) {
      inst = vps_isetv(vps, data);
      vcfg_grp_inst_apd(grp, inst);
    } else if (vps_data_scope_of(data, VPS_SCOPE_GLOBAL)) {
      inst = vps_isetgdt(vps, grp, data->id.decoration);
      vcfg_grp_inst_apd(grp, inst);
    } else if (vps_data_scope_of(data, VPS_SCOPE_STATIC)) {
      inst = vps_isetgdt(vps, grp, data->id.decoration);
      vcfg_grp_inst_apd(grp, inst);
    } else if (vps_data_scope_of(data, VPS_SCOPE_DECL)) {
      inst = vps_isetgdt(vps, grp, data->id.decoration);
      vcfg_grp_inst_apd(grp, inst);
    } else {
      uabort("variable scope error!");
    }
  } else {
    decoration = vps->symgen(jreq->reader->symtb, jreq->reader->buff,
			     sym_name, vfe_ast, vastk_symbol,
			     vfe_ast, vastk_symbol);
    if (!decoration) {
      uabort("decoration error!");
    }
    inst = vps_isetgdt(vps, grp, decoration);
    vcfg_grp_inst_apd(grp, inst);
  }
UEND

UDEFUN(UFNAME leval_data_ref_call,
       UARGS (vast_eval_req* jreq,
              vcfg_graph* grp,
              vclosure_data* cdata,
              vast_symbol* sym_name),
       URET lapi void lcall)
UDECLARE
  vps_data* data;
  vps_cntr* vps;
  vps_inst* inst;
  ustring* decoration;
UBEGIN
  vps = jreq->vps;
  if (cdata) {
    data = cdata->data;
    if (vps_data_scope_of(data, VPS_SCOPE_LOCAL)) {
      inst = vps_irefv(vps, data);
      vcfg_grp_inst_apd(grp, inst);
    } else if (vps_data_scope_of(data, VPS_SCOPE_GLOBAL)) {
      inst = vps_irefgdt(vps, grp, data->id.decoration);
      vcfg_grp_inst_apd(grp, inst);
    } else if (vps_data_scope_of(data, VPS_SCOPE_STATIC)) {
      inst = vps_irefgdt(vps, grp, data->id.decoration);
      vcfg_grp_inst_apd(grp, inst);
    } else if (vps_data_scope_of(data, VPS_SCOPE_DECL)) {
      inst = vps_irefgdt(vps, grp, data->id.decoration);
      vcfg_grp_inst_apd(grp, inst);
    } else {
      uabort("variable scope error!");
    }
  } else {
    decoration = vps->symgen(jreq->reader->symtb, jreq->reader->buff,
			     sym_name, vfe_ast, vastk_symbol,
			     vfe_ast, vastk_symbol);
    if (!decoration) {
      uabort("decoration error!");
    }
    inst = vps_irefgdt(vps, grp, decoration);
    vcfg_grp_inst_apd(grp, inst);
  }
UEND

UDEFUN(UFNAME leval_curr_path_match,
       UARGS (leval* eval,
              ustring* file_path,
              ustringp* out_path),
       URET lapi int lcall)
UDECLARE
  ucursor c;
  ulist_ustringp* path_list;
  ustring* base_path;
  ustring* file_full_path;
UBEGIN
  path_list = eval->path_list;
  path_list->iterate(&c);
  while (1) {
    unext_ustringp next = path_list->next((uset_ustringp*)path_list,&c);
    if (!unext_has(next)) {
      break;
    }
    base_path = unext_get(next);
    file_full_path = ustring_concat_by_strtb(
      eval->vm->symtb,
      base_path,
      file_path);
    if (!file_full_path) {
      return -1;
    }
    if (ufile_exists_us(file_full_path)) {
      *out_path = base_path;
      return 0;
    }
  }
  return -2;
UEND

UDEFUN(UFNAME leval_lib_path_add_s,
       UARGS (leval* eval,
              char* lib_path),
       URET lapi int lcall)
UDECLARE
  vreader* reader;
  ustring* lib_path_us;
UBEGIN
  reader = eval->lib_reader;
  lib_path_us = ustring_table_put(reader->symtb,lib_path,-1);
  if (!lib_path_us) {
    uabort("lib path put symtb error!");
  }
  return leval_lib_path_add_us(eval,lib_path_us);
UEND

UDEFUN(UFNAME leval_lib_path_add_us,
       UARGS (leval* eval,
              ustring* lib_path),
       URET lapi int lcall)
UDECLARE
  
UBEGIN
  return ulist_ustringp_append(eval->path_list,lib_path);
UEND

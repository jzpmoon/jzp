#!/usr/bin/bash
prefix=$(pwd)
envc="unx"
thw=arm64
currdir=$(pwd)

old_ifs=$IFS
for arg in $*
do
    IFS="="
    array=($arg)
    key=${array[0]}
    val=${array[1]}
    if [ $key = "--prefix" ]
    then
	prefix=$val
    elif [ $key = "--envc" ]
    then
	envc=$val
    elif [ $key = "--thw" ]
    then
	thw=$val
    fi
    IFS=$old_ifs
done

mk=makefile

echo "prefix=$prefix" > $mk
echo "envc=$envc" >> $mk
echo "thw=$thw" >> $mk
echo "currdir=$currdir" >> $mk

echo "include ../../u/env/env_"${envc}".mk" >> $mk

cat liblm.mk >> $mk

cd ../../l/
./configure.sh --prefix=$prefix --envc=$envc --thw=$thw
cd $currdir

echo "/*auto create*/" > dbgmacro.h

cat ../../u/uexport.h | sed 's:xapi:dbgapi:g' | sed 's:xcall:dbgcall:g' | \
    sed 's:_UEXPORT_H_:_UEXPORT_LIBDBG_H_:g' >> dbgmacro.h

#!/usr/bin/bash
currdir=$(pwd)
shdir=$(dirname $0)
cd $shdir
prefix=$(pwd)
cd $currdir
export PATH=$PATH:$prefix
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$prefix
$prefix/l5 --self-path $prefix/ --conf l5.conf --log-enable true --log-name l5.log $*

#!/usr/bin/bash
currdir=$(pwd)
shdir=$(dirname $0)
cd $shdir
prefix=$(pwd)
cd $currdir
export PATH=$PATH:$prefix
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$prefix
$prefix/l3 --self-path $prefix/ --conf l3.conf --log-enable true --log-name l3.log $*

#ifndef _USET_H_
#define _USET_H_

#include "umacro.h"
#include "uerror.h"

typedef struct _ucursor ucursor;

struct _ucursor{
  char pad[32];  
};

#define USETHEADER(t)				\
  uset_##t##_cursor_ft iterate;			\
  uset_##t##_next_ft next

#define uset_tpl(t)                                             \
  typedef struct _uset_##t uset_##t;                            \
  typedef void (ucall* uset_##t##_cursor_ft)(ucursor* cursor);  \
  typedef t* (ucall* uset_##t##_next_ft)(uset_##t* set,         \
                                         ucursor* cursor);      \
  typedef t* unext_##t;                                         \
  struct _uset_##t{                                             \
    USETHEADER(t);                                              \
  }

#define unext_get(next)                         \
  *(next)

#define unext_has(next)                         \
  next

#define UMAPHEADER(t)							\
  USETHEADER(t);                                                        \
  umap_##t##_put_ft put;						\
  umap_##t##_get_ft get;                                                \
  unsigned char flag

#define UMAP_FLAG_REPLACE 0x01

#define umap_replace_enable(map) \
  (map)->flag |= UMAP_FLAG_REPLACE

#define umap_replace_disable(map) \
  (map)->flag &= !UMAP_FLAG_REPLACE

#define umap_tpl(t)							\
  typedef struct _umap_##t umap_##t;					\
  typedef t (ucall* umap_##t##_key_ft)(t*);				\
  typedef int (ucall* umap_##t##_comp_ft)(t*,t*);			\
  typedef int (ucall* umap_##t##_put_ft)(umap_##t* map,			\
					 unsigned int hscd,		\
					 t* in,				\
					 t** out,			\
					 umap_##t##_key_ft put,		\
					 umap_##t##_comp_ft comp);	\
  typedef int (ucall* umap_##t##_get_ft)(umap_##t* map,			\
					 unsigned int hscd,		\
					 t* in,				\
					 t** out,			\
					 umap_##t##_comp_ft comp);	\
  struct _umap_##t{							\
    UMAPHEADER(t);							\
  }

#define umap_decl_tpl(t)			\
  uset_tpl(t);                                  \
  umap_tpl(t)

#ifndef _UMAP_TPL_INT_
umap_decl_tpl(int);
#endif
#ifndef _UMAP_TPL_SHORT_
umap_decl_tpl(short);
#endif
#ifndef _UMAP_TPL_LONG_
umap_decl_tpl(long);
#endif
#ifndef _UMAP_TPL_FLOAT_
umap_decl_tpl(float);
#endif
#ifndef _UMAP_TPL_DOUBLE_
umap_decl_tpl(double);
#endif
#ifndef _UMAP_TPL_UVOIDP_
umap_decl_tpl(uvoidp);
#endif
#ifndef _UMAP_TPL_UCHARP
umap_decl_tpl(ucharp);
#endif

#endif

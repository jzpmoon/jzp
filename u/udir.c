#include "udir.in.h"
#include "ulist_tpl.c"

#ifndef _ULIST_TPL_UDIR_
ulist_def_tpl(u,udir);
#endif

uapi ustring ustring_root = { "/", 0, 0 };

uapi ustring ustring_parent = { "..", 0, 0 };

uapi ustring ustring_current = { ".", 0, 0 };

uapi udir udir_root_level = {&ustring_root, UNULL, UNULL};

uapi udir udir_parent_level = {&ustring_parent, UNULL, UNULL};

uapi udir udir_current_level = {&ustring_current, UNULL, UNULL};

int ucall udir_comp(udir* dir1,udir* dir2)
{
  if (dir1->name == dir2->name) {
    return 0;
  } else if (dir1->name > dir2->name) {
    return 1;
  } else {
    return -1;
  }
}

UDEFUN(UFNAME udir_init,
       UARGS (uallocator* allocator,udir* dir),
       URET uapi int ucall)
UDECLARE
  ulist_udir* list = ulist_udir_alloc(allocator);
UBEGIN
  if (!list) {
    return -1;
  }
  dir->childs = list;
  return 0;
UEND

UDEFUN(UFNAME udir_relative_path_get,
       UARGS (uallocator* allocator,ulist_udir* from,ulist_udir* to),
       URET uapi ulist_udir* ucall)
UDECLARE
  unext_udir next_from;
  unext_udir next_to;
  ulist_udir* path;
  ucursor c_from;
  ucursor c_to;
  udir dir_from;
  udir dir_to;
  int count = 0;
  int i = 0;
UBEGIN
  if (!from) {
    return to;
  }
  if (!to) {
    return UNULL;
  }
  from->iterate(&c_from);
  to->iterate(&c_to);
  while (1) {
    next_from = from->next((uset_udir*)from,&c_from);
    next_to = to->next((uset_udir*)to,&c_to);
    if (!next_from) {
      break;
    }
    if (!next_to) {
      break;
    }
    dir_from = unext_get(next_from);
    dir_to = unext_get(next_to);
    if (udir_comp(&dir_from,&dir_to)) {
      break;
    }
    count++;
  }
  path = ulist_udir_alloc(allocator);
  if (!path) {
    return UNULL;
  }
  if (next_from) {
    count = from->len - count;
    while (i < count) {
      ulist_udir_append(path,udir_parent_level);
      i++;
    }
  }
  if (next_to) {
    while (1) {
      next_to = to->next((uset_udir*)to,&c_to);
      if (!next_to) {
        break;
      }
      dir_to = unext_get(next_to);
      ulist_udir_append(path,dir_to);
    }
  }
  return path;
UEND

UDEFUN(UFNAME udir_path_add,
       UARGS (udir* top_dir,ulist_udir* path),
       URET uapi int ucall)
UDECLARE
  ucursor c;
  udir* curr_dir;
  udir* temp_dir;
  udir path_dir;
  umap_udir* childs;
  ustring* name;
  int retval;
UBEGIN
  curr_dir = top_dir;
  path->iterate(&c);
  while (1) {
    unext_udir next = path->next((uset_udir*)path,&c);
    if (!next) {
      break;
    }
    path_dir = unext_get(next);
    name = path_dir.name;
    childs = (umap_udir*)curr_dir->childs;
    retval = childs->get(
      childs,
      name->hash_code,
      &path_dir,
      &temp_dir,
      udir_comp);
    if (retval) {
      if (udir_init(top_dir->childs->allocator,&path_dir)) {
        return -1;
      }
      retval = childs->put(
        childs,
        name->hash_code,
        &path_dir,
        &temp_dir,
        UNULL,
        udir_comp);
      if (retval) {
        return -1;
      }
    }
    curr_dir = temp_dir;
  }
  return 0;
UEND

UDEFUN(UFNAME udir_ls2us_by_strtb,
       UARGS (ustring_table* strtb,
              ubuffer* buff,
              ulist_udir* path,
              ustring* sub_path,
              char sep,
              ustringp* out_path),
       URET uapi int ucall)
UDECLARE
  ucursor c;
  udir dir;
  ustring* dir_name;
  ustring* base_path;
  ustring* file_full_path;
UBEGIN
  ubuffer_ready_write(buff);
  path->iterate(&c);
  while (1) {
    unext_udir next = path->next((uset_udir*)path,&c);
    if (!unext_has(next)) {
      break;
    }
    dir = unext_get(next);
    dir_name = dir.name;
    if (ubuffer_write_ustring(buff,dir_name) < dir_name->len) {
      return -2;
    }
    if (ubuffer_write_next(buff,sep)) {
      return -2;
    }
  }
  ubuffer_ready_read(buff);
  ubuffer_pop(buff);
  base_path = ubuffer2ustring(strtb, buff);
  if (!base_path) {
    return -1;
  }
  if (sub_path) {
    file_full_path = ustring_concat_by_strtb(
      strtb,
      base_path,
      sub_path);
  } else {
    file_full_path = base_path;
  }
  *out_path = file_full_path;
  return 0;
UEND

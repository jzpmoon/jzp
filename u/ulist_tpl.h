#ifndef _ULIST_TPL_H_
#define _ULIST_TPL_H_

#include "umacro.h"
#include "uerror.h"
#include "udef.h"
#include "uset.h"
#include "umempool.h"

#define ulist_tpl(t)				\
  typedef struct _ulsnd_##t{			\
    struct _ulsnd_##t* prev;			\
    struct _ulsnd_##t* next;			\
    t value;					\
  } ulsnd_##t;					\
  typedef struct _ulist_##t##_cursor{		\
    ulsnd_##t* next;				\
  } ulist_##t##_cursor;				\
  typedef struct _ulist_##t{			\
    UMAPHEADER(t);                              \
    uallocator* allocator;			\
    int len;					\
    ulsnd_##t* header;				\
  } ulist_##t;					\
  typedef void(*ulist_##t##_dest_ft)(t)

#define ulist_nd_new_decl_tpl(m,t)              \
m##api ulsnd_##t* ucall ulist_##t##_nd_new();

#define ulist_nd_alloc_decl_tpl(m,t)                                    \
m##api ulsnd_##t* ucall ulist_##t##_nd_alloc(uallocator* allocator);

#define ulist_nd_init_decl_tpl(m,t)                     \
m##api void ucall ulist_##t##_nd_init(ulsnd_##t* nd);

#define ulist_init_decl_tpl(m,t)                                        \
m##api void ucall ulist_##t##_init(ulist_##t* list,uallocator* allocator);

#define ulist_new_decl_tpl(m,t)                 \
  m##api ulist_##t* ucall ulist_##t##_new()

#define ulist_alloc_decl_tpl(m,t)               \
  UDECLFUN(UFNAME ulist_##t##_alloc,            \
           UARGS (uallocator* allocator),       \
           URET m##api ulist_##t* ucall);
  
#define ulist_append_decl_tpl(m,t)              \
  UDECLFUN(UFNAME ulist_##t##_append,           \
           UARGS (ulist_##t* list,t value),     \
           URET m##api int ucall)

#define ulist_nd_append_decl_tpl(m,t)                                   \
m##api void ucall ulist_##t##_nd_append(ulist_##t* list,ulsnd_##t* node)

#define ulist_insert_decl_tpl(m,t)                                      \
  m##api int ucall ulist_##t##_insert(ulist_##t* list,ulsnd_##t* nd,t value)

#define ulist_nd_insert_decl_tpl(m,t)                           \
  UDECLFUN(UFNAME ulist_##t##_nd_insert,                        \
           UARGS (ulist_##t* list,ulsnd_##t* nd,ulsnd_##t* af), \
           URET m##api void ucall)

#define ulist_ins_bf_decl_tpl(m,t)                              \
  UDECLFUN(UFNAME ulist_##t##_ins_bf,                           \
           UARGS (ulist_##t* list,ulsnd_##t* nd,t value),       \
           URET m##api int ucall);

#define ulist_nd_ins_bf_decl_tpl(m,t)                           \
  UDECLFUN(UFNAME ulist_##t##_nd_ins_bf,                        \
           UARGS (ulist_##t* list,ulsnd_##t* nd,ulsnd_##t* bf),  \
           URET m##api void ucall);

#define ulist_concat_decl_tpl(m,t)                                      \
  m##api void ucall ulist_##t##_concat(ulist_##t* list1,ulist_##t* list2)

#define ulist_first_get_decl_tpl(m,t)                                   \
  m##api void ucall ulist_##t##_first_get(ulist_##t* list,t** outval)

#define ulist_last_get_decl_tpl(m,t)                                    \
  m##api void ucall ulist_##t##_last_get(ulist_##t* list,t** outval)

#define ulist_dest_decl_tpl(m,t)                                        \
  m##api void ucall ulist_##t##_dest(ulist_##t* list,ulist_##t##_dest_ft del)
  
#define ulist_decl_tpl(m,t)			\
  ulist_tpl(t);					\
  ulist_nd_new_decl_tpl(m,t);                   \
  ulist_nd_alloc_decl_tpl(m,t);                 \
  ulist_nd_init_decl_tpl(m,t);                  \
  ulist_init_decl_tpl(m,t);                     \
  ulist_new_decl_tpl(m,t);			\
  ulist_alloc_decl_tpl(m,t);			\
  ulist_append_decl_tpl(m,t);			\
  ulist_nd_append_decl_tpl(m,t);                \
  ulist_insert_decl_tpl(m,t);                   \
  ulist_nd_insert_decl_tpl(m,t);                \
  ulist_ins_bf_decl_tpl(m,t);                   \
  ulist_nd_ins_bf_decl_tpl(m,t);                \
  ulist_concat_decl_tpl(m,t);			\
  ulist_first_get_decl_tpl(m,t);                \
  ulist_last_get_decl_tpl(m,t);			\
  ulist_dest_decl_tpl(m,t)

#define ulist_first_nd_get(list,first)                                  \
  do {                                                                  \
    first = (list)->header;                                             \
  } while(0)

#define ulist_last_nd_get(list,last)                                    \
  do {                                                                  \
    if ((list)->header) {                                               \
      last = (list)->header->prev;                                      \
    } else {                                                            \
      last = UNULL;                                                     \
    }                                                                   \
  } while(0)
  
#define ulist_nd_first_get(list) \
  ((list)->header)
  
#define ulist_nd_last_get(list) \
  (((list)->header) ? (list)->header->prev : UNULL)

#define ulist_nd_next(ls,nd,nx)                 \
  do {                                          \
    if ((nd)->next != ls->header) {             \
      nx = (nd)->next;                          \
    } else {                                    \
      nx = UNULL;                               \
    }                                           \
  } while(0)
    
#define ulist_nd_prev(ls,nd,nx)                 \
  do {                                          \
    if ((nd)->prev != ls->header->prev) {       \
      nx = (nd)->prev;                          \
    } else {                                    \
      nx = UNULL;                               \
    }                                           \
  } while(0)

#define ulist_nd_value(nd,val)                  \
  do {                                          \
    val = (nd)->value;                          \
  } while(0)


#define ulist_nd_value_set(nd,val)              \
  do {                                          \
    (nd)->value = val;                          \
  } while(0)

#endif

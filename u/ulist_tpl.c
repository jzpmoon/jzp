#include "ualloc.h"
#include "ulist_tpl.h"

#define ulist_cursor_tpl(t)                                     \
  static void ucall ulist_##t##_cursor_init(ucursor* cursor){   \
    ulist_##t##_cursor* c = (ulist_##t##_cursor*)cursor;        \
    c->next = UNULL;                                            \
  }

#define ulist_cursor_next_tpl(t)                                \
  static t* ucall ulist_##t##_cursor_next(uset_##t* set,        \
                                          ucursor* cursor){     \
    ulist_##t* list = (ulist_##t*)set;                          \
    ulist_##t##_cursor* c = (ulist_##t##_cursor*)cursor;        \
    ulsnd_##t* next;                                            \
    if(!list->header){                                          \
      return UNULL;                                             \
    }else if(!c->next){                                         \
      next = list->header;                                      \
    }else if(c->next->next != list->header){                    \
      next = c->next->next;                                     \
    }else{                                                      \
      return UNULL;                                             \
    }                                                           \
    c->next = next;                                             \
    return &next->value;                                        \
  }

#define ulist_map_put_tpl(t)                                            \
  static int ucall ulist_map_##t##_put(umap_##t*           map,         \
                                       unsigned int        hscd,        \
                                       t*                  ink,         \
                                       t**                 outk,        \
                                       umap_##t##_key_ft   putk,        \
                                       umap_##t##_comp_ft  comp)        \
  {                                                                     \
    ulist_##t* list = (ulist_##t*)map;                                  \
    ulsnd_##t* next = list->header;                                     \
    ulsnd_##t* node;                                                    \
    while (next) {                                                      \
      if (!comp(ink,&next->value)) {                                    \
        if (list->flag & UMAP_FLAG_REPLACE) {                           \
          if (putk) {                                                   \
            next->value = putk(ink);                                    \
          } else {                                                      \
            next->value = *ink;                                         \
          }                                                             \
        }                                                               \
        if (outk) {                                                     \
          *outk = &next->value;                                         \
        }                                                               \
        return 1;                                                       \
      }                                                                 \
    }                                                                   \
    node = ulist_##t##_nd_alloc(list->allocator);                       \
    if (!node) {                                                        \
      return -1;                                                        \
    }                                                                   \
    ulist_##t##_nd_append(list,node);                                   \
    if (putk) {                                                         \
      node->value = putk(ink);                                          \
    } else {                                                            \
      node->value = *ink;                                               \
    }                                                                   \
    if (outk) {                                                         \
      *outk = &node->value;                                             \
    }                                                                   \
    return 0;                                                           \
  }

#define ulist_map_get_tpl(t)                                            \
  static int ucall ulist_map_##t##_get(umap_##t*           map,         \
                                       unsigned int        hscd,        \
                                       t*                  ink,         \
                                       t**                 outk,        \
                                       umap_##t##_comp_ft  comp)        \
  {                                                                     \
    ulist_##t* list = (ulist_##t*)map;                                  \
    ulsnd_##t* next = list->header;                                     \
    while (next) {                                                      \
      if (!comp(ink,&next->value)) {                                    \
        *outk = &next->value;                                           \
        return 0;                                                       \
      }                                                                 \
    }                                                                   \
    *outk = UNULL;                                                      \
    return -1;                                                          \
  }

#define ulist_nd_new_tpl(m,t)                           \
  m##api ulsnd_##t* ucall ulist_##t##_nd_new()          \
  {                                                     \
    uallocator* allocator = &u_global_allocator;        \
    return ulist_##t##_nd_alloc(allocator);             \
  }

#define ulist_nd_alloc_tpl(m,t)                                         \
  m##api ulsnd_##t* ucall ulist_##t##_nd_alloc(uallocator* allocator)   \
  {                                                                     \
    ulsnd_##t* nd;                                                      \
    nd = allocator->alloc(allocator,sizeof(ulsnd_##t));                 \
    if (nd) {                                                           \
      ulist_##t##_nd_init(nd);                                          \
    }                                                                   \
    return nd;                                                          \
  }

#define ulist_nd_init_tpl(m,t)                                  \
  m##api void ucall ulist_##t##_nd_init(ulsnd_##t* nd)          \
  {                                                             \
    nd->prev = UNULL;                                           \
    nd->next = UNULL;                                           \
  }

#define ulist_init_tpl(m,t)                                     \
  m##api void ucall ulist_##t##_init(ulist_##t* list,           \
                                       uallocator* allocator)   \
  {                                                             \
    if(list){                                                   \
      list->iterate = ulist_##t##_cursor_init;                  \
      list->next = ulist_##t##_cursor_next;                     \
      list->put = ulist_map_##t##_put;                          \
      list->get = ulist_map_##t##_get;                          \
      list->flag = 0;                                           \
      list->allocator = allocator;                              \
      list->len = 0;                                            \
      list->header = UNULL;                                     \
    }                                                           \
  }

#define ulist_new_tpl(m,t)                                              \
  m##api ulist_##t* ucall ulist_##t##_new(){                            \
    uallocator* allocator = &u_global_allocator;                        \
    return ulist_##t##_alloc(allocator);                                \
  }

#define ulist_alloc_tpl(m,t)                                            \
  m##api ulist_##t* ucall ulist_##t##_alloc(uallocator* allocator){     \
    ulist_##t* list;                                                    \
                                                                        \
    list = allocator->alloc(allocator,sizeof(ulist_##t));               \
    ulist_##t##_init(list,allocator);                                   \
    return list;                                                        \
  }

#define ulist_append_tpl(m,t)                                           \
UDEFUN(UFNAME ulist_##t##_append,                                       \
       UARGS (ulist_##t* list,t value),                                 \
       URET m##api int ucall)                                           \
UDECLARE                                                                \
  ulsnd_##t* node=UNULL;                                                \
UBEGIN                                                                  \
    node = ulist_##t##_nd_alloc(list->allocator);                       \
    if (!node) {                                                        \
      return -1;                                                        \
    }                                                                   \
    ulist_##t##_nd_append(list,node);                                   \
    node->value = value;                                                \
    return 0;                                                           \
UEND

#define ulist_nd_append_tpl(m,t)                                \
  UDEFUN(UFNAME ulist_##t##_nd_append,                          \
         UARGS (ulist_##t* list,                                \
                ulsnd_##t* node),                               \
         URET m##api void ucall)                                \
UDECLARE                                                        \
    ulsnd_##t* header = list->header;                           \
    ulsnd_##t* footer = UNULL;                                  \
UBEGIN                                                          \
    if (header == UNULL) {                                      \
      list->header = header = footer = node;                    \
    } else {                                                    \
      footer = header->prev;                                    \
    }                                                           \
    header->prev = node;                                        \
    footer->next = node;                                        \
    node->prev = footer;                                        \
    node->next = header;                                        \
    list->len++;                                                \
    return;                                                     \
UEND

#define ulist_ins_bf_tpl(m,t)                                           \
UDEFUN(UFNAME ulist_##t##_ins_bf,                                       \
       UARGS (ulist_##t* list,ulsnd_##t* nd,t value),                   \
       URET m##api int ucall)                                           \
UDECLARE                                                                \
  ulsnd_##t* after = ulist_##t##_nd_alloc(list->allocator);             \
UBEGIN                                                                  \
  if (after) {                                                          \
    after->value = value;                                               \
    ulist_##t##_nd_ins_bf(list,nd,after);                               \
    return 0;                                                           \
  } else {                                                              \
    return -1;                                                          \
  }                                                                     \
UEND

#define ulist_nd_ins_bf_tpl(m,t)                                \
  UDEFUN(UFNAME ulist_##t##_nd_ins_bf,                          \
         UARGS (ulist_##t* list,ulsnd_##t* nd,ulsnd_##t* bf),   \
         URET m##api void ucall)                                \
       UDECLARE                                                 \
       ulsnd_##t* prev;                                         \
  UBEGIN                                                        \
  ulist_nd_prev(list,nd,prev);                                  \
  if (!prev) {                                                  \
    prev = list->header;                                        \
    ulist_##t##_nd_insert(list,prev,bf);                        \
    list->header = bf;                                          \
  } else {                                                      \
    ulist_##t##_nd_insert(list,prev,bf);                        \
  }                                                             \
  UEND

#define ulist_insert_tpl(m,t)                                           \
UDEFUN(UFNAME ulist_##t##_insert,                                       \
       UARGS (ulist_##t* list,ulsnd_##t* nd,t value),                   \
       URET m##api int ucall)                                           \
UDECLARE                                                                \
  ulsnd_##t* after = ulist_##t##_nd_alloc(list->allocator);             \
UBEGIN                                                                  \
  if (after) {                                                          \
    after->value = value;                                               \
    ulist_##t##_nd_insert(list,nd,after);                               \
    return 0;                                                           \
  } else {                                                              \
    return -1;                                                          \
  }                                                                     \
UEND

#define ulist_nd_insert_tpl(m,t)                                \
UDEFUN(UFNAME ulist_##t##_nd_insert,                            \
       UARGS (ulist_##t* list,ulsnd_##t* nd,ulsnd_##t* af),     \
       URET m##api void ucall)                                  \
UDECLARE                                                        \
                                                                \
UBEGIN                                                          \
  af->prev = nd;                                                \
  af->next = nd->next;                                          \
  nd->next->prev = af;                                          \
  nd->next = af;                                                \
  list->len++;                                                  \
UEND

#define ulist_concat_tpl(m,t)                                   \
  m##api void ucall ulist_##t##_concat(ulist_##t* list1,        \
                                         ulist_##t* list2){     \
    ulsnd_##t* header1 = list1->header;                         \
    ulsnd_##t* header2 = list2->header;                         \
    ulsnd_##t* footer1;                                         \
    ulsnd_##t* footer2;                                         \
    if(!header1 || !header2){                                   \
      return;                                                   \
    }                                                           \
    footer1 = header1->prev;                                    \
    footer2 = header2->prev;                                    \
    header1->prev = footer2;                                    \
    footer2->next = header1;                                    \
    header2->prev = footer1;                                    \
    footer1->next = header2;                                    \
    list1->len += list2->len;                                   \
    list2->header = UNULL;                                      \
    list2->len = 0;                                             \
  }

#define ulist_first_get_tpl(m,t)                                        \
  m##api void ucall ulist_##t##_first_get(ulist_##t* list,t** outval)   \
  {                                                                     \
    ulsnd_##t* header;                                                  \
    if (list->len > 0) {                                                \
      header = list->header;                                            \
      *outval = &header->value;                                         \
    } else {                                                            \
      *outval = UNULL;                                                  \
    }                                                                   \
  }

#define ulist_last_get_tpl(m,t)                                         \
  m##api void ucall ulist_##t##_last_get(ulist_##t* list,t** outval)    \
  {                                                                     \
    ulsnd_##t* header;                                                  \
    if (list->len > 0) {                                                \
      header = list->header;                                            \
      *outval = &header->prev->value;                                   \
    } else {                                                            \
      *outval = UNULL;                                                  \
    }                                                                   \
  }

#define ulist_dest_tpl(m,t)                                             \
  m##api void ucall ulist_##t##_dest(ulist_##t* list,                   \
                                       ulist_##t##_dest_ft dest){       \
    uallocator* allocator = list->allocator;                            \
    ulsnd_##t* header = list->header;                                   \
    ulsnd_##t* nd = list->header;                                       \
    if (nd) {                                                           \
      do {                                                              \
        ulsnd_##t* temp = nd;                                           \
        nd = nd->next;                                                  \
        if (dest) {                                                     \
          dest(temp->value);                                            \
        }                                                               \
        allocator->free(allocator,temp);                                \
      }        while (nd->next != header);                              \
    }                                                                   \
    allocator->free(allocator,list);                                    \
  }

#define ulist_def_tpl(m,t)                      \
  ulist_cursor_tpl(t);                          \
  ulist_cursor_next_tpl(t);                     \
  ulist_map_put_tpl(t);                         \
  ulist_map_get_tpl(t);                         \
  ulist_nd_new_tpl(m,t);                        \
  ulist_nd_alloc_tpl(m,t);                      \
  ulist_nd_init_tpl(m,t);                       \
  ulist_init_tpl(m,t);                          \
  ulist_new_tpl(m,t);                           \
  ulist_alloc_tpl(m,t);                         \
  ulist_append_tpl(m,t);                        \
  ulist_nd_append_tpl(m,t);                     \
  ulist_ins_bf_tpl(m,t);                        \
  ulist_nd_ins_bf_tpl(m,t);                     \
  ulist_insert_tpl(m,t);                        \
  ulist_nd_insert_tpl(m,t);                     \
  ulist_concat_tpl(m,t);                        \
  ulist_first_get_tpl(m,t);                     \
  ulist_last_get_tpl(m,t);                      \
  ulist_dest_tpl(m,t)

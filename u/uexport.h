#ifndef _UEXPORT_H_
#define _UEXPORT_H_

/* replace _EXPORT_XXX_ to library name */

#if UOS == WIN
  #ifdef _EXPORT_XXX_
    #define xapi __declspec(dllexport)
  #elif _NOTEXPORT_XXX_
    #define xapi
  #else
    #define xapi __declspec(dllimport)
  #endif

  #define xcall __stdcall
#else
  #define xapi
  #define xcall
#endif

#endif

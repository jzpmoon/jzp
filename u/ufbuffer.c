#include "ufbuffer.h"
#include "ulist_tpl.c"

#ifndef _ULIST_TPL_UBUFFER_ROWP_
ulist_def_tpl(u,ufbuffer_rowp);
#endif
#ifndef _ULIST_TPL_UBUFFERP_
ulist_def_tpl(u,ubufferp);
#endif

UDEFUN(UFNAME ufbuffer_new,
       UARGS (char* file_path,
              int buf_size_max,
              int row_buf_len_max,
              int col_buf_len_max),
       URET uapi ufbuffer* ucall)
UDECLARE
UBEGIN
  return ufbuffer_alloc(
    &u_global_allocator,
    file_path,
    buf_size_max,
    row_buf_len_max,
    col_buf_len_max);
UEND

UDEFUN(UFNAME ufbuffer_des,
       UARGS (ufbuffer* buf),
       URET uapi void ucall)
UDECLARE
  uallocator* allocator;
UBEGIN
  allocator = buf->allocator;
  allocator->clean(allocator);
UEND

UDEFUN(UFNAME ufbuffer_alloc,
       UARGS (uallocator* alloc,
              char* file_path,
              int buf_size_max,
              int row_buf_len_max,
              int col_buf_len_max),
      URET uapi ufbuffer* ucall)
UDECLARE
  ufbuffer* buf = UNULL;
  ustream* stream = UNULL;
  ulist_ufbuffer_rowp* buf_list = UNULL;
UBEGIN
  buf = alloc->alloc(alloc,sizeof(ufbuffer));
  if (buf) {
    buf->allocator = alloc;
    stream = ustream_alloc(alloc,USTREAM_INPUT,USTREAM_FILE,!USTREAM_READ_LINE);
    if (!stream) {
      goto err;
    }
    buf->stream = stream;
    buf->file_path = file_path;
    buf_list = ulist_ufbuffer_rowp_alloc(alloc);
    if (!buf_list) {
      goto err;
    }
    buf->buf_list = buf_list;
    buf->curr_row = UNULL;
    buf->curr_col = UNULL;
    buf->buf_size_max = buf_size_max;
    buf->row_buf_len_max = row_buf_len_max;
    buf->col_buf_len_max = col_buf_len_max;
    buf->row_pos = 0;
    buf->col_pos = 0;
    buf->buf_col_pos = 0;
    buf->row_count = 0;
  }
  return buf;
  err:
  alloc->free(alloc,buf);
  alloc->free(alloc,stream);
  alloc->free(alloc,buf_list);
  return UNULL;
UEND

UDEFUN(UFNAME ufbuffer_row_alloc,
       UARGS (ufbuffer* buf),
       URET uapi ufbuffer_row* ucall)
UDECLARE
  uallocator* allocator;
  ufbuffer_row* row;
  ulist_ubufferp* row_list;
  ubuffer* col_buf;
UBEGIN
  allocator = buf->allocator;
  row = allocator->alloc(allocator,sizeof(ufbuffer_row));
  if (!row) {
    uabort("ufbuffer_row alloc error!");
  }
  row->flag = 0;
  row->col_count = 0;
  row_list = ulist_ubufferp_alloc(allocator);
  if (!row_list) {
    uabort("ulist alloc error");
  }
  row->row_list = row_list;
  col_buf = ubuffer_alloc(allocator,buf->buf_size_max);
  if (!col_buf) {
    uabort("ubuffer alloc error");
  }
  ulist_ubufferp_append(row_list,col_buf);
  return row;
UEND

UDEFUN(UFNAME ufbuffer_row_load,
       UARGS (ufbuffer* buf,
              ufbuffer_row* row,
              int retval),
       URET uapi int ucall)
UDECLARE
  URI_DEFINE;
  ulist_ubufferp* row_list;
  ubuffer* col_buf = UNULL;
  ucursor c;
  usize_t count = 0;
UBEGIN
  row_list = row->row_list;
  row_list->iterate(&c);
  while (1) {
    unext_ubufferp next = row_list->next((uset_ubufferp*)row_list, &c);
    if (!next) {
      break;
    }
    col_buf = unext_get(next);
    if (retval == 1) {
      retval = ustream_read_line_to_buff(buf->stream,col_buf,URI_REF);
      URI_ERROR;
        URI_CASE(UERR_EOF);
          continue;
        URI_ELSE
          uabort(URI_DESC);
        URI_END;
      URI_END;
      count += ubuffer_limit_get(col_buf);
    } else {
      ubuffer_empty(col_buf);
    }
  }
  while (1) {
    if (retval == 0 || retval == -1) {
      break;
    }
    if (retval == 1) {
      col_buf = ubuffer_alloc(buf->allocator,buf->buf_size_max);
      if (!col_buf) {
        uabort("ubuffer alloc error");
      }
      if (ulist_ubufferp_append(row_list,col_buf)) {
        uabort("ulist append error");
      }
    }
    retval = ustream_read_line_to_buff(buf->stream,col_buf,URI_REF);
    URI_ERROR;
      URI_CASE(UERR_EOF);
        continue;
      URI_ELSE
        uabort(URI_DESC);
      URI_END;
    URI_END;
    count += ubuffer_limit_get(col_buf);
  }
  row->col_count = count;
  return retval;
UEND

UDEFUN(UFNAME ufbuffer_load,
       UARGS (ufbuffer* buf),
       URET uapi void ucall)
UDECLARE
  ulist_ufbuffer_rowp* buf_list;
  ufbuffer_row* row;
  ulsnd_ufbuffer_rowp* lsnd;
  ucursor c;
  int retval = 1;
UBEGIN
  ustream_close(buf->stream);
  ustream_iot_change(buf->stream,USTREAM_INPUT);
  if (ustream_open_by_path(buf->stream,buf->file_path)) {
    uabort("open file error:%d!",ustdc_errno);
  }
  ustream_seek(buf->stream,0,USEEK_SET);
  buf_list = buf->buf_list;
  buf_list->iterate(&c);
  while (1) {
    unext_ufbuffer_rowp next =
      buf_list->next((uset_ufbuffer_rowp*)buf_list, &c);
    if (!next) {
      break;
    }
    row = unext_get(next);
    retval = ufbuffer_row_load(buf,row,retval);
    if (retval == 0) {
      retval = 1;
    }
  }
  while (1) {
    if (retval == -1) {
      break;
    }
    row = ufbuffer_row_alloc(buf);
    if (!row) {
      uabort("ufbuffer_row_alloc error");
    }
    ulist_ufbuffer_rowp_append(buf_list,row);
    buf->row_count++;
    retval = ufbuffer_row_load(buf,row,1);
  }
  ulist_first_nd_get(buf_list,lsnd);
  buf->curr_row = lsnd;
UEND

UDEFUN(UFNAME ufbuffer_store,
       UARGS (ufbuffer* buf),
       URET uapi int ucall)
UDECLARE
  URI_DEFINE;
  ulist_ufbuffer_rowp* buf_list;
  ufbuffer_row* row;
  ulist_ubufferp* row_list;
  ubuffer* col_buf;
  ucursor c;
  ucursor cc;
  int count = 0;
  int flag = 0;
UBEGIN
  ustream_close(buf->stream);
  ustream_iot_change(buf->stream,USTREAM_OUTPUT);
  if (ustream_open_by_path(buf->stream,buf->file_path)) {
    uabort("open file error!");
  }
  ustream_seek(buf->stream,0,USEEK_SET);
  buf_list = buf->buf_list;
  buf_list->iterate(&c);
  while (1) {
    unext_ufbuffer_rowp next =
      buf_list->next((uset_ufbuffer_rowp*)buf_list, &c);
    if (!next) {
      break;
    }
    row = unext_get(next);
    if (ufbuffer_row_flag_of(row,UFBUFFER_ROW_FLAG_RM)) {
      continue;
    }
    if (flag) {
      ustream_write_char(buf->stream,'\n',URI_REF);
      URI_ERROR;
        uabort(URI_DESC);
      URI_END;
    } else {
      flag = 1;
    }
    row_list = row->row_list;
    row_list->iterate(&cc);
    while (1) {
      unext_ubufferp next = row_list->next((uset_ubufferp*)row_list, &cc);
      if (!next) {
        break;
      }
      col_buf = unext_get(next);
      count = ustream_write_buff(buf->stream,col_buf);
      if (count < 0) {
        uabort("ufbuffer write error!");
      }
    }
  }
  return count;
UEND

UDEFUN(UFNAME ufbuffer_println,
       UARGS (ufbuffer* buf),
       URET uapi void ucall)
UDECLARE
  ulist_ufbuffer_rowp* buf_list;
  ufbuffer_row* row;
  ulist_ubufferp* row_list;
  ucursor c;
  ucursor cc;
UBEGIN
  buf_list = buf->buf_list;
  buf_list->iterate(&c);
  while (1) {
    unext_ufbuffer_rowp next =
      buf_list->next((uset_ufbuffer_rowp*)buf_list, &c);
    if (!next) {
      break;
    }
    row = unext_get(next);
    if (ufbuffer_row_flag_of(row,UFBUFFER_ROW_FLAG_RM)) {
      continue;
    }
    row_list = row->row_list;
    row_list->iterate(&cc);
    while (1) {
      unext_ubufferp next = row_list->next((uset_ubufferp*)row_list, &cc);
      ubuffer* col_buf;
      if (!next) {
        break;
      }
      col_buf = unext_get(next);
      ubuffer_print(col_buf);
    }
    ustdc_fprintf(ustdc_stdout,"\n");
  }
UEND

UDEFUN(UFNAME ufbuffer_row_println,
       UARGS (ufbuffer* buf),
       URET uapi void ucall)
UDECLARE
  ulsnd_ufbuffer_rowp* curr_row;
  ufbuffer_row* row;
  ulist_ubufferp* row_list;
  ucursor c;
UBEGIN
  curr_row = buf->curr_row;
  if (!curr_row) {
    return;
  }
  ulist_nd_value(curr_row,row);
  if (ufbuffer_row_flag_of(row,UFBUFFER_ROW_FLAG_RM)) {
    return;
  }
  row_list = row->row_list;
  row_list->iterate(&c);
  while (1) {
    unext_ubufferp next = row_list->next((uset_ubufferp*)row_list, &c);
    ubuffer* col_buf;
    if (!next) {
      break;
    }
    col_buf = unext_get(next);
    ubuffer_print(col_buf);
  }
  ustdc_fprintf(ustdc_stdout,"\n");
UEND

UDEFUN(UFNAME ufbuffer_row_next,
       UARGS (ufbuffer* buf),
       URET uapi int ucall)
UDECLARE
  ulsnd_ufbuffer_rowp* curr_row;
  ulsnd_ufbuffer_rowp* next_row;
  ufbuffer_row* row;
UBEGIN
  curr_row = buf->curr_row;
  if (!curr_row) {
    return 0;
  }
  next_row = curr_row;
  while (1) {
    ulist_nd_next(buf->buf_list,next_row,next_row);
    if (!next_row) {
      break;
    }
    ulist_nd_value(next_row,row);
    if (!ufbuffer_row_flag_of(row,UFBUFFER_ROW_FLAG_RM)) {
      buf->curr_row = next_row;
      buf->row_pos++;
      ufbuffer_col_reset(buf);
      return 1;
    }
  }
  return 0;
UEND

UDEFUN(UFNAME ufbuffer_row_prev,
       UARGS (ufbuffer* buf),
       URET uapi int ucall)
UDECLARE
  ulsnd_ufbuffer_rowp* curr_row;
  ulsnd_ufbuffer_rowp* prev_row;
  ufbuffer_row* row;
UBEGIN
  curr_row = buf->curr_row;
  if (!curr_row) {
    return 0;
  }
  prev_row = curr_row;
  while (1) {
    ulist_nd_prev(buf->buf_list,prev_row,prev_row);
    if (!prev_row) {
      break;
    }
    ulist_nd_value(prev_row,row);
    if (!ufbuffer_row_flag_of(row,UFBUFFER_ROW_FLAG_RM)) {
      buf->curr_row = prev_row;
      buf->row_pos--;
      ufbuffer_col_reset(buf);
      return 1;
    }
  }
  return 0;
UEND

UDEFUN(UFNAME ufbuffer_row_goto,
       UARGS (ufbuffer* buf,
              unsigned int row_pos),
       URET uapi void ucall)
UDECLARE
  unsigned int i;
  unsigned int offset;
UBEGIN
  if (buf->row_pos > row_pos) {
    offset = buf->row_pos - row_pos;
    for (i = 0; i < offset; i++) {
      ufbuffer_row_prev(buf);
    }
  } else {
    offset = row_pos - buf->row_pos;
    for (i = 0; i < offset; i++) {
      ufbuffer_row_next(buf);
    }
  }
UEND

UDEFUN(UFNAME ufbuffer_row_goto_begin,
       UARGS (ufbuffer* buf),
       URET uapi void ucall)
UDECLARE
  ulsnd_ufbuffer_rowp* first_row;
  ufbuffer_row* row;
  ulist_ubufferp* row_list;
  ulsnd_ubufferp* first_col;
UBEGIN
  ulist_first_nd_get(buf->buf_list,first_row);
  while (1) {
    if (!first_row) {
      return;
    }
    ulist_nd_value(first_row,row);
    if (!ufbuffer_row_flag_of(row,UFBUFFER_ROW_FLAG_RM)) {
      break;
    }
    ulist_nd_next(buf->buf_list,first_row,first_row);
  }
  row_list = row->row_list;
  ulist_first_nd_get(row_list,first_col);
  buf->curr_row = first_row;
  buf->curr_col = first_col;
  buf->row_pos = 0;
  buf->col_pos = 0;
  buf->buf_col_pos = 0;
UEND

UDEFUN(UFNAME ufbuffer_row_goto_end,
       UARGS (ufbuffer* buf),
       URET uapi void ucall)
UDECLARE
  ulsnd_ufbuffer_rowp* last_row;
  ufbuffer_row* row;
  ulist_ubufferp* row_list;
  ulsnd_ubufferp* first_col;
UBEGIN
  last_row = ulist_nd_last_get(buf->buf_list);
  while (1) {
    if (!last_row) {
      return;
    }
    ulist_nd_value(last_row,row);
    if (!ufbuffer_row_flag_of(row,UFBUFFER_ROW_FLAG_RM)) {
      break;
    }
    ulist_nd_prev(buf->buf_list,last_row,last_row);
  }
  row_list = row->row_list;
  first_col = ulist_nd_first_get(row_list);
  buf->curr_row = last_row;
  buf->curr_col = first_col;
  buf->row_pos = buf->row_count - 1;
  buf->col_pos = 0;
  buf->buf_col_pos = 0;
UEND

UDEFUN(UFNAME ufbuffer_col_next,
       UARGS (ufbuffer* buf),
       URET uapi void ucall)
UDECLARE
  ulsnd_ufbuffer_rowp* curr_row;
  ufbuffer_row* row;
  ulist_ubufferp* row_list;
  ulsnd_ubufferp* curr_col;
  ulsnd_ubufferp* next_col;
  ubuffer* col_buf;
  ubuffer* next_col_buf;
UBEGIN
  curr_row = buf->curr_row;
  if (!curr_row) {
    return;
  }
  ulist_nd_value(curr_row,row);
  row_list = row->row_list;
  curr_col = ufbuffer_curr_col_get(buf);
  ulist_nd_value(curr_col,col_buf);
  if (buf->buf_col_pos < col_buf->limit) {
    buf->buf_col_pos++;
    buf->col_pos++;
  } else {
    next_col = curr_col;
    while (1) {
      ulist_nd_next(row_list,next_col,next_col);
      if (!next_col) {
        break;
      }
      ulist_nd_value(next_col,next_col_buf);
      if (next_col_buf->limit > 0) {
        buf->buf_col_pos = 0;
        buf->col_pos++;
        buf->curr_col = next_col;
        return;
      }
    }
  }
UEND

UDEFUN(UFNAME ufbuffer_col_prev,
       UARGS (ufbuffer* buf),
       URET uapi void ucall)
UDECLARE
  ulsnd_ufbuffer_rowp* curr_row;
  ufbuffer_row* row;
  ulist_ubufferp* row_list;
  ulsnd_ubufferp* curr_col;
  ulsnd_ubufferp* next_col;
  ubuffer* next_col_buf;
UBEGIN
  curr_row = buf->curr_row;
  if (!curr_row) {
    return;
  }
  ulist_nd_value(curr_row,row);
  row_list = row->row_list;
  curr_col = ufbuffer_curr_col_get(buf);
  if (buf->buf_col_pos > 0) {
    buf->buf_col_pos--;
    buf->col_pos--;
  } else {
    next_col = curr_col;
    while (1) {
      ulist_nd_prev(row_list,next_col,next_col);
      if (!next_col) {
        break;
      }
      ulist_nd_value(next_col,next_col_buf);
      if (next_col_buf->limit > 0) {
        buf->buf_col_pos = next_col_buf->limit - 1;
        buf->col_pos--;
        buf->curr_col = next_col;
        return;
      }
    }
  }
UEND

UDEFUN(UFNAME ufbuffer_col_goto,
       UARGS (ufbuffer* buf,
              unsigned int col_pos),
       URET uapi void ucall)
UDECLARE
  unsigned int i;
  unsigned int offset;
UBEGIN
  if (buf->col_pos > col_pos) {
    offset = buf->col_pos - col_pos;
    for (i = 0; i < offset; i++) {
      ufbuffer_col_prev(buf);
    }
  } else {
    offset = col_pos - buf->col_pos;
    for (i = 0; i < offset; i++) {
      ufbuffer_col_next(buf);
    }
  }
UEND

UDEFUN(UFNAME ufbuffer_col_goto_begin,
       UARGS (ufbuffer* buf),
       URET uapi void ucall)
UDECLARE
  ulsnd_ufbuffer_rowp* curr_row;
  ufbuffer_row* row;
  ulist_ubufferp* row_list;
  ulsnd_ubufferp* first_col;
UBEGIN
  curr_row = buf->curr_row;
  if (!curr_row) {
    return;
  }
  ulist_nd_value(curr_row,row);
  row_list = row->row_list;
  first_col = ulist_nd_first_get(row_list);
  buf->curr_col = first_col;
  buf->col_pos = 0;
  buf->buf_col_pos = 0;
UEND

UDEFUN(UFNAME ufbuffer_col_goto_end,
       UARGS (ufbuffer* buf),
       URET uapi void ucall)
UDECLARE
  ulsnd_ufbuffer_rowp* curr_row;
  ufbuffer_row* row;
  ulist_ubufferp* row_list;
  ulsnd_ubufferp* last_col;
  ubuffer* last_col_buf;
UBEGIN
  curr_row = buf->curr_row;
  if (!curr_row) {
    return;
  }
  ulist_nd_value(curr_row,row);
  row_list = row->row_list;
  last_col = ulist_nd_last_get(row_list);
  ulist_nd_value(last_col,last_col_buf);
  buf->curr_col = last_col;
  buf->col_pos = row->col_count;
  buf->buf_col_pos = ubuffer_limit_get(last_col_buf);
UEND

UDEFUN(UFNAME ufbuffer_col_reset,
       UARGS (ufbuffer* buf),
       URET uapi void ucall)
UDECLARE

UBEGIN
  buf->curr_col = UNULL;
  buf->col_pos = 0;
  buf->buf_col_pos = 0;
UEND

UDEFUN(UFNAME ufbuffer_row_rm_next_get,
       UARGS (ufbuffer* buf),
       URET uapi ulsnd_ufbuffer_rowp* ucall)
UDECLARE
  ulsnd_ufbuffer_rowp* curr_row;
  ulsnd_ufbuffer_rowp* next;
  ufbuffer_row* row;
UBEGIN
  curr_row = buf->curr_row;
  if (curr_row) {
    ulist_nd_next(buf->buf_list,curr_row,next);
    if (next) {
      ulist_nd_value(next,row);
      if (ufbuffer_row_flag_of(row,UFBUFFER_ROW_FLAG_RM)) {
        ufbuffer_row_flag_disable(row,UFBUFFER_ROW_FLAG_RM);
        return next;
      }
    }
  }
  return UNULL;
UEND

UDEFUN(UFNAME ufbuffer_row_rm_prev_get,
       UARGS (ufbuffer* buf),
       URET uapi ulsnd_ufbuffer_rowp* ucall)
UDECLARE
  ulsnd_ufbuffer_rowp* curr_row;
  ulsnd_ufbuffer_rowp* prev;
  ufbuffer_row* row;
UBEGIN
  curr_row = buf->curr_row;
  if (curr_row) {
    ulist_nd_prev(buf->buf_list,curr_row,prev);
    if (prev) {
      ulist_nd_value(prev,row);
      if (ufbuffer_row_flag_of(row,UFBUFFER_ROW_FLAG_RM)) {
        ufbuffer_row_flag_disable(row,UFBUFFER_ROW_FLAG_RM);
        return prev;
      }
    }
  }
  return UNULL;
UEND

UDEFUN(UFNAME ufbuffer_row_append,
       UARGS (ufbuffer* buf),
       URET uapi void ucall)
UDECLARE
  ulist_ufbuffer_rowp* buf_list;
  ufbuffer_row* row;
UBEGIN
  buf_list = buf->buf_list;
  row = ufbuffer_row_alloc(buf);
  if (!row) {
    uabort("ufbuffer_row_alloc error");
  }
  ulist_ufbuffer_rowp_append(buf_list,row);
  buf->row_count++;
  if (!buf->curr_row) {
    buf->curr_row = ulist_nd_last_get(buf_list);
    ufbuffer_col_reset(buf);
  }
UEND

UDEFUN(UFNAME ufbuffer_row_insert,
       UARGS (ufbuffer* buf),
       URET uapi void ucall)
UDECLARE
  ulist_ufbuffer_rowp* buf_list;
  ulsnd_ufbuffer_rowp* row;
  ulsnd_ufbuffer_rowp* curr_row;
  ufbuffer_row* new_row;
UBEGIN
  buf_list = buf->buf_list;
  row = ufbuffer_row_rm_prev_get(buf);
  if (!row) {
    curr_row = ufbuffer_curr_row_get(buf);
    if (curr_row) {
      new_row = ufbuffer_row_alloc(buf);
      if (!new_row) {
        uabort("ufbuffer_row_alloc error");
      }
      ulist_ufbuffer_rowp_ins_bf(buf_list,curr_row,new_row);
      ulist_nd_prev(buf_list,curr_row,curr_row);
      buf->curr_row = curr_row;
      buf->row_count++;
      ufbuffer_col_reset(buf);
    } else {
      ufbuffer_row_append(buf);
    }
  } else {
    buf->curr_row = row;
    buf->row_count++;
    ufbuffer_col_reset(buf);
  }
UEND

UDEFUN(UFNAME ufbuffer_row_remove,
       UARGS (ufbuffer* buf),
       URET uapi void ucall)
UDECLARE
  ulsnd_ufbuffer_rowp* curr_row;
  ufbuffer_row* row;
  ulist_ubufferp* row_list;
  ucursor c;
  ubuffer* col_buf;
UBEGIN
  curr_row = buf->curr_row;
  if (curr_row) {
    ulist_nd_value(curr_row,row);
    ufbuffer_row_flag_enable(row,UFBUFFER_ROW_FLAG_RM);
    row->col_count = 0;
    row_list = row->row_list;
    row_list->iterate(&c);
    while (1) {
      unext_ubufferp next = row_list->next((uset_ubufferp*)row_list, &c);
      if (!next) {
        break;
      }
      col_buf = unext_get(next);
      ubuffer_empty(col_buf);
    }
    buf->row_count--;
    ufbuffer_col_reset(buf);
    if (ufbuffer_row_next(buf) == 0) {
      ufbuffer_row_prev(buf);
    } else {
      buf->row_pos--;
    }
  }
UEND

UDEFUN(UFNAME ufbuffer_col_buf_insert,
       UARGS (ufbuffer* buf),
       URET uapi void ucall)
UDECLARE
  ulsnd_ufbuffer_rowp* curr_row;
  ufbuffer_row* row;
  ulist_ubufferp* row_list;
  ulsnd_ubufferp* curr_col;
  ubuffer* col_buf;
UBEGIN
  curr_row = buf->curr_row;
  if (!curr_row) {
    return;
  }
  ulist_nd_value(curr_row,row);
  row_list = row->row_list;
  curr_col = ufbuffer_curr_col_get(buf);
  
  col_buf = ubuffer_alloc(buf->allocator,buf->buf_size_max);
  if (!col_buf) {
    uabort("ubuffer alloc error");
  }
  if (ulist_ubufferp_insert(row_list,curr_col,col_buf)) {
    uabort("ulist insert error!");
  }
UEND

UDEFUN(UFNAME ufbuffer_col_char_insert,
       UARGS (ufbuffer* buf,
              int c),
       URET uapi void ucall)
UDECLARE
  ulsnd_ufbuffer_rowp* curr_row;
  ufbuffer_row* row;
  ulist_ubufferp* row_list;
  ulsnd_ubufferp* curr_col;
  ulsnd_ubufferp* next_col = UNULL;
  ubuffer* col_buf;
  ubuffer* next_col_buf;
  int pop;
UBEGIN
  curr_row = ufbuffer_curr_row_get(buf);
  ulist_nd_value(curr_row,row);
  if (ufbuffer_row_flag_of(row,UFBUFFER_ROW_FLAG_RM)) {
    ufbuffer_row_flag_disable(row,UFBUFFER_ROW_FLAG_RM);
  }
  row_list = row->row_list;
  curr_col = ufbuffer_curr_col_get(buf);
  ulist_nd_value(curr_col,col_buf);
  if (ubuffer_is_full(col_buf)) {
    ulist_nd_next(row_list,curr_col,next_col);
    if (!next_col) {
      ufbuffer_col_buf_insert(buf);
      ulist_nd_next(row_list,curr_col,next_col);
      ulist_nd_value(next_col,next_col_buf);
    } else {
      ulist_nd_value(next_col,next_col_buf);
      if (ubuffer_is_full(next_col_buf)) {
        ufbuffer_col_buf_insert(buf);
        ulist_nd_next(row_list,curr_col,next_col);
        ulist_nd_value(next_col,next_col_buf);
      }
    }
    pop = ubuffer_pop(col_buf);
    ubuffer_insert_next(next_col_buf,0,pop);
  }
  ubuffer_insert_next(col_buf,buf->buf_col_pos,c);
  buf->col_pos++;
  buf->buf_col_pos++;
  row->col_count++;
  if (buf->buf_col_pos == col_buf->size) {
    buf->buf_col_pos = 0;
    if (!next_col) {
      ufbuffer_col_buf_insert(buf);
      ulist_nd_next(row_list,curr_col,next_col);
    }
    buf->curr_col = next_col;
  }
UEND

UDEFUN(UFNAME ufbuffer_col_char_remove,
       UARGS (ufbuffer* buf),
       URET uapi void ucall)
UDECLARE
  ulsnd_ufbuffer_rowp* curr_row;
  ufbuffer_row* row;
  ulsnd_ubufferp* curr_col;
  ubuffer* col_buf;
UBEGIN
  curr_row = buf->curr_row;
  if (!curr_row) {
    return;
  }
  curr_col = ufbuffer_curr_col_get(buf);
  ulist_nd_value(curr_col,col_buf);
  if (col_buf) {
      ubuffer_remove_next(col_buf,buf->buf_col_pos);
    if (buf->buf_col_pos > 0 &&
        buf->buf_col_pos >= col_buf->limit) {
      buf->buf_col_pos--;
    }
    if (buf->col_pos > 0) {
      buf->col_pos--;
      ulist_nd_value(curr_row,row);
      row->col_count--;
    }
  }
UEND

UDEFUN(UFNAME ufbuffer_curr_row_get,
       UARGS (ufbuffer* buf),
       URET uapi ulsnd_ufbuffer_rowp* ucall)
UDECLARE
  ulsnd_ufbuffer_rowp* curr_row;
  ufbuffer_row* row;
UBEGIN
  curr_row = buf->curr_row;
  if (!curr_row) {
    row = ufbuffer_row_alloc(buf);
    ulist_ufbuffer_rowp_append(buf->buf_list,row);
    curr_row = ulist_nd_first_get(buf->buf_list);
    buf->curr_row = curr_row;
    buf->row_count++;
  }
  return curr_row;
UEND

UDEFUN(UFNAME ufbuffer_curr_col_get,
       UARGS (ufbuffer* buf),
       URET uapi ulsnd_ubufferp* ucall)
UDECLARE
  ulsnd_ufbuffer_rowp* curr_row;
  ufbuffer_row* row;
  ulist_ubufferp* row_list;
  ulsnd_ubufferp* curr_col;
  ubuffer* col_buf;
UBEGIN
  curr_col = buf->curr_col;
  if (!curr_col) {
    curr_row = ufbuffer_curr_row_get(buf);
    ulist_nd_value(curr_row,row);
    row_list = row->row_list;
    ulist_first_nd_get(row_list,curr_col);
    if (!curr_col) {
      col_buf = ubuffer_alloc(buf->allocator,buf->buf_size_max);
      if (!col_buf) {
        uabort("ubuffer alloc error");
      }
      if (ulist_ubufferp_append(row_list,col_buf)) {
        uabort("ulist append error");
      }
      ulist_first_nd_get(row_list,curr_col);
    }
  }
  buf->curr_col = curr_col;
  return curr_col;
UEND

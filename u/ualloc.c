#include "ualloc.h"

uapi void* ucall ualloc(usize_t size)
{
  return ustdc_malloc(size);
}

uapi void ucall ufree(void* ptr)
{
  ustdc_free(ptr);
}

static void* ucall default_alloc(uallocator* allocator, usize_t size)
{
  return ustdc_malloc(size);
}

static void* ucall default_allocx(uallocator* allocator,char* data,
  usize_t size)
{
  return ustdc_malloc(size);
}

static void ucall default_free(uallocator* allocator,void* ptr)
{
  ustdc_free(ptr);
}

static void ucall default_clean(uallocator* allocator)
{}

uapi uallocator u_global_allocator = {default_alloc,default_allocx,default_free,
  default_clean};

#include "udef.h"
#include "uhstb_tpl.c"
#include "uhstb.h"

#ifndef _UHSTB_TPL_INT_
uhstb_def_tpl(u,int);
#endif
#ifndef _UHSTB_TPL_SHORT_
uhstb_def_tpl(u,short);
#endif
#ifndef _UHSTB_TPL_LONG_
uhstb_def_tpl(u,long);
#endif
#ifndef _UHSTB_TPL_FLOAT_
uhstb_def_tpl(u,float);
#endif
#ifndef _HSTB_TPL_DOUBLE_
uhstb_def_tpl(u,double);
#endif
#ifndef _HSTB_TPL_UVOIDP_
uhstb_def_tpl(u,uvoidp);
#endif

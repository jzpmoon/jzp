#ifndef _UDGRAPH_H_
#define _UDGRAPH_H_

#define UDGNODEHEADER \
  ulist_udgedgep* in_degree; \
  ulist_udgedgep* out_degree

#define UDGEDGEHEADER \
  ulist_udgedgep* in_degree; \
  ulist_udgedgep* out_degree

struct _udgnode{
  UDGEDGEHEADER;
};
struct _udgedge{
  UDGNODEHEADER;
};

#endif
#ifndef _UBUFFER_H_
#define _UBUFFER_H_

#include "uerror.h"
#include "ualloc.h"
#include "uref.h"
#include "ustring.h"
#include "ustring_table.h"

typedef struct _ubuffer{
  uallocator* allocator;
  usize_t pos;
  usize_t limit;
  usize_t size;
  char data[1];
} ubuffer,*ubufferp;

uapi ubuffer* ucall ubuffer_new(int size);

uapi ubuffer* ucall ubuffer_alloc(uallocator* allocator,int size);

uapi void ucall ubuffer_dest(ubuffer* buff);

uapi int ucall ubuffer_read_from_buff
(ubuffer* to_buff, ubuffer* from_buff);

uapi int ucall ubuffer_read_from_file
(ubuffer* to_buff, USTDC_FILE* from_file);

UDECLFUN(UFNAME ubuffer_write_to_file,
         UARGS (ubuffer* from_buff, USTDC_FILE* to_file),
         URET uapi int ucall);

UDECLFUN(UFNAME ubuffer_read_line_from_file,
         UARGS (ubuffer* to_buff, USTDC_FILE* from_file),
         URET uapi int ucall);

UDECLFUN(UFNAME ubuffer_read_from_stdin,
         UARGS (ubuffer* to_buff, USTDC_FILE* from_stdin),
         URET uapi int ucall);

uapi int ucall ubuffer_read_next(ubuffer* buff);

UDECLFUN(UFNAME ubuffer_read_at,
         UARGS (ubuffer* buff,usize_t pos),
         URET uapi int ucall);

UDECLFUN(UFNAME ubuffer_insert_next,
         UARGS (ubuffer* buff,usize_t pos,int byte),
         URET uapi void ucall);

UDECLFUN(UFNAME ubuffer_remove_next,
         UARGS (ubuffer* buff,usize_t pos),
         URET uapi void ucall);

UDECLFUN(UFNAME ubuffer_pop,
         UARGS (ubuffer* buff),
         URET uapi int ucall);

uapi int ucall ubuffer_look_ahead(ubuffer* buff);

uapi int ucall ubuffer_write_next(ubuffer* buff,int byte);

uapi int ucall ubuffer_write_int2ascii(ubuffer* buff,int inte);

UDECLFUN(UFNAME ubuffer_write_ustring,
         UARGS (ubuffer* buff,ustring* str),
         URET uapi int ucall);

UDECLFUN(UFNAME ubuffer2ustring,
         UARGS (ustring_table* strtb,
                ubuffer* buff),
         URET uapi ustring* ucall);

UDECLFUN(UFNAME ubuffer_print,
         UARGS (ubuffer* buff),
         URET uapi void ucall);

#define USEEK_SET USTDC_SEEK_SET

#define USEEK_CUR USTDC_SEEK_CUR

#define USEEK_END USTDC_SEEK_END

uapi int ucall ubuffer_seek(ubuffer* buff,long offset,int origin);

#define ubuffer_ready_read(buff)		\
  do{						\
    (buff)->limit = (buff)->pos;		\
    (buff)->pos   = 0;				\
  }while(0)

#define ubuffer_ready_write(buff)		\
  do{						\
    (buff)->limit = (buff)->size;		\
    (buff)->pos   = 0;				\
  }while(0)

#define ubuffer_ready_append(buff)		\
  do{						\
    (buff)->pos   = (buff)->limit;              \
    (buff)->limit = (buff)->size;		\
  }while(0)

#define ubuffer_empty(buff) \
  ((buff)->pos = 0,(buff)->limit = 0)

#define ubuffer_stock(buff) \
  ((buff)->limit)

#define ubuffer_has_next(buff) \
  ((buff)->limit - (buff)->pos > 0)

#define ubuffer_is_empty(buff) \
  ((buff)->pos == 0 && (buff)->limit == 0)
  
#define ubuffer_is_full(buff) \
  ((buff)->limit == (buff)->size)

#define ubuffer_data_get(buff) \
  (buff)->data

#define ubuffer_pos_get(buff) \
  (buff)->pos

#define ubuffer_limit_get(buff) \
  (buff)->limit

#endif

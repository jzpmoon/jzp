#ifndef _USTREAM_H_
#define _USTREAM_H_

#include "umacro.h"
#include "udbuffer.h"
#include "ustring_table.h"

#define USTREAM_INPUT  1
#define USTREAM_OUTPUT 2

#define USTREAM_BUFF   0
#define USTREAM_FILE   1

#define USTREAM_READ_LINE 1

typedef struct _ufile_infor ufile_infor;
typedef struct _ustream ustream;

struct _ustream{
  unsigned int iot:2; /* io type */
  unsigned int dst:1; /* data source type */
  unsigned int rdl:1; /* read line */
  uallocator* allocator;
  union {
    struct {
      udbuffer* dbuff;
      USTDC_FILE* file;
    } s;
    ubuffer* buff;
  } u;
};

#define ustream_iot_set(STREAM,IOT) \
  ((STREAM)->iot |= (IOT))

#define ustream_iot_of(STREAM,IOT) \
  ((STREAM)->iot & (IOT))

struct _ufile_infor{
  ustring* file_path;
  ustring* dir_name;
  ustring* file_name;
};

void default_stream_init();

UDECLFUN(UFNAME ufile_init_by_strtb,
         UARGS (ustring_table* strtb,
                ufile_infor* fi,
                ustring* file_path),
         URET uapi ufile_infor* ucall);

UDECLFUN(UFNAME ufile_log,
         UARGS (ufile_infor* fi),
         URET uapi void ucall);

UDECLFUN(UFNAME ufile_exists,
         UARGS (char* file_path),
         URET uapi int ucall);

#define ufile_exists_us(us) \
  ufile_exists((us)->value)

UDECLFUN(UFNAME ustream_iot_change,
         UARGS (ustream* stream,int iot),
         URET uapi void ucall);

uapi ustream* ucall ustream_new_by_buff(int iot,ubuffer* buff,URI_DECL);

UDECLFUN(UFNAME ustream_new_by_file,
         UARGS (int iot,ustring* file_path,URI_DECL),
         URET uapi ustream* ucall);

uapi ustream* ucall ustream_new_by_fd(int iot,int rdl,USTDC_FILE* fd,URI_DECL);

uapi ustream* ucall ustream_stdin(URI_DECL);

uapi ustream* ucall ustream_stdout(URI_DECL);

uapi ustream* ucall ustream_stderr(URI_DECL);

uapi ustream* ucall ustream_new(int iot,int dst);

UDECLFUN(UFNAME ustream_alloc,
         UARGS (uallocator* allocator,int iot,int dst,int rdl),
         URET uapi ustream* ucall);

uapi void ucall ustream_dest(ustream* stream);

UDECLFUN(UFNAME ustream_open_by_path,
         UARGS (ustream* stream,
                char* file_path),
         URET uapi int ucall);

uapi void ucall ustream_close(ustream* stream);

#define ustream_is_close(stream) \
  ((stream)->dst == USTREAM_FILE ? !(stream)->u.s.file : UNULL)

uapi int ucall ustream_read_to_buff(ustream* stream,ubuffer* buff,URI_DECL);

UDECLFUN(UFNAME ustream_read_line_to_buff,
         UARGS (ustream* stream,ubuffer* buff,URI_DECL),
         URET uapi int ucall);

uapi int ucall ustream_read_to_stream(ustream* to_stream,ustream* from_stream,URI_DECL);

uapi int ucall ustream_read_next(ustream* stream,URI_DECL);

uapi int ucall ustream_look_ahead(ustream* stream,URI_DECL);

uapi int ucall ustream_write_dnum(ustream* stream,double dnum,URI_DECL);

uapi int ucall ustream_write_int(ustream* stream, int inte,URI_DECL);

uapi int ucall ustream_write_char(ustream* stream, int chara,URI_DECL);

uapi int ucall ustream_write_string(ustream* stream,char* charp,URI_DECL);

UDECLFUN(UFNAME ustream_write_buff,
         UARGS (ustream* stream,ubuffer* buff),
         URET uapi int ucall);

UDECLFUN(UFNAME ustream_seek,
         UARGS (ustream* stream,long offset,int origin),
         URET uapi int ucall);

UDECLFUN(UFNAME ustream_insert_next,
         UARGS (ustream* stream,long pos,int c),
         URET uapi int ucall);

#define ustream_buff_get(stream) \
  (stream)->u.buff

#endif

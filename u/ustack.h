#ifndef _USTACK_H_
#define _USTACK_H_

#include "udef.h"
#include "ustack_tpl.h"

#ifndef _USTACK_TPL_INT_
ustack_decl_tpl(u,int);
#endif
#ifndef _USTACK_TPL_LONG_
ustack_decl_tpl(u,long);
#endif
#ifndef _USTACK_TPL_FLOAT_
ustack_decl_tpl(u,float);
#endif
#ifndef _USTACK_TPL_DOUBLE_
ustack_decl_tpl(u,double);
#endif
#ifndef _USTACK_TPL_UVOIDP_
ustack_decl_tpl(u,uvoidp);
#endif

#endif

#include "ualloc.h"
#include "ulist_tpl.c"
#include "ulist.h"

#ifndef _ULIST_TPL_INT_
ulist_def_tpl(u,int);
#endif
#ifndef _ULIST_TPL_LONG_
ulist_def_tpl(u,long);
#endif
#ifndef _ULIST_TPL_FLOAT_
ulist_def_tpl(u,float);
#endif
#ifndef _ULIST_TPL_DOUBLE_
ulist_def_tpl(u,double);
#endif
#ifndef _ULIST_TPL_UVOIDP_
ulist_def_tpl(u,uvoidp);
#endif
#ifndef _ULIST_TPL_UCHARP_
ulist_def_tpl(u,ucharp);
#endif

#ifndef _UDEF_H_
#define _UDEF_H_

#include "stddef.h"
#include "uref.h"

#define UTRUE 1
#define UFALSE 0

typedef int ubool;
typedef ustdc_size_t usize_t;
typedef void* uvoidp;
typedef char* ucharp;

#ifdef _THW_X86_H_
  typedef unsigned char uui8;
  typedef unsigned short int uui16;
  typedef unsigned int uui32;
  typedef signed int usi32;
#elif defined _THW_X86_64_H_
  typedef unsigned char uui8;
  typedef unsigned short int uui16;
  typedef unsigned int uui32;
  typedef signed int usi32;
#elif defined _THW_ARM64_H_
  typedef unsigned char uui8;
  typedef unsigned short int uui16;
  typedef unsigned int uui32;
  typedef signed int usi32;
#endif

#endif

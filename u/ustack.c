#include "udef.h"
#include "ustack_tpl.c"
#include "ustack.h"

#ifndef _USTACK_TPL_INT_
ustack_def_tpl(u,int);
#endif
#ifndef _USTACK_TPL_LONG_
ustack_def_tpl(u,long);
#endif
#ifndef _USTACK_TPL_FLOAT_
ustack_def_tpl(u,float);
#endif
#ifndef _USTACK_TPL_DOUBLE_
ustack_def_tpl(u,double);
#endif
#ifndef _USTACK_TPL_UVOIDP_
ustack_def_tpl(u,uvoidp);
#endif

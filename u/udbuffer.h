#ifndef _UDBUFFER_H_
#define _UDBUFFER_H_

#include "ubuffer.h"

typedef struct _udbuffer{
  ubuffer* buff_prev;
  ubuffer* buff_next;
  ubuffer* buff_curr;
  int      buff_size;
} udbuffer;

uapi udbuffer* ucall udbuffer_new(int size);

uapi udbuffer* ucall udbuffer_alloc(uallocator* allocator,int size);

uapi int ucall udbuffer_read_next(udbuffer* dbuff);

uapi int ucall udbuffer_look_ahead(udbuffer* dbuff);

uapi int ucall udbuffer_read_from_file(udbuffer* dbuff,USTDC_FILE* file);

UDECLFUN(UFNAME udbuffer_read_from_stdin,
         UARGS (udbuffer* dbuff,USTDC_FILE* file),
         URET uapi int ucall);

uapi void ucall udbuffer_empty(udbuffer* dbuff);

#define udbuffer_is_empty(dbuff) \
  ((!(dbuff)->buff_prev || ubuffer_is_empty((dbuff)->buff_prev)) && \
   (!(dbuff)->buff_next || ubuffer_is_empty((dbuff)->buff_next)))

#define udbuffer_is_full(dbuff) \
  ((dbuff)->buff_curr && ubuffer_is_full((dbuff)->buff_curr))

#endif

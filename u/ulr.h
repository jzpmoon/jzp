#ifndef _ULR_H_
#define _ULR_H_

#include "umacro.h"
#include "ulist_tpl.h"
#include "uhstb_tpl.h"
#include "ugraph.h"

#define ULRINITSYM -1

/*production*/
typedef struct _ulrprod{
  int head;
  /*body length*/
  int bdylen;
  int body[1];
} ulrprod,*ulrprodp;

#ifndef _UMAP_TPL_ULRPRODP_
umap_decl_tpl(ulrprodp);
#endif

#ifndef _ULIST_TPL_ULRPRODP_
ulist_decl_tpl(u,ulrprodp);
#endif

/*context-free grammar*/
typedef struct _ulrgram{
  /*nonterminals count*/
  int noncnt;
  /*terminals count*/
  int tercnt;
  /*acceptance production*/
  ulrprod* accprod;
  ulist_ulrprodp* prods;
} ulrgram;

/*item*/
typedef struct _ulritem{
  int dot;
  ulrprod* prod;
} ulritem,*ulritemp;

#ifndef _UMAP_TPL_ULRITEMP_
umap_decl_tpl(ulritemp);
#endif

#ifndef _ULIST_TPL_ULRITEMP_
ulist_decl_tpl(u,ulritemp);
#endif

/*item set*/
typedef struct _ulrset{
  UGNODEHEADER;
  /*input symbol*/
  int inpsym;
  ulist_ulritemp* items;
} ulrset,*ulrsetp;

#ifndef _UMAP_TPL_ULRSETP_
umap_decl_tpl(ulrsetp);
#endif

#ifndef _UHSTB_TPL_ULRSETP_
uhstb_decl_tpl(u,ulrsetp);
#endif

/*item set collection*/
typedef struct _ulrcoll{
  UGRAPHHEADER;
  uhstb_ulrsetp* sets;
} ulrcoll;

uapi ulrprod* ucall ulrprod_new(int bdylen);

uapi ulrgram* ucall ulrgram_new();

uapi ulrcoll* ucall ulr0auto(ulrgram* gram);

#endif

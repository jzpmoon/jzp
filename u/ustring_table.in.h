#ifndef _USTRING_TABLE_IN_H_
#define _USTRING_TABLE_IN_H_

#include "umacro.h"
#include "uhstb_tpl.h"
#include "ustring.h"

#ifndef _UMAP_TPL_USTRING_
umap_decl_tpl(ustring);
#endif
#ifndef _UHSTB_TPL_USTRING_
uhstb_decl_tpl(u,ustring);
#endif

typedef uhstb_ustring ustring_table;

#define ustring_table_new(len) \
  uhstb_ustring_new(len)

#define ustring_table_alloc(allocator,len) \
  uhstb_ustring_alloc(allocator,len)

uapi ustring* ucall ustring_table_put(ustring_table* strtb,
			   char*          charp,
			   int            len);

uapi ustring* ucall ustring_table_add(ustring_table* strtb,
			   char*          charp,
			   int            len);

uapi ustring* ucall ustring_concat_by_strtb(ustring_table* strtb,
				 ustring* str1,
				 ustring* str2);
#endif

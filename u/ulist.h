#ifndef _ULIST_H_
#define _ULIST_H_

#include "ulist_tpl.h"
#include "uerror.h"

#ifndef _ULIST_TPL_INT_
ulist_decl_tpl(u,int);
#endif
#ifndef _ULIST_TPL_LONG_
ulist_decl_tpl(u,long);
#endif
#ifndef _ULIST_TPL_FLOAT_
ulist_decl_tpl(u,float);
#endif
#ifndef _ULIST_TPL_DOUBLE_
ulist_decl_tpl(u,double);
#endif
#ifndef _ULIST_TPL_UVOIDP_
ulist_decl_tpl(u,uvoidp);
#endif
#ifndef _ULIST_TPL_UCHARP_
ulist_decl_tpl(u,ucharp);
#endif

#endif

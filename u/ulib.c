#include "umacro.h"
#include "uerror.h"
#include "ustream.h"
#include "ulib.h"

void ulib_dll_init(void)
{
  default_stream_init();
}

udlinit_register(ulib_dll_init,UNULL)

uapi ulib ucall udlopen(ustring* name,int mode)
{
#if UOS == WIN
  return uwin_LoadLibrary(name->value);
#elif UOS == CYGWIN
  return uunx_dlopen(name->value, mode);
#elif UOS == UNX
  return uunx_dlopen(name->value,mode);
#endif
}

uapi ulibsym ucall udlsym(ulib handle,ustring* symbol)
{
#if UOS == WIN
  return uwin_GetProcAddress(handle, symbol->value);
#elif UOS == CYGWIN
  return uunx_dlsym(handle, symbol->value);
#elif UOS == UNX
  return uunx_dlsym(handle,symbol->value);
#endif
}

uapi int ucall udlclose(ulib handle)
{
#if UOS == WIN
  return uwin_FreeLibrary(handle);
#elif UOS == CYGWIN
  return uunx_dlclose(handle);
#elif UOS == UNX
  return uunx_dlclose(handle);
#endif
}

uapi const char* ucall udlerrorstr()
{
#if UOS == WIN
  return UNULL;
#elif UOS == CYGWIN
  return uunx_dlerror();
#elif UOS == UNX
  return uunx_dlerror();
#endif
}

uapi usi32 ucall udlerrorcode()
{
#if UOS == WIN
  return uwin_GetLastError();
#else
  return -1;
#endif
}

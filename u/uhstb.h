#ifndef _UHSTB_H_
#define _UHSTB_H_

#include "udef.h"
#include "uhstb_tpl.h"

#ifndef _UHSTB_TPL_INT_
uhstb_decl_tpl(u,int);
#endif
#ifndef _UHSTB_TPL_SHORT_
uhstb_decl_tpl(u,short);
#endif
#ifndef _UHSTB_TPL_LONG_
uhstb_decl_tpl(u,long);
#endif
#ifndef _UHSTB_TPL_FLOAT_
uhstb_decl_tpl(u,float);
#endif
#ifndef _HSTB_TPL_DOUBLE_
uhstb_decl_tpl(u,double);
#endif
#ifndef _HSTB_TPL_UVOIDP_
uhstb_decl_tpl(u,uvoidp);
#endif

#endif

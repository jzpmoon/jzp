#ifndef _UFBUFFER_H_
#define _UFBUFFER_H_

#include "ulist_tpl.h"
#include "ubuffer.h"
#include "ustream.h"

typedef struct _ufbuffer ufbuffer;

typedef struct _ufbuffer_row ufbuffer_row,*ufbuffer_rowp;

#ifndef _UMAP_TPL_UFBUFFER_ROWP_
umap_decl_tpl(ufbuffer_rowp);
#endif
#ifndef _UMAP_TPL_UBUFFERP_
umap_decl_tpl(ubufferp);
#endif
#ifndef _ULIST_TPL_UFBUFFER_ROWP_
ulist_decl_tpl(u,ufbuffer_rowp);
#endif
#ifndef _ULIST_TPL_UBUFFERP_
ulist_decl_tpl(u,ubufferp);
#endif

struct _ufbuffer {
  uallocator* allocator;
  ustream* stream;
  char* file_path;
  ulist_ufbuffer_rowp* buf_list;
  ulsnd_ufbuffer_rowp* curr_row;
  ulsnd_ubufferp* curr_col;
  int buf_size_max;
  int row_buf_len_max;
  int col_buf_len_max;
  usize_t row_pos;
  usize_t col_pos;
  usize_t buf_col_pos;
  usize_t row_count;
};

#define UFBUFFER_ROW_FLAG_RM 0x1

struct _ufbuffer_row {
  ulist_ubufferp* row_list;
  usize_t col_count;
  unsigned char flag;
};

#define ufbuffer_row_flag_enable(ROW,FLAG) \
  ((ROW)->flag |= (FLAG))

#define ufbuffer_row_flag_disable(ROW,FLAG) \
  ((ROW)->flag &= ~(FLAG))

#define ufbuffer_row_flag_of(ROW,FLAG) \
  ((ROW)->flag & (FLAG))

#define ufbuffer_file_path_has(FBUF) \
  ((FBUF)->file_path)

#define ufbuffer_file_path_set(FBUF,FILE_PATH)  \
  ((FBUF)->file_path = FILE_PATH)

#define ufbuffer_rows_count_get(FBUF) \
  ((FBUF)->row_count)

#define ufbuffer_row_pos_get(FBUF) \
  ((FBUF)->row_pos)

#define ufbuffer_col_pos_get(FBUF) \
  ((FBUF)->col_pos)

#define ufbuffer_buf_col_pos_get(FBUF) \
  ((FBUF)->buf_col_pos)

UDECLFUN(UFNAME ufbuffer_row_alloc,
         UARGS (ufbuffer* buf),
         URET uapi ufbuffer_row* ucall);

UDECLFUN(UFNAME ufbuffer_new,
         UARGS (char* file_path,
                int buf_size_max,
                int row_buf_len_max,
                int col_buf_len_max),
         URET uapi ufbuffer* ucall);

UDECLFUN(UFNAME ufbuffer_des,
         UARGS (ufbuffer* buf),
         URET uapi void ucall);

UDECLFUN(UFNAME ufbuffer_alloc,
         UARGS (uallocator* alloc,
                char* file_path,
                int buf_size_max,
                int row_buf_len_max,
                int col_buf_len_max),
        URET uapi ufbuffer* ucall);

UDECLFUN(UFNAME ufbuffer_row_load,
         UARGS (ufbuffer* buf,
                ufbuffer_row* row,
                int retval),
         URET uapi int ucall);

UDECLFUN(UFNAME ufbuffer_load,
         UARGS (ufbuffer* buf),
         URET uapi void ucall);
         
UDECLFUN(UFNAME ufbuffer_store,
         UARGS (ufbuffer* buf),
         URET uapi int ucall);

UDECLFUN(UFNAME ufbuffer_println,
         UARGS (ufbuffer* buf),
         URET uapi void ucall);

UDECLFUN(UFNAME ufbuffer_row_println,
         UARGS (ufbuffer* buf),
         URET uapi void ucall);

UDECLFUN(UFNAME ufbuffer_row_next,
         UARGS (ufbuffer* buf),
         URET uapi int ucall);

UDECLFUN(UFNAME ufbuffer_row_prev,
         UARGS (ufbuffer* buf),
         URET uapi int ucall);

UDECLFUN(UFNAME ufbuffer_row_goto,
         UARGS (ufbuffer* buf,
                unsigned int row_pos),
         URET uapi void ucall);

UDECLFUN(UFNAME ufbuffer_row_goto_begin,
         UARGS (ufbuffer* buf),
         URET uapi void ucall);

UDECLFUN(UFNAME ufbuffer_row_goto_end,
         UARGS (ufbuffer* buf),
         URET uapi void ucall);

UDECLFUN(UFNAME ufbuffer_col_next,
         UARGS (ufbuffer* buf),
         URET uapi void ucall);

UDECLFUN(UFNAME ufbuffer_col_prev,
         UARGS (ufbuffer* buf),
         URET uapi void ucall);

UDECLFUN(UFNAME ufbuffer_col_goto,
         UARGS (ufbuffer* buf,
                unsigned int col_pos),
         URET uapi void ucall);

UDECLFUN(UFNAME ufbuffer_col_goto_begin,
         UARGS (ufbuffer* buf),
         URET uapi void ucall);

UDECLFUN(UFNAME ufbuffer_col_goto_end,
         UARGS (ufbuffer* buf),
         URET uapi void ucall);
  
UDECLFUN(UFNAME ufbuffer_col_reset,
         UARGS (ufbuffer* buf),
         URET uapi void ucall);

UDECLFUN(UFNAME ufbuffer_row_rm_next_get,
         UARGS (ufbuffer* buf),
         URET uapi ulsnd_ufbuffer_rowp* ucall);

UDECLFUN(UFNAME ufbuffer_row_rm_prev_get,
         UARGS (ufbuffer* buf),
         URET uapi ulsnd_ufbuffer_rowp* ucall);

UDECLFUN(UFNAME ufbuffer_row_append,
         UARGS (ufbuffer* buf),
         URET uapi void ucall);
         
UDECLFUN(UFNAME ufbuffer_row_insert,
         UARGS (ufbuffer* buf),
         URET uapi void ucall);
         
UDECLFUN(UFNAME ufbuffer_row_remove,
         UARGS (ufbuffer* buf),
         URET uapi void ucall);
         
UDECLFUN(UFNAME ufbuffer_col_buf_insert,
         UARGS (ufbuffer* buf),
         URET uapi void ucall);

UDECLFUN(UFNAME ufbuffer_col_char_insert,
         UARGS (ufbuffer* buf,
                int c),
         URET uapi void ucall);

UDECLFUN(UFNAME ufbuffer_col_char_remove,
         UARGS (ufbuffer* buf),
         URET uapi void ucall);

UDECLFUN(UFNAME ufbuffer_curr_row_get,
         UARGS (ufbuffer* buf),
         URET uapi ulsnd_ufbuffer_rowp* ucall);

UDECLFUN(UFNAME ufbuffer_curr_col_get,
         UARGS (ufbuffer* buf),
         URET uapi ulsnd_ubufferp* ucall);
#endif

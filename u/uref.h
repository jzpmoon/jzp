#ifndef _UREF_H_
#define _UREF_H_

#include "umacro.h"

#include <stdlib.h>
#include <malloc.h>
#include <time.h>
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#include <string.h>
#include <stddef.h>
#include <setjmp.h>
#include <limits.h>
#include <errno.h>

#if UOS == WIN

  #include <Windows.h>

  #define UWIN_HMODULE HMODULE
  #define UWIN_FARPROC FARPROC
  #define UWIN_HINSTANCE HINSTANCE
  #define UWIN_DWORD DWORD
  #define UWIN_LPVOID LPVOID
  #define UWIN_DLL_PROCESS_ATTACH DLL_PROCESS_ATTACH
  #define UWIN_DLL_PROCESS_DETACH DLL_PROCESS_DETACH
  #define uwin_LoadLibrary LoadLibrary
  #define uwin_GetProcAddress GetProcAddress
  #define uwin_FreeLibrary FreeLibrary
  #define uwin_GetLastError GetLastError

#elif UOS == CYGWIN

  #include <dlfcn.h>

  #define UUNX_RTLD_LAZY RTLD_LAZY
  #define uunx_dlopen dlopen
  #define uunx_dlsym dlsym
  #define uunx_dlclose dlclose
  #define uunx_dlerror dlerror

#elif UOS == UNX

  #include <dlfcn.h>

  #define UUNX_RTLD_LAZY RTLD_LAZY
  #define uunx_dlopen dlopen
  #define uunx_dlsym dlsym
  #define uunx_dlclose dlclose
  #define uunx_dlerror dlerror

#endif

#define ustdc_system(command) system(command)

#define ustdc_exit(status) exit(status)

#define ustdc_malloc(size) malloc(size)

#define ustdc_free(ptr) free(ptr)

#define ustdc_localtime(time) localtime(time)

#define USTDC_FILE FILE

#define ustdc_stdin stdin

#define ustdc_stdout stdout

#define ustdc_stderr stderr

#define ustdc_fputc fputc

#define ustdc_sprintf sprintf

#define ustdc_fprintf fprintf

#define ustdc_vfprintf vfprintf

#define ustdc_fflush(file) fflush(file)

#define ustdc_va_start(ap,msg) va_start(ap,msg)

#define ustdc_va_end(ap) va_end(ap)

#define ustdc_va_list va_list

#define ustdc_memcpy(dest,src,len) memcpy(dest,src,len)

#define ustdc_memcmp(dest,src,len) memcmp(dest,src,len)

#define ustdc_strcmp(str1,str2) strcmp(str1,str2)

#define ustdc_strlen(str) strlen(str)

#define ustdc_isdigit(c) isdigit(c)

#define ustdc_strtol(str,endstr,base) strtol(str,endstr,base)

#define ustdc_atof(a) atof(a)

#define USTDC_NULL NULL

#define ustdc_size_t size_t

#define ustdc_fgetc(stream) fgetc(stream)

#define ustdc_jmp_buf jmp_buf

#define ustdc_setjmp(envbuf) setjmp(envbuf)

#define ustdc_longjmp(envbuf,val) longjmp(envbuf,val)

#define ustdc_offsetof(stype,member) offsetof(stype,member)

#define ustdc_fseek fseek

#define ustdc_fread fread

#define ustdc_fwrite fwrite

#define ustdc_fopen fopen

#define ustdc_fclose fclose

#define USTDC_SEEK_SET SEEK_SET

#define USTDC_SEEK_CUR SEEK_CUR

#define USTDC_SEEK_END SEEK_END

#define USTDC_ULONG_MAX ULONG_MAX

#define ustdc_errno errno

#endif

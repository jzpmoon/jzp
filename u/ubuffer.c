#include "umacro.h"
#include "ualloc.h"
#include "ustring.h"
#include "ubuffer.h"

uapi ubuffer* ucall ubuffer_new(int size)
{
  int _size;
  ubuffer* buff;
  uallocator* allocator;

  _size = TYPE_SIZE_OF(ubuffer,char,size);
  allocator = &u_global_allocator;
  buff = allocator->alloc(allocator,_size);
  if (buff) {
    buff->allocator = allocator;
    buff->pos   = 0;
    buff->limit = 0;
    buff->size  = size;
  }
  return buff;
}

uapi ubuffer* ucall ubuffer_alloc(uallocator* allocator,int size)
{
  int _size;
  ubuffer* buff;

  _size = TYPE_SIZE_OF(ubuffer,char,size);
  buff = allocator->alloc(allocator,_size);
  if (buff) {
    buff->allocator = allocator;
    buff->pos   = 0;
    buff->limit = 0;
    buff->size  = size;
  }
  return buff;
}

uapi void ucall ubuffer_dest(ubuffer* buff)
{
  buff->allocator->free(buff->allocator,buff);
}

uapi int ucall ubuffer_read_from_buff
(ubuffer* to_buff, ubuffer* from_buff){
  int count = 0;
  while(to_buff->pos < to_buff->limit &&
	from_buff->pos < from_buff->limit){
    to_buff->data[to_buff->pos++] = from_buff->data[from_buff->pos++];
    count++;
  }
  return count;
}

uapi int ucall ubuffer_read_from_file
(ubuffer* to_buff, USTDC_FILE* from_file)
{
  int count = 0;
  int eof_val;
  int err_val;
  int len   = to_buff->limit - to_buff->pos;
  if (len > 0) {
    count = ustdc_fread(&to_buff->data[to_buff->pos],1,len,from_file);
    eof_val = feof(from_file);
    err_val = ferror(from_file);
    if (!eof_val && err_val) {
      return -1;
    }
    to_buff->pos += count;
    to_buff->limit = to_buff->pos;
  }
  return count;
}

UDEFUN(UFNAME ubuffer_write_to_file,
       UARGS (ubuffer* from_buff, USTDC_FILE* to_file),
       URET uapi int ucall)
UDECLARE
  int count;
  int err_val;
UBEGIN
  count = ustdc_fwrite(from_buff->data,1,from_buff->limit,to_file);
  err_val = ferror(to_file);
  if (err_val) {
    return -1;
  }
  return count;
UEND

UDEFUN(UFNAME ubuffer_read_line_from_file,
       UARGS (ubuffer* to_buff, USTDC_FILE* from_file),
       URET uapi int ucall)
UDECLARE
  int eof_val;
  int len = to_buff->limit - to_buff->pos;
  int i = 0;
UBEGIN
  while (i < len) {
    int c = ustdc_fgetc(from_file);
    eof_val = feof(from_file);
    if (eof_val) {
      return -1;
    }
    if (c == '\r' || c == '\n') {
      return 0;
    }
    to_buff->data[to_buff->pos++] = c;
    i++;
  }
  return 1;
UEND

UDEFUN(UFNAME ubuffer_read_from_stdin,
       UARGS (ubuffer* to_buff, USTDC_FILE* from_stdin),
       URET uapi int ucall)
UDECLARE
  int count = 0;
  int len   = to_buff->limit - to_buff->pos;
UBEGIN
  if (len > 0) {
    int c;
    while (1) {
      if (count >= len) break;
      c = ustdc_fgetc(from_stdin);
      if (c == '\r' || c == '\n' || feof(from_stdin))
        break;
      to_buff->data[to_buff->pos + count] = c;
      count++;
    }
    to_buff->pos += count;
    to_buff->limit = to_buff->pos;
  }
  return count;
UEND

uapi int ucall ubuffer_read_next(ubuffer* buff){
  int next;
  if(buff->pos < buff->limit){
    next = buff->data[buff->pos++];
  }else{
    next = -1;
  }
  return next;
}

UDEFUN(UFNAME ubuffer_read_at,
       UARGS (ubuffer* buff,usize_t pos),
       URET uapi int ucall)
UDECLARE

UBEGIN
  if (pos < 0 || pos > buff->limit) {
    return -1;
  }
  return buff->data[pos];
UEND

UDEFUN(UFNAME ubuffer_insert_next,
       UARGS (ubuffer* buff,usize_t pos,int byte),
       URET uapi void ucall)
UDECLARE
  usize_t i;
UBEGIN
  if (pos < 0 || pos > buff->limit) {
    return;
  }
  if (buff->limit >= buff->size) {
    i = buff->limit - 1;
  } else {
    i = buff->limit;
    buff->limit++;
  }
  while (i > pos) {
    buff->data[i] = buff->data[i - 1];
    i--;
  }
  buff->data[pos] = byte;
UEND

UDEFUN(UFNAME ubuffer_remove_next,
       UARGS (ubuffer* buff,usize_t pos),
       URET uapi void ucall)
UDECLARE
  usize_t i = pos + 1;
UBEGIN
  if (pos < 0 || pos >= buff->limit) {
    return;
  }
  while (i < buff->limit) {
    buff->data[i - 1] = buff->data[i];
    i++;
  }
  if (buff->limit > 0) {
    buff->limit--;
  }
UEND

UDEFUN(UFNAME ubuffer_pop,
       UARGS (ubuffer* buff),
       URET uapi int ucall)
UDECLARE
  int byte = buff->data[buff->limit - 1];
UBEGIN
  if (buff->limit > 0) {
    buff->limit--;
  }
  return byte;
UEND

uapi int ucall ubuffer_write_next(ubuffer* buff,int byte){
  if(buff->pos < buff->limit){
    buff->data[buff->pos++] = byte;
    return 0;
  }else{
    return -1;
  }
}

uapi int ucall ubuffer_write_int2ascii(ubuffer* buff,int inte)
{
  int count;
  if (buff->pos < buff->limit) {
    count = uitostr(&buff->data[buff->pos], buff->limit - buff->pos, inte);
    if (count < 0) {
      return -1;
    }
    buff->pos += count;
    return 0;
  } else {
    return -1;
  }
}

UDEFUN(UFNAME ubuffer_write_ustring,
       UARGS (ubuffer* buff,ustring* str),
       URET uapi int ucall)
UDECLARE
  int i;
UBEGIN
  for (i = 0; i < str->len; i++) {
    if (ubuffer_write_next(buff,str->value[i])) {
      break;
    }
  }
  return i;
UEND

UDEFUN(UFNAME ubuffer2ustring,
       UARGS (ustring_table* strtb,
              ubuffer* buff),
       URET uapi ustring* ucall)
UDECLARE
  ustring* ustr;
UBEGIN
  ustr = ustring_table_put(
    strtb,
    ubuffer_data_get(buff),
    ubuffer_limit_get(buff));
  return ustr;
UEND

UDEFUN(UFNAME ubuffer_print,
       UARGS (ubuffer* buff),
       URET uapi void ucall)
UDECLARE
  usize_t i;
UBEGIN
  for (i = 0; i < buff->limit; i++) {
    ustdc_fputc(buff->data[i],ustdc_stdout);
  }
UEND

uapi int ucall ubuffer_look_ahead(ubuffer* buff){
  int next;
  if(buff->pos < buff->limit){
    next = buff->data[buff->pos];
  }else{
    next = -1;
  }
  return next;
}

uapi int ucall ubuffer_seek(ubuffer* buff,long offset,int origin){
  usize_t pos;
  if(origin == USEEK_SET){
    pos = 0;
  }else if(origin == USEEK_CUR){
    pos = buff->pos;
  }else if(origin == USEEK_END){
    pos = buff->limit;
  }else{
    return -1;
  }
  pos += offset;
  if(pos < 0){
    pos = 0;
  }else if(pos > buff->limit){
    pos = buff->limit;
  }
  buff->pos = pos;
  return 0;
}


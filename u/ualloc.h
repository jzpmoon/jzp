#ifndef _UALLOC_H_
#define _UALLOC_H_

#include "uref.h"
#include "udef.h"
#include "umacro.h"

#define UNULL USTDC_NULL

#define uif(test,t,e) \
  if(test){t}else{e}

#define unew(ptr,size,err) \
  uif(!((ptr) = ualloc(size)),err,(void)0;)

uapi void* ucall ualloc(usize_t size);

uapi void ucall ufree(void* ptr);

#define BOUNDARY (sizeof(void*))
#define ALIGN(size,align) ((size+align-1)&(~(align-1)))

#define ALIGN_BOUNDARY(size) ALIGN(size,BOUNDARY)

#define TYPE_SIZE_OF(stype,mtype,length) \
  sizeof(stype)+sizeof(mtype)*((length)-1)

#define uoffsetof(stype,member) ustdc_offsetof(stype,member)

#define ustruct_addr(ptr,stype,member) \
  ((stype*)((char*)ptr - uoffsetof(stype,member)))

typedef struct _uallocator uallocator;
typedef void* (ucall *ualloc_ft)(uallocator* allocator, usize_t size);
typedef void* (ucall *uallocx_ft)(uallocator* allocator,char* data,
  usize_t size);
typedef void (ucall *ufree_ft)(uallocator* allocator,void* ptr);
typedef void (ucall *uclean_ft)(uallocator* allocator);

#define UALLOCATOR_HEADER \
  ualloc_ft alloc;	  \
  uallocx_ft allocx;	  \
  ufree_ft free;	  \
  uclean_ft clean

struct _uallocator{
  UALLOCATOR_HEADER;
};

extern uapi uallocator u_global_allocator;

#endif

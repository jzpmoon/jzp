#include "umacro.h"
#include "uref.h"
#include "ualloc.h"
#include "utime.h"
#include "uerror.h"

URI_ERR_DEFINE(UERR_SUCC, "U000", "success");
URI_ERR_DEFINE(UERR_ERR, "U001", "error");
URI_ERR_DEFINE(UERR_OFM, "U002", "out of memory");
URI_ERR_DEFINE(UERR_EOF, "U003", "end of file");
URI_ERR_DEFINE(UERR_IOT, "U004", "unknow IO type");
URI_ERR_DEFINE(UERR_DST, "U005", "unknow data source type");
URI_ERR_DEFINE(UERR_IOCLOSE, "U005", "IO is close");

static ulog_conf _log_conf = {UNULL,UTRUE,-1};

uapi ulog_infor _uli = {UNULL,UNULL,1};

uapi int ucall ulog_init(ulog_conf* conf)
{
    int retval = 0;
    USTDC_FILE* log_fd;

    if (conf)
    {
        if (_uli.log_fd &&
            _uli.log_fd != ustdc_stdout &&
            fclose(_uli.log_fd) == EOF)
        {
            retval = -1;
        }
        else
        {
            log_fd = fopen(conf->log_fn, "w");
            if (!log_fd)
            {
                retval = -1;
            }
            else
            {
                _uli.log_fd = log_fd;
                _uli.conf = conf;
            }
        }
    }
    else
    {
        _uli.log_fd = ustdc_stdout;
        _uli.conf = &_log_conf;
    }
    return retval;
}

uapi void ucall uverror(char* msg,ustdc_va_list ap)
{
  USTDC_FILE* log_fd;
  char* time_str;

  if (!_uli.conf || !_uli.conf->power) {
    ustdc_vfprintf(ustdc_stdout, msg, ap);
  } else {
    log_fd = _uli.log_fd;
    if (log_fd) {
      time_str = utimestr();
      if (time_str == UNULL) {
        time_str = "get time error!";
      }
      ustdc_fprintf(log_fd, "%d.<%s>", _uli.curr_line, time_str);
      ustdc_vfprintf(log_fd, msg, ap);
      ustdc_fprintf(log_fd, "\n");
      ustdc_fflush(log_fd);
    }
  }
}

uapi void ucall uerror(char* msg,...)
{
  ustdc_va_list ap;
  ustdc_va_start(ap, msg);
  uverror(msg,ap);
  ustdc_va_end(ap);

  ustdc_fprintf(ustdc_stderr, "error!");

  ustdc_exit(-1);
}

uapi void ucall uabort(char* msg,...)
{
  ustdc_va_list ap;

  ustdc_va_start(ap, msg);
  uverror(msg,ap);
  ustdc_va_end(ap);

  ustdc_fprintf(ustdc_stderr,"abort!");

  abort();
}

uapi void ucall uexit(int c, char* msg,...)
{
  ustdc_va_list ap;
  ustdc_va_start(ap, msg);
  uverror(msg,ap);
  ustdc_va_end(ap);

  ustdc_fprintf(ustdc_stderr, "exit!");

  ustdc_exit(c);
}

uapi void ucall ulog(char* msg,...)
{
  ulog_conf* conf;
  USTDC_FILE* log_fd;
  ustdc_va_list ap;
  char* time_str;

  conf = _uli.conf;
  if (!conf || !conf->power || !_uli.log_fd) {
    return;
  }
  if (conf->line > 0 &&
      _uli.curr_line % conf->line == 0 &&
      _uli.log_fd != ustdc_stdout) {
    if (fclose(_uli.log_fd) == EOF) {
      return;
    }
    _uli.log_fd = fopen(conf->log_fn,"w");
  }
  log_fd = _uli.log_fd;
  time_str = utimestr();
  if (time_str == UNULL) {
    time_str = "get time error!";
  }
  ustdc_fprintf(log_fd,"%d.[%s]", _uli.curr_line, time_str);
  ustdc_va_start(ap,msg);
  ustdc_vfprintf(log_fd,msg,ap);
  ustdc_va_end(ap);
  ustdc_fprintf(log_fd,"\n");
  ustdc_fflush(log_fd);
  _uli.curr_line++;
}

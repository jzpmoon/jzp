#ifndef _UDIR_IN_H_
#define _UDIR_IN_H_

#include "ulist_tpl.h"
#include "ustring.h"
#include "ustring_table.h"
#include "ubuffer.h"

typedef struct _udir udir;

struct _udir {
  ustring* name;
  struct _ulist_udir* childs;
  udir* parent;
};

#ifndef _UMAP_TPL_UDIR_
umap_decl_tpl(udir);
#endif
#ifndef _ULIST_TPL_UDIR_
ulist_decl_tpl(u,udir);
#endif

uapi extern ustring ustring_root;

uapi extern ustring ustring_parent;

uapi extern ustring ustring_current;

uapi extern udir udir_root_level;

uapi extern udir udir_parent_level;

uapi extern udir udir_current_level;

UDECLFUN(UFNAME udir_init,
         UARGS (uallocator* allocator,udir* dir),
         URET uapi int ucall);

UDECLFUN(UFNAME udir_relative_path_get,
         UARGS (uallocator* allocator,ulist_udir* from,ulist_udir* to),
         URET uapi ulist_udir* ucall);

UDECLFUN(UFNAME udir_path_add,
         UARGS (udir* top_dir,ulist_udir* path),
         URET uapi int ucall);

UDECLFUN(UFNAME udir_ls2us_by_strtb,
         UARGS (ustring_table* strtb,
                ubuffer* buff,
                ulist_udir* path,
                ustring* sub_path,
                char sep,
                ustringp* out_path),
         URET uapi int ucall);

#endif

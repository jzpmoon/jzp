#include "ualloc.h"
#include "ustream.h"

static ustream* stdin_stream;
static ustream* stdout_stream;
static ustream* stderr_stream;

void default_stream_init()
{
  URI_DEFINE;
  ustream_stdin(URI_REF);
  ustream_stdout(URI_REF);
  ustream_stderr(URI_REF);
}

#define USTREAM_FILE_BUFF_SIZE 10
#define USTREAM_FILE_BUFF_GET(stream) ((stream)->u.s.dbuff)
#define USTREAM_FILE_GET(stream) ((stream)->u.s.file)

static ustring* ufile_name_get_by_strtb(ustring_table* strtb,
                                        ustring* file_path)
{
  ustring* file_name;
  int pos;
  int len;

  pos = ustring_char_at(file_path,UDIR_SEP,-1);
  len = file_path->len - pos - 1;
  if (pos < 0) {
    pos = 0;
  }
  if (len <= 0) {
    return UNULL;
  }
  file_name = ustring_table_put(strtb,
                                file_path->value + pos,
                                len);
  return file_name;
}

static ustring* ufile_dir_get_by_strtb(ustring_table* strtb,
                                       ustring* file_path)
{
  ustring* file_name;
  int len;

  len = ustring_char_at(file_path,UDIR_SEP,-1) + 1;
  if (len <= 0) {
    return UNULL;
  }
  file_name = ustring_table_put(strtb,
                                file_path->value,
                                len);
  return file_name;
}

uapi ufile_infor* ucall ufile_init_by_strtb(
  ustring_table* strtb,
  ufile_infor* fi,
  ustring* file_path)
{
  ustring* file_name = UNULL;
  ustring* dir_name = UNULL;
  
  dir_name = ufile_dir_get_by_strtb(strtb,file_path);
  file_name = ufile_name_get_by_strtb(strtb,file_path);
  if (!file_name) {
    goto err;
  }
  fi->file_path = file_path;
  fi->dir_name = dir_name;
  fi->file_name = file_name;
  return fi;
 err:
  return UNULL;
}

uapi void ucall ufile_log(ufile_infor* fi)
{
  if (fi->file_path)
    udebug1("file path:%s",fi->file_path->value);
  if (fi->dir_name)
    udebug1("dir name:%s",fi->dir_name->value);
  if (fi->file_name)
    udebug1("file name:%s",fi->file_name->value);
}

UDEFUN(UFNAME ufile_exists,
       UARGS (char* file_path),
       URET uapi int ucall)
UDECLARE
  USTDC_FILE* fp;
UBEGIN
  fp = ustdc_fopen(file_path, "r");
  if (fp) {
    ustdc_fclose(fp);
    return 1;
  } else {
    return 0;
  }
UEND

UDEFUN(UFNAME ustream_iot_change,
       UARGS (ustream* stream,int iot),
       URET uapi void ucall)
UDECLARE
  udbuffer* dbuff = UNULL;
UBEGIN
  stream->iot = iot;
  if (stream->dst == USTREAM_FILE) {
    if (ustream_iot_of(stream, USTREAM_INPUT) &&
        stream->u.s.dbuff == UNULL) {
      dbuff = udbuffer_alloc(stream->allocator,USTREAM_FILE_BUFF_SIZE);
      if(!dbuff){
        uabort("udbuffer_alloc error!");
      }
      stream->u.s.dbuff = dbuff;
    }
  }
UEND

uapi ustream* ucall ustream_new_by_buff(int iot,ubuffer* buff,URI_DECL){
  uallocator* allocator;
  ustream* stream;

  allocator = &u_global_allocator;
  stream = ustream_alloc(allocator,iot,USTREAM_BUFF,!USTREAM_READ_LINE);
  if (!stream) {
    URI_RETVAL(UERR_OFM,UNULL);
  }
  stream->u.buff = buff;
  URI_RETVAL(UERR_SUCC,stream);
}

UDEFUN(UFNAME ustream_new_by_file,
       UARGS (int iot,ustring* file_path,URI_DECL), 
       URET uapi ustream* ucall)
UDECLARE
  ustream* stream = UNULL;
  uallocator* allocator;
UBEGIN
  allocator = &u_global_allocator;
  stream = ustream_alloc(allocator,iot,USTREAM_FILE,!USTREAM_READ_LINE);
  if (!stream) {
    goto err;
  }
  if (ustream_open_by_path(stream,file_path->value)) {
    goto err;
  }
  URI_RETVAL(UERR_SUCC,stream);
 err:
  ustream_dest(stream);
  URI_RETVAL(UERR_OFM,UNULL);
UEND

uapi ustream* ucall ustream_new_by_fd(int iot,int rdl,USTDC_FILE* fd,URI_DECL)
{
  ustream* stream = UNULL;
  uallocator* allocator;

  allocator = &u_global_allocator;
  stream = ustream_alloc(allocator,iot,USTREAM_FILE,rdl);
  if (!stream) {
    goto err;
  }
  USTREAM_FILE_GET(stream) = fd;
  URI_RETVAL(UERR_SUCC,stream);
 err:
  ustream_dest(stream);
  URI_RETVAL(UERR_OFM,UNULL);
}

uapi ustream* ucall ustream_stdin(URI_DECL)
{
  if (stdin_stream == UNULL) {
    stdin_stream = ustream_new_by_fd(USTREAM_INPUT, USTREAM_READ_LINE, ustdc_stdin, URI_REF);
    URI_ERROR;
    return UNULL;
    URI_END;
  }
  URI_RETVAL(UERR_SUCC, stdin_stream);
}

uapi ustream* ucall ustream_stdout(URI_DECL)
{
  if (stdout_stream == UNULL) {
    stdout_stream = ustream_new_by_fd(USTREAM_OUTPUT, !USTREAM_READ_LINE, ustdc_stdout, URI_REF);
    URI_ERROR;
    return UNULL;
    URI_END;
  }
  URI_RETVAL(UERR_SUCC, stdout_stream);
}

uapi ustream* ucall ustream_stderr(URI_DECL)
{
  if (stderr_stream == UNULL) {
    stderr_stream = ustream_new_by_fd(USTREAM_OUTPUT, !USTREAM_READ_LINE, stderr, URI_REF);
    URI_ERROR;
    return UNULL;
    URI_END;
  }
  URI_RETVAL(UERR_SUCC, stderr_stream);
}

uapi ustream* ucall ustream_new(int iot,int dst){
  uallocator* allocator;

  allocator = &u_global_allocator;
  return ustream_alloc(allocator,iot,dst,!USTREAM_READ_LINE);
}

UDEFUN(UFNAME ustream_alloc,
       UARGS (uallocator* allocator,int iot,int dst,int rdl),
       URET uapi ustream* ucall)
UDECLARE
  ustream* stream = UNULL;
  udbuffer* dbuff = UNULL;
UBEGIN
  stream = allocator->alloc(allocator,sizeof(ustream));
  if (stream) {
    ustream_iot_set(stream,iot);
    stream->dst = dst;
    stream->rdl = rdl;
    stream->allocator = allocator;
    if (dst == USTREAM_BUFF) {
      stream->u.buff = UNULL;
    } else if (dst == USTREAM_FILE) {
      if (ustream_iot_of(stream, USTREAM_INPUT)) {
        dbuff = udbuffer_alloc(allocator,USTREAM_FILE_BUFF_SIZE);
        if(!dbuff){
          goto err;
        }
        stream->u.s.dbuff = dbuff;
      } else {
        stream->u.s.dbuff = UNULL;
      }
      stream->u.s.file = UNULL;
    }
  }
  return stream;
 err:
  allocator->free(allocator,stream);
  allocator->free(allocator,dbuff);
  return UNULL;
UEND

uapi void ucall ustream_dest(ustream* stream)
{
  uallocator* alloc = stream->allocator;

  ustream_close(stream);
  alloc->free(alloc,stream->u.s.dbuff);
  alloc->free(alloc,stream);
}

UDEFUN(UFNAME ustream_open_by_path,
       UARGS (ustream* stream,
              char* file_path),
       URET uapi int ucall)
UDECLARE
  char* mode;
UBEGIN
  if (stream->dst == USTREAM_BUFF) {
    return -2;
  }
  udbuffer_empty(stream->u.s.dbuff);
  if (stream->u.s.file) {
    ustream_close(stream);
  }
  if (ustream_iot_of(stream,USTREAM_INPUT) &&
      ustream_iot_of(stream,USTREAM_OUTPUT)) {
    mode = "r+";
  } else {
    if (ustream_iot_of(stream,USTREAM_INPUT)) {
      mode = "r";
    } else if (ustream_iot_of(stream,USTREAM_OUTPUT)) {
      mode = "w";
    } else {
      uabort("unknow io type!");
    }
  }
  stream->u.s.file = ustdc_fopen(file_path,mode);
  if (!stream->u.s.file) {
    return -1;
  } else {
    return 0;
  }
UEND

uapi void ucall ustream_close(ustream* stream)
{
  if (stream->dst == USTREAM_FILE) {
    USTDC_FILE* file = stream->u.s.file;
    if (file == ustdc_stdin || file == ustdc_stdout || file == ustdc_stdout) {
      stream->u.s.file = UNULL;
    } else if (file) {
      ustdc_fclose(file);
      stream->u.s.file = UNULL;
    }
  }
}

uapi int ucall ustream_read_to_buff(ustream* stream,ubuffer* buff,URI_DECL)
{
  int count = 0;
  if(!ustream_iot_of(stream, USTREAM_INPUT)){
    URI_RETVAL(UERR_IOT,count);
  }
  if(stream->dst == USTREAM_BUFF){
    ubuffer_ready_write(buff);
    count = ubuffer_read_from_buff(buff,stream->u.buff);
    ubuffer_ready_read(buff);
    URI_RETVAL(UERR_SUCC,count);
  }else if(stream->dst == USTREAM_FILE){
    USTDC_FILE* file = USTREAM_FILE_GET(stream);
    if (!file) {
      URI_RETVAL(UERR_IOCLOSE, -1);
    }
    if (file == ustdc_stdin) {
      do {
        ubuffer_ready_write(buff);
        count = ubuffer_read_from_stdin(buff,file);
      } while (count == 0);
    } else {
      ubuffer_ready_write(buff);
      count = ubuffer_read_from_file(buff,file);
      if(count == 0){
        URI_RETVAL(UERR_EOF,count);
      } if (count == -1) {
        URI_RETVAL(UERR_ERR,count);
      }
    }
    ubuffer_ready_read(buff);
    URI_RETVAL(UERR_SUCC,count);
  }else{
    URI_RETVAL(UERR_DST,count);
  }
}

/* -1 end of file,
 *  0 end of line,
 *  1 continue read line
 */
UDEFUN(UFNAME ustream_read_line_to_buff,
       UARGS (ustream* stream,ubuffer* buff,URI_DECL),
       URET uapi int ucall)
UDECLARE
  int count = 0;
UBEGIN
  if(!ustream_iot_of(stream, USTREAM_INPUT)){
    URI_RETVAL(UERR_IOT,count);
  }
  if(stream->dst == USTREAM_BUFF){
    ubuffer_ready_write(buff);
    count = ubuffer_read_from_buff(buff,stream->u.buff);
    ubuffer_ready_read(buff);
    URI_RETVAL(UERR_SUCC,count);
  }else if(stream->dst == USTREAM_FILE){
    USTDC_FILE* file = USTREAM_FILE_GET(stream);
    if (!file) {
      URI_RETVAL(UERR_IOCLOSE, -1);
    }
    ubuffer_ready_write(buff);
    count = ubuffer_read_line_from_file(buff,file);
    ubuffer_ready_read(buff);
    URI_RETVAL(UERR_SUCC,count);
  }else{
    URI_RETVAL(UERR_DST,count);
  }
UEND

uapi int ucall ustream_read_to_stream(ustream* to_stream,ustream* from_stream,URI_DECL)
{
  if (from_stream->dst == USTREAM_BUFF) {
    
  } else if (from_stream->dst == USTREAM_FILE) {
    if (to_stream->dst == USTREAM_BUFF) {
      return ustream_read_to_buff(from_stream,to_stream->u.buff,URI_REF);
    }
  }
  URI_RETVAL(UERR_ERR,-1);
}

uapi int ucall ustream_read_next(ustream* stream,URI_DECL){
  int next = -1;
  if(!ustream_iot_of(stream, USTREAM_INPUT)){
    URI_RETVAL(UERR_IOT,next);
  }
  if(stream->dst == USTREAM_BUFF){
    next = ubuffer_read_next(stream->u.buff);
    if(next == -1){
      URI_RETVAL(UERR_EOF,next);
    }
  }else if(stream->dst == USTREAM_FILE){
    udbuffer* dbuff = USTREAM_FILE_BUFF_GET(stream);
    USTDC_FILE* file = USTREAM_FILE_GET(stream);
    if (!file) {
      URI_RETVAL(UERR_IOCLOSE, -1);
    }
    next = udbuffer_read_next(dbuff);
    if(next == -1){
      if (file == ustdc_stdin) {
        if (stream->rdl == USTREAM_READ_LINE &&
            (udbuffer_is_empty(dbuff) || udbuffer_is_full(dbuff))) {
          udbuffer_read_from_stdin(dbuff,file);
        } else {
          udbuffer_empty(dbuff);
          URI_RETVAL(UERR_EOF,next);
        }
      } else {
        udbuffer_read_from_file(dbuff,file);
      }
      next = udbuffer_read_next(dbuff);
      if(next == -1){
        URI_RETVAL(UERR_EOF,next);
      }
    }
  }else{
    URI_RETVAL(UERR_DST,next);
  }
  URI_RETVAL(UERR_SUCC,next);
}

uapi int ucall ustream_look_ahead(ustream* stream,URI_DECL){
  int next = -1;
  if(!ustream_iot_of(stream, USTREAM_INPUT)){
    URI_RETVAL(UERR_IOT,next);
  }
  if(stream->dst == USTREAM_BUFF){
    next = ubuffer_look_ahead(stream->u.buff);
    if(next == -1){
      URI_RETVAL(UERR_EOF,next);
    }
  }else if(stream->dst == USTREAM_FILE){
    udbuffer* dbuff = USTREAM_FILE_BUFF_GET(stream);
    USTDC_FILE* file = USTREAM_FILE_GET(stream);
    if (!file) {
      URI_RETVAL(UERR_IOCLOSE, -1);
    }
    next = udbuffer_look_ahead(dbuff);
    if(next == -1){
      if (file == ustdc_stdin) {
        udbuffer_read_from_stdin(dbuff,file);
      } else {
        udbuffer_read_from_file(dbuff,file);
      }
      next = udbuffer_look_ahead(dbuff);
      if(next == -1){
        URI_RETVAL(UERR_EOF,next);        
      }
    }
  }else{
    URI_RETVAL(UERR_DST,next);
  }
  URI_RETVAL(UERR_SUCC,next);
}

uapi int ucall ustream_write_dnum(ustream* stream,double dnum,URI_DECL){
  if(!ustream_iot_of(stream, USTREAM_OUTPUT)){
    URI_RETVAL(UERR_IOT,-1);
  }
  if(stream->dst == USTREAM_BUFF){

  }else if(stream->dst == USTREAM_FILE){
    USTDC_FILE* file = USTREAM_FILE_GET(stream);
    if (!file) {
      URI_RETVAL(UERR_IOCLOSE, -1);
    }
    if(ustdc_fprintf(file,"%f",dnum) < 0){
      URI_RETVAL(UERR_ERR,-1);
    }
  }else{
    URI_RETVAL(UERR_DST,-1);
  }
  URI_RETVAL(UERR_SUCC,0);
}

uapi int ucall ustream_write_int(ustream* stream, int inte,URI_DECL)
{
  if(!ustream_iot_of(stream, USTREAM_OUTPUT)){
    URI_RETVAL(UERR_IOT,-1);
  }
  if(stream->dst == USTREAM_BUFF){

  }else if(stream->dst == USTREAM_FILE){
    USTDC_FILE* file = USTREAM_FILE_GET(stream);
    if (!file) {
      URI_RETVAL(UERR_IOCLOSE, -1);
    }
    if(ustdc_fprintf(file,"%d",inte) < 0){
      URI_RETVAL(UERR_ERR,-1);
    }
  }else{
    URI_RETVAL(UERR_DST,-1);
  }
  URI_RETVAL(UERR_SUCC,0);
}

uapi int ucall ustream_write_char(ustream* stream, int chara,URI_DECL)
{
  if(!ustream_iot_of(stream, USTREAM_OUTPUT)){
    URI_RETVAL(UERR_IOT,-1);
  }
  if(stream->dst == USTREAM_BUFF){

  }else if(stream->dst == USTREAM_FILE){
    USTDC_FILE* file = USTREAM_FILE_GET(stream);
    if (!file) {
      URI_RETVAL(UERR_IOCLOSE, -1);
    }
    if(ustdc_fputc(chara,file) < 0){
      URI_RETVAL(UERR_ERR,-1);
    }
  }else{
    URI_RETVAL(UERR_DST,-1);
  }
  URI_RETVAL(UERR_SUCC,0);
}

uapi int ucall ustream_write_string(ustream* stream,char* charp,URI_DECL)
{
  if(!ustream_iot_of(stream, USTREAM_OUTPUT)){
    URI_RETVAL(UERR_IOT,-1);
  }
  if(stream->dst == USTREAM_BUFF){

  }else if(stream->dst == USTREAM_FILE){
    USTDC_FILE* file = USTREAM_FILE_GET(stream);
    if (!file) {
      URI_RETVAL(UERR_IOCLOSE, -1);
    }
    if(ustdc_fprintf(file,"%s",charp) < 0){
      URI_RETVAL(UERR_ERR,-1);
    }
  }else{
    URI_RETVAL(UERR_DST,-1);
  }
  URI_RETVAL(UERR_SUCC,0);
}

UDEFUN(UFNAME ustream_write_buff,
       UARGS (ustream* stream,ubuffer* buff),
       URET uapi int ucall)
UDECLARE
  int count = 0;
UBEGIN
  if(!ustream_iot_of(stream, USTREAM_OUTPUT)){
    return -1;
  }
  if(stream->dst == USTREAM_BUFF){

  }else if(stream->dst == USTREAM_FILE){
    USTDC_FILE* file = USTREAM_FILE_GET(stream);
    if (!file) {
      return -1;
    }
    count = ubuffer_write_to_file(buff,file);
    if (count < 0) {
      return -1;
    }
  }else{
    return -1;
  }
  return count;
UEND

UDEFUN(UFNAME ustream_seek,
       UARGS (ustream* stream,long offset,int origin),
       URET uapi int ucall)
UDECLARE
UBEGIN
  if(stream->dst == USTREAM_BUFF){
    return ubuffer_seek(stream->u.buff,offset,origin);
  }else if(stream->dst == USTREAM_FILE){
    USTDC_FILE* file = USTREAM_FILE_GET(stream);
    return ustdc_fseek(file,offset,origin);
  }else{
    return -1;
  }
UEND

UDEFUN(UFNAME ustream_insert_next,
       UARGS (ustream* stream,long pos,int c),
       URET uapi int ucall)
UDECLARE

UBEGIN
  if (stream->dst == USTREAM_BUFF) {
    ubuffer_insert_next(stream->u.buff,pos,c);
    return 0;
  } else {
    return -1;
  }
UEND
